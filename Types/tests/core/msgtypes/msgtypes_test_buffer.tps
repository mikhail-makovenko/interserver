!!CLASSNAME_PREFIX          INTERSERVER::TEST::
!!NAMESPACE                 interserver::test::msgtypes


class TestBuffer_Message    !!CONSTRUCTOR(BASIC)
    uint16_t attr_1
    uint32_t attr_2

    {!
    bool operator == (const TestBuffer_Message& other) const {
       return ((this->attr_1_ == other.attr_1_) &&
               (this->attr_2_ == other.attr_2_));
    }

    std::string to_string() const {
        return "attr_1_: " + std::to_string(this->attr_1_) + ", " +
                "attr_2_: " + std::to_string(this->attr_2_);
    }
    !}
