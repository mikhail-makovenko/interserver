!!CLASSNAME_PREFIX          INTERSERVER::TEST::
!!NAMESPACE                 interserver::test::msgtypes


// TestTypeMap_2 requires TestTypeMap_1 to have empty constructor
class TestTypeMap_1     !!CONSTRUCTOR(EMPTY)
    int32_t         id
    string          name


class TestTypeMap_2
    int32_t         id
    string          name
    TestTypeMap_1   typemap_1


class TestTypeMap_Named_1   !!NAME(TestTypeMap_1);
    int32_t         id


class TestTypeMap_Named_2   !!NAME(TestTypeMap_2);
    int32_t         id
