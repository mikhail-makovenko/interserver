!!CLASSNAME_PREFIX          INTERSERVER::
!!SEPARATE_METHODS_FILE     1
!!NAMESPACE                 interserver::msgtypes


/// `BaseType` represent a simple type like an integer or a float.
class __BaseType__      !!CONSTRUCTOR(BASIC)
    /// Type id
    int32_t         id
    /// Type name
    string          name
    /// Type size, bytes. -1 means size is in the next bytes
    int32_t         size

    {!
        virtual ~__BaseType__() {};
    !}


/// `TypeField` represents a field of a `StructType` object.
class __TypeField__     !!CONSTRUCTOR(BASIC)
    string      name
    /// type_id of a `StructType`
    int32_t     type_id

    {!
        virtual ~__TypeField__() {};
    !}


/// `StructType` represents a type with properties.
class __StructType__    !!CONSTRUCTOR(BASIC)
    string              name
    int32_t             id
    __TypeField__[]     fields

    {!
        virtual ~__StructType__() {};
    !}


/// `FuncArg` contains function argument name and it's id.
class __FuncArg__       !!CONSTRUCTOR(BASIC)
    string              name
    int32_t             type_id

    {!
        virtual ~__FuncArg__() {};
    !}


/// `FuncType` contains function name and id and arguments.
class __FuncType__      !!CONSTRUCTOR(BASIC)
    string              name
    int32_t             id
    __FuncArg__[]       args

    {!
        virtual ~__FuncType__() {};
    !}


/// `TypeMap` class is used to keep track of all known types.
///
/// Keeps track of all known `BaseType` and `StructType` types.
/// Can provide collected information in a form that can be passed to
/// a client program via a socket.
class __TypeMap__       !!CONSTRUCTOR(EMPTY)
    __BaseType__[]      base_types
    __StructType__[]    struct_types
    __FuncType__[]      func_types

    {!
        virtual ~__TypeMap__() {};
    !}
