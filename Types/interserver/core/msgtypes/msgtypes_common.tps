!!CLASSNAME_PREFIX          INTERSERVER::
!!NAMESPACE                 interserver::msgtypes


/// `BaseType` represent a simple type like an integer or a float.
class IntegerAndString      !!CONSTRUCTOR(BASIC)
    int32_t         integer
    string          string
