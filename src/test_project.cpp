#include "gtest/gtest.h"
#include "tests/tests.hpp"


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    //::testing::GTEST_FLAG(filter) = "Server.client_connection";
    return RUN_ALL_TESTS();
}
