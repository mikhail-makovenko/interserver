#pragma once

#include <arpa/inet.h>
#include <cstring>
#include <memory>
#include <thread>

#include "interserver/settings.hpp"
#include "buffer.hpp"
#include "programbase.hpp"
#include "frontbase.hpp"


namespace interserver {

    enum ClientConnectionState {
            connected, connecting, disconnected,
    };


    /// For client side programs.
    class ClientProgram : protected ProgramBase {
    private:
        std::shared_ptr<spdlog::logger>     logger_;

        bool                stop_flag_ = false;
        std::thread         thread_;

        int                 socket_desc_ = 0;
        struct sockaddr_in  server_address_;

        int                 port_ = 0;
        std::string         host_;
        ClientConnectionState   connection_state_ = disconnected;

        void sync_byte_order();
        void sync_ids();
        int recv_into_read_buffer(bool rewind);
        int recv_into_read_buffer_full_message(bool rewind);
        void loop_listen();
        void process_input();

        Buffer*                 server_buffer_;
        ClientSocketWrapper*    client_ = nullptr;

        void on_client_disconnect();

        int32_t last_message_id = 0;
    protected:
        int32_t gen_message_id() {
            ++last_message_id;
            if (last_message_id <= 0) {
                last_message_id = 1;
            }
            return last_message_id;
        };
    public:
        std::shared_ptr<spdlog::logger> get_logger() {
            return logger_;
        };

        ClientProgram();
        ClientProgram(const ClientProgram& client) =delete;
        ~ClientProgram();

        void start_tcp(const std::string& host, int port);
        void stop();
    };

}
