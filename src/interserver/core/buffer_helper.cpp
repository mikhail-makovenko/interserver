#include <cstdint>
#include <string>


namespace interserver {

    class Buffer;


    class BufferHelper {
    private:
        Buffer* buffer_;
    public:
        BufferHelper(Buffer* buffer): buffer_(buffer) {};
        BufferHelper(const Buffer& buffer) =delete;

        /// Returns true if first integer in buffer is equal or smaller then
        /// used number of bytes.
        bool have_full_message();

        void flush_data_to_socket(int socket_desc, bool set_message_size,
                int forward_count=-1);

        int32_t get_message_size();

        void set_message_id(int32_t id);
        int32_t get_message_id();

        void set_error_flag(bool flag);
        bool get_error_flag();

        void set_front_id(int32_t id);
        int32_t get_front_id();

        void write_error_message(const std::string& msg);

        void seek_to_content();
    };

}
