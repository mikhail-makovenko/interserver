#pragma once

#include <cstdint>
#include <string>
#include <memory>
#include <unordered_map>
#include "buffer.hpp"
#include "typemap.hpp"
#include "utils/sequence.hpp"
#include "clientsocketwrapper.hpp"
#include "spdlog/spdlog.h"
#include "interserver/settings.hpp"
#include "interserver/core/msgtypes/msgtypes_common.hpp"


namespace interserver {

    class ProgramBase;


    class FrontBase  {
    // for doing sync ids
    friend class ProgramBase;
    private:
        std::vector<std::shared_ptr<FrontBase>>     fronts_;

        int32_t id_;
        int32_t other_id_ = -1; /// id of the same front on another machine
        std::string name_;

        ProgramBase* program_ = nullptr;

        static utils::IntSequence id_generator_;

        static std::shared_ptr<spdlog::logger> logger_;

        static int32_t last_func_id;

        // for doing sync ids
        void collect_front_names(std::vector<std::string>* v);
        void collect_front_names_and_ids(
                std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v);
        void sync_front_other_ids(std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v);
    public:
        FrontBase(std::string name);
        FrontBase(std::string name, int32_t id);
        FrontBase(const FrontBase& front) =delete;
        virtual ~FrontBase() {};

        static int32_t gen_func_id();

        void add_front(std::shared_ptr<FrontBase> front) {
            fronts_.push_back(front);
        }

        virtual std::shared_ptr<spdlog::logger> get_logger() {
            return logger_;
        };

        const int32_t& get_id() { return id_; };
        const int32_t& get_other_id() { return other_id_; };
        std::string get_name() { return name_; };

        void set_id(int32_t id) { id_ = id; };
        void set_other_id(int32_t id) { other_id_ = id; };
        void set_name(std::string name) { name_ = name; };

        ProgramBase* get_program() { return program_; };

        virtual void on_client_connect(ClientSocketWrapper* client) {};
        virtual void on_client_disconnect(ClientSocketWrapper* client) {};
        virtual void on_register(ProgramBase* program);
        virtual void on_unregister(ProgramBase* program);
        virtual void on_program_stop() {};

        virtual void read_function_call(ClientSocketWrapper* client) {};
        virtual void typemap_register(TypeMap* typemap) {};

        FrontBase* get_front_by_id(int32_t id);
        FrontBase* get_front_by_name(std::string name);
    };



    class FrontBaseCustomLogger : public FrontBase {
    private:
        std::shared_ptr<spdlog::logger> logger_;
    public:
        virtual ~FrontBaseCustomLogger() {};

        std::shared_ptr<spdlog::logger> get_logger() {
            return logger_;
        };

        FrontBaseCustomLogger(
                std::string name,
                std::string logger_name, spdlog::level::level_enum severity);
        FrontBaseCustomLogger(
                std::string name, int32_t id,
                std::string logger_name, spdlog::level::level_enum severity);
    };



    class ServerFrontBaseCustomLogger : public FrontBaseCustomLogger {
    public:
        virtual ~ServerFrontBaseCustomLogger() {};

        ServerFrontBaseCustomLogger(
                std::string name,
                spdlog::level::level_enum severity);
        ServerFrontBaseCustomLogger(
                std::string name, int32_t id,
                spdlog::level::level_enum severity);
    };



    class ClientFrontBaseCustomLogger : public FrontBaseCustomLogger {
    public:
        virtual ~ClientFrontBaseCustomLogger() {};

        ClientFrontBaseCustomLogger(
                std::string name,
                spdlog::level::level_enum severity);
        ClientFrontBaseCustomLogger(
                std::string name, int32_t id,
                spdlog::level::level_enum severity);
    };
}
