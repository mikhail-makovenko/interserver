#include <unordered_map>
#include <string>
#include <vector>

#include <unordered_map>
#include <string>
#include <vector>

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "typemap_constants.hpp"

#include "msgtypes/msgtypes_typemap.hpp"



namespace interserver {


    /** `BaseType` represent a simple type like an integer or a float.
     *
     * Fields:
     *  - id
     *  - name
     *  - size in bytes,  -1 means size is in the next bytes
     */
    class BaseType : public msgtypes::__BaseType__ {
    public:
        BaseType(int32_t id, const std::string& name, int32_t size);
    };


    BaseType::BaseType(int32_t id, const std::string& name, int32_t size):
        msgtypes::__BaseType__(id, name, size) {}


    /* `TypeField` represents a field of a `StructType` object.
     *
     * Fields:
     *  - name
     *  - type_id (of a `StructType`)
     */
    class TypeField : public msgtypes::__TypeField__ {
    public:
        TypeField(const std::string& name, int32_t type_id);
    };


    TypeField::TypeField(const std::string& name, int32_t type_id):
        msgtypes::__TypeField__(name, type_id) {}


    /** `StructType` represents a type with properties.
     *
     * Fields:
     *  - name
     *  - id
     *  - fields
     */
    class StructType : public msgtypes::__StructType__ {
    public:
        StructType(const std::string& name, int32_t id);

        void add_field(const std::string& name, int32_t type_id_);
    };


    StructType::StructType(const std::string& name, int32_t id):
        msgtypes::__StructType__(name, id) {}


    void StructType::add_field(const std::string& name, int32_t type_id) {
        fields_.push_back(
            std::shared_ptr<TypeField>(new TypeField(name, type_id))
        );
    }


    /// `FuncArg` contains function argument name and it's id.
    class FuncArg : public msgtypes::__FuncArg__ {
    public:
        using msgtypes::__FuncArg__::__FuncArg__;
    };


    /// `FuncType` contains function name and id and arguments.
    class FuncType : public msgtypes::__FuncType__ {
    public:
        using msgtypes::__FuncType__::__FuncType__;

        void add_arg(const std::string& name, int32_t type_id);
    };


    void FuncType::add_arg(const std::string& name, int32_t type_id) {
        args_.push_back(
            std::shared_ptr<FuncArg>(new FuncArg(name, type_id))
        );
    }


    /** `TypeMap` class is used to keep track of all known types.
     *
     * Keeps track of all known `BaseType` and `StructType` types.
     * Can provide collected information in a form that can be passed to
     * a client program via a socket.
     *
     * Fields:
     *  - base_types
     *  - struct_types
     */
    class TypeMap : public msgtypes::__TypeMap__ {
    private:
        std::unordered_map<int32_t, int32_t> synced_type_ids_;
        std::unordered_map<int32_t, int32_t> synced_func_ids_;
    public:
        TypeMap();
        TypeMap(Buffer* buffer, bool use_type_id):
                msgtypes::__TypeMap__(buffer, nullptr, use_type_id) {};

        static int32_t gen_type_id();
        StructType* add_struct(
                const std::string& struct_name, int32_t struct_id);
        FuncType* add_func(
                const std::string& func_name, int32_t func_id);

        std::string to_json();

        void sync(Buffer* buffer, bool use_type_id);
        void sync(const TypeMap* typemap);
        int32_t get_synced_type_id(int32_t not_synced_id);
        int32_t get_synced_func_id(int32_t not_synced_id);

        StructType* get_struct_by_name(const std::string& name);
        FuncType* get_func_by_name(const std::string& name);

        void sync_type_id(int32_t not_synced_id, int32_t synced_id);
        void sync_func_id(int32_t not_synced_id, int32_t synced_id);

        bool has_type_name(std::string name);
        bool has_type_id(int32_t id);

        bool has_func_name(std::string name);
        bool has_func_id(int32_t id);
    };


    TypeMap::TypeMap() {
        typedef std::shared_ptr<msgtypes::__BaseType__> basetype_sptr;
        base_types_ = {
                basetype_sptr(new BaseType(MAPTYPE_EMPTY,   "EMTPY",   0)),
                basetype_sptr(new BaseType(MAPTYPE_NULL,    "NULL",    0)),

                basetype_sptr(new BaseType(MAPTYPE_INT_8,   "INT_8",   1)),
                basetype_sptr(new BaseType(MAPTYPE_INT_16,  "INT_16",  2)),
                basetype_sptr(new BaseType(MAPTYPE_INT_32,  "INT_32",  4)),
                basetype_sptr(new BaseType(MAPTYPE_INT_64,  "INT_64",  8)),

                basetype_sptr(new BaseType(MAPTYPE_UINT_8,  "UINT_8",  1)),
                basetype_sptr(new BaseType(MAPTYPE_UINT_16, "UINT_16", 2)),
                basetype_sptr(new BaseType(MAPTYPE_UINT_32, "UINT_32", 4)),
                basetype_sptr(new BaseType(MAPTYPE_UINT_64, "UINT_64", 8)),

                basetype_sptr(new BaseType(MAPTYPE_FLOAT,   "FLOAT",   4)),
                basetype_sptr(new BaseType(MAPTYPE_DOUBLE,  "FLOAT",   8)),

                basetype_sptr(new BaseType(MAPTYPE_BOOL,    "BOOL",    1)),
                basetype_sptr(new BaseType(MAPTYPE_STRING,  "STRING", -1)),

                basetype_sptr(new BaseType(MAPTYPE_ARRAY,   "ARRAY",  -1)),
                basetype_sptr(new BaseType(MAPTYPE_MAP,     "MAP",    -1)),
                basetype_sptr(new BaseType(MAPTYPE_BINARY,  "BINARY", -1)),

                basetype_sptr(new BaseType(MAPTYPE_FUNCTION_CALL,
                        "FUNCTION_CALL", 4)), // 4 bytes - function ID
                basetype_sptr(new BaseType(MAPTYPE_CALLBACK,
                        "CALLBACK", 4))
        };
    }

}


