#include <vector>
#include <memory>
#include "interserver/core/messagebase.hpp"
#include <cstring>


namespace interserver {
    class Buffer;
    class TypeMap;
}


namespace interserver { namespace msgtypes { 


    //--------------------------------------------------------------------------

    /// `BaseType` represent a simple type like an integer or a float.
    class __BaseType__ : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        __BaseType__(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        __BaseType__(int32_t id, const std::string& name, int32_t size): 
            id_(id),
            name_(name),
            size_(size) {};

        /// Type id
        int32_t id_;

        /// Type name
        std::string name_;

        /// Type size, bytes. -1 means size is in the next bytes
        int32_t size_;


        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        virtual ~__BaseType__() {};

    };


    // constants ------------------------------------------

    const int __BaseType__::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string __BaseType__::TYPE_NAME =
            "INTERSERVER::__BaseType__";


    //--------------------------------------------------------------------------


    /// `TypeField` represents a field of a `StructType` object.
    class __TypeField__ : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        __TypeField__(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        __TypeField__(const std::string& name, int32_t type_id): 
            name_(name),
            type_id_(type_id) {};

        std::string name_;

        /// type_id of a `StructType`
        int32_t type_id_;


        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        virtual ~__TypeField__() {};

    };


    // constants ------------------------------------------

    const int __TypeField__::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string __TypeField__::TYPE_NAME =
            "INTERSERVER::__TypeField__";


    //--------------------------------------------------------------------------


    /// `StructType` represents a type with properties.
    class __StructType__ : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        __StructType__(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        __StructType__(const std::string& name, int32_t id): 
            name_(name),
            id_(id) {};

        std::string name_;
        int32_t id_;
        std::vector<std::shared_ptr<__TypeField__>> fields_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        virtual ~__StructType__() {};

    };


    // constants ------------------------------------------

    const int __StructType__::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string __StructType__::TYPE_NAME =
            "INTERSERVER::__StructType__";


    //--------------------------------------------------------------------------


    /// `FuncArg` contains function argument name and it's id.
    class __FuncArg__ : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        __FuncArg__(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        __FuncArg__(const std::string& name, int32_t type_id): 
            name_(name),
            type_id_(type_id) {};

        std::string name_;
        int32_t type_id_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        virtual ~__FuncArg__() {};

    };


    // constants ------------------------------------------

    const int __FuncArg__::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string __FuncArg__::TYPE_NAME =
            "INTERSERVER::__FuncArg__";


    //--------------------------------------------------------------------------


    /// `FuncType` contains function name and id and arguments.
    class __FuncType__ : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        __FuncType__(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        __FuncType__(const std::string& name, int32_t id): 
            name_(name),
            id_(id) {};

        std::string name_;
        int32_t id_;
        std::vector<std::shared_ptr<__FuncArg__>> args_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        virtual ~__FuncType__() {};

    };


    // constants ------------------------------------------

    const int __FuncType__::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string __FuncType__::TYPE_NAME =
            "INTERSERVER::__FuncType__";


    //--------------------------------------------------------------------------


    /// `TypeMap` class is used to keep track of all known types.
    ///
    /// Keeps track of all known `BaseType` and `StructType` types.
    /// Can provide collected information in a form that can be passed to
    /// a client program via a socket.
    class __TypeMap__ : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        __TypeMap__() {};
        __TypeMap__(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };

        std::vector<std::shared_ptr<__BaseType__>> base_types_;
        std::vector<std::shared_ptr<__StructType__>> struct_types_;
        std::vector<std::shared_ptr<__FuncType__>> func_types_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        virtual ~__TypeMap__() {};

    };


    // constants ------------------------------------------

    const int __TypeMap__::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string __TypeMap__::TYPE_NAME =
            "INTERSERVER::__TypeMap__";


    //--------------------------------------------------------------------------


} }