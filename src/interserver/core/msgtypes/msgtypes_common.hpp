#pragma once

#include "interserver/core/messagebase.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"
#include <cstring>


namespace interserver { namespace msgtypes { 


    //--------------------------------------------------------------------------

    /// `BaseType` represent a simple type like an integer or a float.
    class IntegerAndString : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        IntegerAndString(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        IntegerAndString(int32_t integer, const std::string& string): 
            integer_(integer),
            string_(string) {};

        int32_t integer_;
        std::string string_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    //--------------------------------------------------------------------------


} }