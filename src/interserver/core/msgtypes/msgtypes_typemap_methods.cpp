#include <vector>
#include <memory>
#include "interserver/core/messagebase.hpp"
#include <cstring>
#include "interserver/core/buffer_methods.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"
#include "msgtypes_typemap.hpp"


namespace interserver {
    class Buffer;
    class TypeMap;
}


namespace interserver { namespace msgtypes { 


    //--------------------------------------------------------------------------

    // getters --------------------------------------------

    const int32_t __BaseType__::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return __BaseType__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__BaseType__::TYPE_ID);
    }

    const int32_t __BaseType__::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return __BaseType__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__BaseType__::TYPE_ID);
    }

    const std::string& __BaseType__::get_type_name() const {
        return __BaseType__::TYPE_NAME;
    }

    const std::string& __BaseType__::static_get_type_name() {
        return __BaseType__::TYPE_NAME;
    }

    // other methods --------------------------------------

    void __BaseType__::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&id_);
        buffer->write_value(&name_);
        buffer->write_value(&size_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void __BaseType__::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&id_);
        buffer->read_value(&name_);
        buffer->read_value(&size_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void __BaseType__::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                __BaseType__::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                __BaseType__::static_get_type_name(),
                __BaseType__::static_get_type_id(typemap));

        new_type->add_field("id",
                MAPTYPE_INT_32);

        new_type->add_field("name",
                MAPTYPE_STRING);

        new_type->add_field("size",
                MAPTYPE_INT_32);

    }


    //--------------------------------------------------------------------------


    // getters --------------------------------------------

    const int32_t __TypeField__::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return __TypeField__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__TypeField__::TYPE_ID);
    }

    const int32_t __TypeField__::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return __TypeField__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__TypeField__::TYPE_ID);
    }

    const std::string& __TypeField__::get_type_name() const {
        return __TypeField__::TYPE_NAME;
    }

    const std::string& __TypeField__::static_get_type_name() {
        return __TypeField__::TYPE_NAME;
    }

    // other methods --------------------------------------

    void __TypeField__::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&name_);
        buffer->write_value(&type_id_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void __TypeField__::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&name_);
        buffer->read_value(&type_id_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void __TypeField__::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                __TypeField__::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                __TypeField__::static_get_type_name(),
                __TypeField__::static_get_type_id(typemap));

        new_type->add_field("name",
                MAPTYPE_STRING);

        new_type->add_field("type_id",
                MAPTYPE_INT_32);

    }


    //--------------------------------------------------------------------------


    // getters --------------------------------------------

    const int32_t __StructType__::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return __StructType__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__StructType__::TYPE_ID);
    }

    const int32_t __StructType__::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return __StructType__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__StructType__::TYPE_ID);
    }

    const std::string& __StructType__::get_type_name() const {
        return __StructType__::TYPE_NAME;
    }

    const std::string& __StructType__::static_get_type_name() {
        return __StructType__::TYPE_NAME;
    }

    // other methods --------------------------------------

    void __StructType__::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&name_);
        buffer->write_value(&id_);
        buffer->write_container_wrapped_pointers(&fields_, typemap, false);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void __StructType__::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&name_);
        buffer->read_value(&id_);
        fields_.clear();
        buffer->read_container_wrapped_pointers(&fields_, typemap, false);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void __StructType__::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                __StructType__::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                __StructType__::static_get_type_name(),
                __StructType__::static_get_type_id(typemap));

        new_type->add_field("name",
                MAPTYPE_STRING);

        new_type->add_field("id",
                MAPTYPE_INT_32);

        new_type->add_field("fields",
                __TypeField__::static_get_type_id(typemap));

    }


    //--------------------------------------------------------------------------


    // getters --------------------------------------------

    const int32_t __FuncArg__::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return __FuncArg__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__FuncArg__::TYPE_ID);
    }

    const int32_t __FuncArg__::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return __FuncArg__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__FuncArg__::TYPE_ID);
    }

    const std::string& __FuncArg__::get_type_name() const {
        return __FuncArg__::TYPE_NAME;
    }

    const std::string& __FuncArg__::static_get_type_name() {
        return __FuncArg__::TYPE_NAME;
    }

    // other methods --------------------------------------

    void __FuncArg__::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&name_);
        buffer->write_value(&type_id_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void __FuncArg__::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&name_);
        buffer->read_value(&type_id_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void __FuncArg__::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                __FuncArg__::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                __FuncArg__::static_get_type_name(),
                __FuncArg__::static_get_type_id(typemap));

        new_type->add_field("name",
                MAPTYPE_STRING);

        new_type->add_field("type_id",
                MAPTYPE_INT_32);

    }


    //--------------------------------------------------------------------------


    // getters --------------------------------------------

    const int32_t __FuncType__::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return __FuncType__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__FuncType__::TYPE_ID);
    }

    const int32_t __FuncType__::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return __FuncType__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__FuncType__::TYPE_ID);
    }

    const std::string& __FuncType__::get_type_name() const {
        return __FuncType__::TYPE_NAME;
    }

    const std::string& __FuncType__::static_get_type_name() {
        return __FuncType__::TYPE_NAME;
    }

    // other methods --------------------------------------

    void __FuncType__::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&name_);
        buffer->write_value(&id_);
        buffer->write_container_wrapped_pointers(&args_, typemap, false);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void __FuncType__::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&name_);
        buffer->read_value(&id_);
        args_.clear();
        buffer->read_container_wrapped_pointers(&args_, typemap, false);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void __FuncType__::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                __FuncType__::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                __FuncType__::static_get_type_name(),
                __FuncType__::static_get_type_id(typemap));

        new_type->add_field("name",
                MAPTYPE_STRING);

        new_type->add_field("id",
                MAPTYPE_INT_32);

        new_type->add_field("args",
                __FuncArg__::static_get_type_id(typemap));

    }


    //--------------------------------------------------------------------------


    // getters --------------------------------------------

    const int32_t __TypeMap__::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return __TypeMap__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__TypeMap__::TYPE_ID);
    }

    const int32_t __TypeMap__::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return __TypeMap__::TYPE_ID;
        }
        return typemap->get_synced_type_id(__TypeMap__::TYPE_ID);
    }

    const std::string& __TypeMap__::get_type_name() const {
        return __TypeMap__::TYPE_NAME;
    }

    const std::string& __TypeMap__::static_get_type_name() {
        return __TypeMap__::TYPE_NAME;
    }

    // other methods --------------------------------------

    void __TypeMap__::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_container_wrapped_pointers(&base_types_, typemap, false);
        buffer->write_container_wrapped_pointers(&struct_types_, typemap, false);
        buffer->write_container_wrapped_pointers(&func_types_, typemap, false);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void __TypeMap__::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        base_types_.clear();
        buffer->read_container_wrapped_pointers(&base_types_, typemap, false);
        struct_types_.clear();
        buffer->read_container_wrapped_pointers(&struct_types_, typemap, false);
        func_types_.clear();
        buffer->read_container_wrapped_pointers(&func_types_, typemap, false);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void __TypeMap__::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                __TypeMap__::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                __TypeMap__::static_get_type_name(),
                __TypeMap__::static_get_type_id(typemap));

        new_type->add_field("base_types",
                __BaseType__::static_get_type_id(typemap));

        new_type->add_field("struct_types",
                __StructType__::static_get_type_id(typemap));

        new_type->add_field("func_types",
                __FuncType__::static_get_type_id(typemap));

    }


    //--------------------------------------------------------------------------


} }