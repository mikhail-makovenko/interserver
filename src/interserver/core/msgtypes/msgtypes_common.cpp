#include "interserver/core/messagebase.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"
#include <cstring>


namespace interserver { namespace msgtypes { 


    //--------------------------------------------------------------------------

    /// `BaseType` represent a simple type like an integer or a float.
    class IntegerAndString : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        IntegerAndString(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        IntegerAndString(int32_t integer, const std::string& string): 
            integer_(integer),
            string_(string) {};

        int32_t integer_;
        std::string string_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    // constants ------------------------------------------

    const int IntegerAndString::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string IntegerAndString::TYPE_NAME =
            "INTERSERVER::IntegerAndString";

    // getters --------------------------------------------

    const int32_t IntegerAndString::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return IntegerAndString::TYPE_ID;
        }
        return typemap->get_synced_type_id(IntegerAndString::TYPE_ID);
    }

    const int32_t IntegerAndString::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return IntegerAndString::TYPE_ID;
        }
        return typemap->get_synced_type_id(IntegerAndString::TYPE_ID);
    }

    const std::string& IntegerAndString::get_type_name() const {
        return IntegerAndString::TYPE_NAME;
    }

    const std::string& IntegerAndString::static_get_type_name() {
        return IntegerAndString::TYPE_NAME;
    }

    // other methods --------------------------------------

    void IntegerAndString::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&integer_);
        buffer->write_value(&string_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void IntegerAndString::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&integer_);
        buffer->read_value(&string_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void IntegerAndString::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                IntegerAndString::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                IntegerAndString::static_get_type_name(),
                IntegerAndString::static_get_type_id(typemap));

        new_type->add_field("integer",
                MAPTYPE_INT_32);

        new_type->add_field("string",
                MAPTYPE_STRING);

    }


    //--------------------------------------------------------------------------


} }