#pragma once


namespace interserver {
    const int32_t MAPTYPE_EMPTY    = 0;
    const int32_t MAPTYPE_NULL     = 1;

    const int32_t MAPTYPE_INT_8    = 2;
    const int32_t MAPTYPE_INT_16   = 3;
    const int32_t MAPTYPE_INT_32   = 4;
    const int32_t MAPTYPE_INT_64   = 5;

    const int32_t MAPTYPE_UINT_8   = 6;
    const int32_t MAPTYPE_UINT_16  = 7;
    const int32_t MAPTYPE_UINT_32  = 8;
    const int32_t MAPTYPE_UINT_64  = 9;

    const int32_t MAPTYPE_FLOAT    = 10;
    const int32_t MAPTYPE_DOUBLE   = 11;

    const int32_t MAPTYPE_BOOL     = 12;
    const int32_t MAPTYPE_STRING   = 13;

    const int32_t MAPTYPE_ARRAY    = 14;
    const int32_t MAPTYPE_MAP      = 15;
    const int32_t MAPTYPE_BINARY   = 16;
    const int32_t MAPTYPE_FUNCTION_CALL = 17;
    const int32_t MAPTYPE_CALLBACK = 17;

    const int32_t TYPEMAP_MIN_STRUCT_ID = 50;


    template<typename T>
    struct TYPEMAP_ID {};

    // int

    template<> struct TYPEMAP_ID<int8_t> { enum: const int32_t {
        ID = MAPTYPE_INT_8
    }; };

    template<> struct TYPEMAP_ID<int16_t> { enum: const int32_t {
        ID = MAPTYPE_INT_16
    }; };

    template<> struct TYPEMAP_ID<int32_t> { enum: const int32_t {
        ID = MAPTYPE_INT_32
    }; };

    template<> struct TYPEMAP_ID<int64_t> { enum: const int32_t {
        ID = MAPTYPE_INT_64
    }; };


    // uint

    template<> struct TYPEMAP_ID<uint8_t> { enum: const int32_t {
        ID = MAPTYPE_UINT_8
    }; };

    template<> struct TYPEMAP_ID<uint16_t> { enum: const int32_t {
        ID = MAPTYPE_UINT_16
    }; };

    template<> struct TYPEMAP_ID<uint32_t> { enum: const int32_t {
        ID = MAPTYPE_UINT_32
    }; };

    template<> struct TYPEMAP_ID<uint64_t> { enum: const int32_t {
        ID = MAPTYPE_UINT_64
    }; };

    // float and double

    template<> struct TYPEMAP_ID<float> { enum: const int32_t {
        ID = MAPTYPE_FLOAT
    }; };

    template<> struct TYPEMAP_ID<double> { enum: const int32_t {
        ID = MAPTYPE_DOUBLE
    }; };

    // other

    template<> struct TYPEMAP_ID<bool> { enum: const int32_t {
       ID = MAPTYPE_BOOL
    }; };

    template<> struct TYPEMAP_ID<std::string> { enum: const int32_t {
       ID = MAPTYPE_STRING
    }; };

}
