#include <algorithm>
#include <arpa/inet.h>
#include <memory>

#include <thread>
#include <ctime>
#include <cstring>
#include <system_error>
#include <unistd.h>

#include "interserver/settings.hpp"
#include "buffer.hpp"
#include "buffer_methods.hpp"
#include "programbase.hpp"
#include "frontbase.hpp"
#include "clientsocketwrapper.hpp"
#include "utils/endian.hpp"
#include "parts/client/fronts/wf/server_core_front_wf.hpp"
#include "msgtypes/msgtypes_common.hpp"


namespace interserver {


    enum ClientConnectionState {
            connected, connecting, disconnected,
    };


    /// For client side programs.
    class ClientProgram : protected ProgramBase {
    private:
        std::shared_ptr<spdlog::logger>     logger_;

        bool                stop_flag_ = false;
        std::thread         thread_;

        int                 socket_desc_ = 0;
        struct sockaddr_in  server_address_;

        int                 port_ = 0;
        std::string         host_;
        ClientConnectionState   connection_state_ = disconnected;

        void sync_byte_order();
        void sync_ids();
        int recv_into_read_buffer(bool rewind);
        int recv_into_read_buffer_full_message(bool rewind);
        void loop_listen();

        Buffer*                 server_buffer_;
        ClientSocketWrapper*    client_ = nullptr;

        void on_client_disconnect();

        int32_t last_message_id = 0;
    protected:
        int32_t gen_message_id() {
            ++last_message_id;
            if (last_message_id <= 0) {
                last_message_id = 1;
            }
            return last_message_id;
        };
    public:
        std::shared_ptr<spdlog::logger> get_logger() {
            return logger_;
        };

        ClientProgram();
        ClientProgram(const ClientProgram& client) =delete;
        ~ClientProgram();

        void start_tcp(const std::string& host, int port);
        void stop();
    };


    ClientProgram::ClientProgram():
            logger_(gen_logger_st("Client", LOG_SEVERITY_CLIENT)),
            server_buffer_(new Buffer(TCP_CLIENT_BUFFER_SIZE, false)) {}


    ClientProgram::~ClientProgram() {
        if (connection_state_ == connected || connection_state_ == connecting) {
            stop();
        }

        if (client_) {
            delete client_;
        }
        if (server_buffer_) {
            delete server_buffer_;
        }
    }


    void ClientProgram::start_tcp(
            const std::string& host, int port) {
        if (connection_state_ == connected || connection_state_ == connecting) {
            throw std::runtime_error("Client already connected or connecting");
        }
        connection_state_ = connecting;
        host_ = host;
        port_ = port;

        stop_flag_ = false;

        socket_desc_ = socket(AF_INET , SOCK_STREAM , 0);
        if (socket_desc_ == -1) {
            throw std::runtime_error("Client program could not create socket");
        }

        server_address_.sin_addr.s_addr = inet_addr(host_.c_str());
        server_address_.sin_family = AF_INET;
        server_address_.sin_port = htons(port_);
        if (connect(
                socket_desc_ ,
                (struct sockaddr*)&server_address_ ,
                sizeof(server_address_)) < 0) {

            throw std::runtime_error("Failed to connect to server: " + host_ +
                    ":" + std::to_string(port_));
        }
        if (client_) {
            delete client_;
        }

        client_ = new ClientSocketWrapper(
                socket_desc_, server_address_,
                std::shared_ptr<Buffer>(
                        new Buffer(TCP_CLIENT_BUFFER_SIZE, false)),
                server_buffer_);
        logger_->info("Connected ({}:{})", host_.c_str(), port_);
        connection_state_ = connected;

        sync_byte_order();
        sync_ids();

        thread_ = std::thread(&ClientProgram::loop_listen, this);
    }


    void ClientProgram::sync_byte_order() {
        // setting byte order
        uint8_t command = 1; // use server byte order
        client_->write_buffer_->reset();
        client_->read_buffer_->reset();

        client_->write_buffer_->write_value(&command);
        logger_->trace("Sent `sync_byte_order` request");
        client_->write_buffer_->helper_.flush_data_to_socket(
                socket_desc_, false);
        recv_into_read_buffer(true);

        logger_->trace("Got `sync_byte_order` response");
        bool server_is_little_endian;
        client_->read_buffer_->read_value(&server_is_little_endian);

        client_->set_changed_byte_order(
                server_is_little_endian != utils::IS_LITTLE_ENDIAN);
        client_->set_byte_order_synced(true);

        logger_->debug("Success `sync_byte_order` "
                "(server: {}, client: {}, change: {})",
                    (server_is_little_endian ? "little" : "big"),
                    (utils::IS_LITTLE_ENDIAN ? "little" : "big"),
                    server_is_little_endian != utils::IS_LITTLE_ENDIAN);
    }


    void ClientProgram::sync_ids() {
        client_->write_buffer_->reset();
        client_->write_buffer_->helper_.seek_to_content();
        WF::S::ServerCoreFront::f_sync_ids(
                this, nullptr, client_->write_buffer_);

        std::vector<std::string> sv;
        // struct types
        logger_->trace("Sync to send struct names count: {}",
                typemap_.struct_types_.size());
        for (const auto& strct : typemap_.struct_types_) {
            sv.push_back(strct->name_);
        }
        client_->write_buffer_->write_container_strings(
                &sv, &typemap_, true);
        sv.clear();
        // func types
        logger_->trace("Sync to send func names count: {}",
                        typemap_.func_types_.size());
        for (const auto& fnc : typemap_.func_types_) {
            sv.push_back(fnc->name_);
        }
        client_->write_buffer_->write_container_strings(
                &sv, &typemap_, true);
        // front ids
        sv.clear();
        collect_front_names(&sv);
        std::vector<std::shared_ptr<msgtypes::IntegerAndString>> isv;
        collect_front_names_and_ids(&isv);
        for (const auto& name_id : isv) {
            client_->add_known_front(name_id->integer_);
        }
        logger_->trace("Sync sending front names count: {}", sv.size());
        client_->write_buffer_->write_container_strings(
                        &sv, &typemap_, true);

        client_->flush_write_buffer(true);
        client_->read_buffer_->reset();

        recv_into_read_buffer_full_message(false);
        client_->read_buffer_->helper_.seek_to_content();
        if (client_->read_buffer_->helper_.get_error_flag()) {
            process_error(client_);
            return;
        }

        std::vector<int32_t> nv;
        client_->read_buffer_->read_container_numbers(
                &nv, &typemap_, true);
        logger_->trace("Sync struct ID resp. count: {}", nv.size());
        for (unsigned int i=0; i < typemap_.struct_types_.size(); i++) {
            typemap_.sync_type_id(typemap_.struct_types_[i]->id_, nv[i]);
        }

        nv.clear();
        client_->read_buffer_->read_container_numbers(
                &nv, &typemap_, true);
        logger_->trace("Sync func ID resp. count: {}", nv.size());
        for (unsigned int i=0; i < typemap_.func_types_.size(); i++) {
            typemap_.sync_func_id(typemap_.func_types_[i]->id_, nv[i]);
        }

        isv.clear();
        client_->read_buffer_->read_container_wrapped_pointers(
                &isv, &typemap_, true);
        logger_->trace("Sync front ID resp. count: {}", isv.size());
        sync_front_other_ids(&isv);

        client_->read_buffer_->reset();

        logger_->debug("Sync ids finished");
    }


    int ClientProgram::recv_into_read_buffer(bool rewind) {
        int read_len = recv(
                socket_desc_,
                client_->read_buffer_->get_write_pointer(),
                client_->read_buffer_->get_available_write_size(), 0);
        if (read_len == -1) {
            throw std::runtime_error("ClientProgram socket read error");
        }
        else {
            client_->read_buffer_->add_written_len(read_len);
        }
        if (rewind) {
            client_->read_buffer_->rewind();
        }
        return read_len;
    }


    int ClientProgram::recv_into_read_buffer_full_message(bool rewind) {
        int total_read = 0;
        while (!client_->read_buffer_->helper_.have_full_message()) {
            total_read += recv_into_read_buffer(false);
        }
        if (rewind) {
            client_->read_buffer_->rewind();
        }
        return total_read;
    }


    void ClientProgram::on_client_disconnect() {
        logger_->info("Disconnected ({}:{})", host_.c_str(), port_);
        connection_state_ = disconnected;
        close(socket_desc_);
        for (auto& front : fronts_) {
            front->on_client_disconnect(client_);
            front->on_program_stop();
        }
        delete client_;
        client_ = nullptr;
    }


    void ClientProgram::loop_listen() {
        for (auto& front : fronts_) {
            front->on_client_connect(client_);
        }

        int read_len;
        struct timeval select_timeout={0};
        int select_result;
        fd_set read_fdset;
        while(!stop_flag_) {
            FD_ZERO(&read_fdset);
            FD_SET(socket_desc_, &read_fdset);

            select_timeout.tv_usec = 1; //0.1 * 1000000;

            select_result = select(
                    socket_desc_ + 1, &read_fdset,
                    nullptr, nullptr, &select_timeout);
            if ((select_result < 0) && (errno != EINTR)) {
                logger_->error("Client select error");
                continue;
            }
            if (!FD_ISSET(socket_desc_, &read_fdset)) {
                continue;
            }

            read_len = recv_into_read_buffer(false);

            if (read_len > 0) {
                client_->read_buffer_->add_written_len(read_len);
                try {
                    process_message(client_);
                    if (client_->get_force_disconnected()) {
                        stop_flag_ = 0;
                    }
                }
                catch (const std::exception& e) {
                    logger_->error("Failed to process input from server "
                        "{}:{}", host_.c_str(), port_);
                    stop_flag_ = 0;
                    client_->write_error_message("Error processing "
                            "server message");
                }
            }
            else {
                stop_flag_ = 0;
            }

        }

        on_client_disconnect();
    }


    void ClientProgram::stop() {
        if (connection_state_ == disconnected) {
            return;
        }

        stop_flag_ = true;
        thread_.join();
    }

}
