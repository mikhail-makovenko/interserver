#include <arpa/inet.h>
#include <cstdint>
#include <unistd.h>
#include <unordered_set>
#include "utils/sequence.hpp"
#include "buffer.hpp"
#include "spdlog/spdlog.h"


namespace interserver {

    /** This is a client wrapper base class that should be used on
     *  server side.
     */
    class ClientSocketWrapper {
    private:
        int32_t id_;
        int socket_desc_;
        struct sockaddr_in socket_address_;
        bool force_disconnected_ = false;

        /// if `true` then byte order and front IDs are synced
        bool byte_order_synced_ = false;

        /// same read or write buffer can be shared across many clients,
        /// so we have to keep client machine byte order separately from
        /// buffers
        bool changed_byte_order_ = false;

        static utils::IntSequence int_sequence_;

        std::unordered_set<int> known_front_ids_;
    public:
        std::shared_ptr<Buffer> read_buffer_;
        Buffer*                 write_buffer_;

        ClientSocketWrapper(int socket, struct sockaddr_in socket_address,
                std::shared_ptr<Buffer> read_buffer, Buffer* write_buffer);
        ClientSocketWrapper(const ClientSocketWrapper& socket) =delete;
        virtual ~ClientSocketWrapper() {};

        bool get_byte_order_synced() { return byte_order_synced_; }
        void set_byte_order_synced(bool synced) { byte_order_synced_ = synced; }

        void prepare_write_buffer();
        void prepare_read_buffer();
        void set_response_message_id();

        int get_id() { return id_; }
        int get_socket_desc() { return socket_desc_; }
        struct sockaddr_in get_socket_addr() { return socket_address_; }
        bool get_changed_byte_order() { return changed_byte_order_; }
        void set_changed_byte_order(bool changed_byte_order);

        void flush_write_buffer(bool set_message_size, int forward_count=-1);

        void force_disconnect();
        bool get_force_disconnected() { return force_disconnected_; };

        void write_error_message(const std::string& msg,
                std::shared_ptr<spdlog::logger> logger=nullptr, bool send=true);
        void read_buffer_remove_current_message() {
            read_buffer_->forward(read_buffer_->helper_.get_message_size());
        }

        bool front_is_known(int32_t id);
        void add_known_front(int32_t id);
    };


    utils::IntSequence ClientSocketWrapper::int_sequence_;


    ClientSocketWrapper::ClientSocketWrapper(
            int socket_desc, struct sockaddr_in socket_address,
            std::shared_ptr<Buffer> read_buffer, Buffer* write_buffer):
            id_(int_sequence_.next()),
            socket_desc_(socket_desc), socket_address_(socket_address),
            read_buffer_(read_buffer), write_buffer_(write_buffer) {}


    void ClientSocketWrapper::prepare_write_buffer() {
        write_buffer_->set_change_byte_order(changed_byte_order_);
    }


    void ClientSocketWrapper::prepare_read_buffer() {
        read_buffer_->set_change_byte_order(changed_byte_order_);
    }


    void ClientSocketWrapper::set_response_message_id() {
        write_buffer_->helper_.set_message_id(
                read_buffer_->helper_.get_message_id());
    }


    void ClientSocketWrapper::set_changed_byte_order(bool changed_byte_order) {
        if (changed_byte_order_ == changed_byte_order) {
            read_buffer_->reset();
            write_buffer_->reset();
            return;
        }
        changed_byte_order_ = changed_byte_order;
        read_buffer_->set_change_byte_order(changed_byte_order_);
        write_buffer_->set_change_byte_order(changed_byte_order_);
    }


    void ClientSocketWrapper::flush_write_buffer(
            bool set_message_size, int forward_count) {

        write_buffer_->helper_.flush_data_to_socket(
                socket_desc_, set_message_size, forward_count);
    }


    void ClientSocketWrapper::force_disconnect() {
        force_disconnected_ = true;
        close(socket_desc_);
    }


    void ClientSocketWrapper::write_error_message(
            const std::string& msg,
            std::shared_ptr<spdlog::logger> logger,
            bool send) {

        if (logger) {
            logger->error(msg);
        }
        set_response_message_id();
        write_buffer_->helper_.write_error_message(msg);
        if (send) {
            flush_write_buffer(true, -1);
        }
        force_disconnected_ = true;
    }


    bool ClientSocketWrapper::front_is_known(int32_t id) {
        return (known_front_ids_.find(id) != known_front_ids_.end());
    }


    void ClientSocketWrapper::add_known_front(int32_t id) {
        known_front_ids_.insert(id);
    }
}
