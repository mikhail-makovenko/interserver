#pragma once

#include <arpa/inet.h>
#include <thread>
#include <vector>

#include "interserver/settings.hpp"
#include "programbase.hpp"
#include "frontbase.hpp"


namespace interserver {


    class ServerProgram : public ProgramBase {
    private:
        std::shared_ptr<spdlog::logger>     logger_;

        std::vector<ClientSocketWrapper*>   clients_;

        int                 master_socket_ = 0;
        struct sockaddr_in  server_addr_;

        bool                stop_flag_ = false;
        bool                in_separate_thread_ = false;
        bool                started_ = false;
        std::thread         thread_;

        Buffer*             server_buffer_;

        void while_loop(int port);
        void on_program_stop();
        void sync_client_byte_order(ClientSocketWrapper* client);

        void on_client_connect(ClientSocketWrapper* client);
        void on_client_disconnect(ClientSocketWrapper* client);

        int32_t last_message_id = 0;
    protected:
            int32_t gen_message_id() {
                --last_message_id;
                if (last_message_id >= 0) {
                    last_message_id = -1;
                }
                return last_message_id;
            };
    public:
        std::shared_ptr<spdlog::logger> get_logger() {
            return logger_;
        };

        ServerProgram();
        ServerProgram(const ServerProgram& server) =delete;
        ~ServerProgram();

        void start_tcp(int port, bool in_separate_thread);
        void stop();
    };
}

