#pragma once

#include <cstdint>
#include <cstring>
#include <memory>
#include <vector>

#include "buffer_helper.hpp"
#include "typedefs.hpp"



namespace interserver {

    class TypeMap;


    /// `Buffer` class instances are used to store and receive Internet data.
    class Buffer {
    friend class BufferHelper;
    private:
        uint8_t*        buffer_;   //< Array of bytes
        int             pos_;
        int             size_;      //< Total capacity of the buffer
        int             used_;      //< Used size
        bool            change_byte_order_;

        int validate_newpos(int offset, int whence);
        void copy_reverse(uint8_t* dest, uint8_t* source, int length);

        void set_pos(int pos);
    public:
        BufferHelper    helper_;    //< Contains help functions

        Buffer(int size, bool change_byte_order=false);
        Buffer(const Buffer& buffer) =delete;
        ~Buffer();
        void reset();
        void clear_after(int32_t pos=-1);
        void set_change_byte_order(bool change_byte_order) {
            change_byte_order_ = change_byte_order;
            reset();
        }
        void write_direct(const void* source, int length=1);
        void read_direct(const void* dest, int length=1);
        void peek_direct(const void* dest, int length=1);

        void write_in_order(const void* source, int length=1);
        void read_in_order(const void* dest, int length=1);
        void peek_in_order(const void* dest, int length=1);

        void seek(int offset, int whence=0);
        void rewind();


        //----------------------------------------------------------------------

        // literals

        template <typename T>
        void write_value(const typename identity<T>::type& value);

        template <typename T>
        typename identity<T>::type read_value();

        template <typename T>
        typename identity<T>::type peek_value();


        // pointers

        template <typename T>
        void write_value(const T* value);

        template <typename T>
        void read_value(T* value);

        template <typename T>
        void peek_value(T* value);

        //----------------------------------------------------------------------


        void add_written_len(int size);

        /** Shifts data in the buffer to the front by `offset`.
         *
         * Shifts data in the buffer to the front by `offset` number of bytes.
         * Number of `offset` bytes from the end get zeroed.
         */
        void forward(int offset=-1);

        int get_size() { return size_; };
        int get_used() { return used_; };
        int get_pos() { return pos_; };
        int get_available_write_size() { return size_ - used_; };
        bool get_changed_byte_order() {
            return this->change_byte_order_;
        };
        uint8_t* get_write_pointer();
        uint8_t* get_buffer();

        /// T is a container of any wrapped pointers
        template <typename T>
        void write_container_wrapped_pointers(
                T* container, TypeMap* typemap, bool use_type_id);

        /// T is a container of any wrapped pointers
        template <typename T>
        void read_container_wrapped_pointers(
                T* container, TypeMap* typemap, bool use_type_id);

        // pure encodable

        template <typename T>
        void write_container_encodables(
                T* container, TypeMap* typemap, bool use_type_id);

        template <typename T>
        void read_container_encodables(
                T* container, TypeMap* typemap, bool use_type_id);

        // simple containers

        template <typename T>
        void write_container_numbers(
                T* container, TypeMap* typemap, bool use_type_id);

        template <typename T>
        void read_container_numbers(
                T* container, TypeMap* typemap, bool use_type_id);

        // strings

        void read_container_strings(
                std::vector<std::string>* container,
                TypeMap* typemap,
                bool use_type_id);

        void write_container_strings(
                std::vector<std::string>* container,
                TypeMap* typemap,
                bool use_type_id);

    };

    //--------------------------------------------------------------------------

    // pointers

    template <typename T>
    void Buffer::write_value(const T* value) {
        write_in_order(value, sizeof(T));
    }


    template <typename T>
    void Buffer::read_value(T* value) {
        read_in_order(value, sizeof(T));
    }


    template <typename T>
    void Buffer::peek_value(T* value) {
        peek_in_order(value, sizeof(T));
    }


    template <>
    void Buffer::write_value(const bool* value);


    template <>
    void Buffer::read_value(bool* value);


    template <>
    void Buffer::peek_value(bool* value);


    template <>
    void Buffer::write_value(const char* value);  // works with string literal


    template <>
    void Buffer::write_value(const std::string* value);


    template <>
    void Buffer::read_value(std::string* value);


    // literals

    template <typename T>
    void Buffer::write_value(const typename identity<T>::type& value) {
        write_value(&value);
    }


    template <typename T>
    typename identity<T>::type Buffer::read_value() {
        T value;
        read_value(&value);
        return value;
    }


    template <typename T>
    typename identity<T>::type Buffer::peek_value() {
        T value;
        peek_value(&value);
        return value;
    }


    template <>
    void Buffer::write_value<std::string>(
            const typename identity<std::string>::type& value);


    template <>
    typename identity<std::string>::type Buffer::read_value<std::string>();


    template <>
    void Buffer::write_value<bool>(
            const typename identity<bool>::type& value);


    template <>
    typename identity<bool>::type Buffer::read_value<bool>();


    template <>
    typename identity<bool>::type Buffer::peek_value<bool>();


    //--------------------------------------------------------------------------


}
