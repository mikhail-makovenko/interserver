#pragma once

#include <arpa/inet.h>
#include <unordered_set>

#include "utils/sequence.hpp"
#include "buffer.hpp"
#include "spdlog/spdlog.h"


namespace interserver {

    /** This is a client wrapper base class that should be used on
     *  server side.
     */
    class ClientSocketWrapper {
    private:
        int32_t id_;
        int socket_desc_;
        struct sockaddr_in socket_address_;
        bool force_disconnected_ = false;

        /// If `true` then byte order and front IDs are synced
        bool byte_order_synced_ = false;

        /// Same read or write buffer can be shared across many clients,
        /// so we have to keep client machine byte order in this class
        bool changed_byte_order_ = false;

        static utils::IntSequence int_sequence_;

        std::unordered_set<int> known_front_ids_;

    public:
        std::shared_ptr<Buffer> read_buffer_;
        Buffer*                 write_buffer_;

        ClientSocketWrapper(int socket, struct sockaddr_in socket_address,
                std::shared_ptr<Buffer> read_buffer, Buffer* write_buffer);
        ClientSocketWrapper(const ClientSocketWrapper& socket) =delete;
        virtual ~ClientSocketWrapper() {};

        void prepare_write_buffer();
        void prepare_read_buffer();
        void set_response_message_id();

        bool get_byte_order_synced() { return byte_order_synced_; }
        void set_byte_order_synced(bool synced) { byte_order_synced_ = synced; }

        int get_id() { return id_; }
        int get_socket_desc() { return socket_desc_; }
        struct sockaddr_in get_socket_addr() { return socket_address_; }
        bool get_changed_byte_order() { return changed_byte_order_; }
        void set_changed_byte_order(bool changed_byte_order);

        void flush_write_buffer(bool set_message_size, int forward_count=-1);

        void force_disconnect();
        bool get_force_disconnected() { return force_disconnected_; };

        void write_error_message(const std::string& error,
                std::shared_ptr<spdlog::logger> logger=nullptr, bool send=true);
        void read_buffer_remove_current_message() {
            read_buffer_->forward(read_buffer_->helper_.get_message_size());
        }

        bool front_is_known(int32_t id);
        void add_known_front(int32_t id);
    };
}
