#include <algorithm>
#include <arpa/inet.h>
#include <interserver/core/parts/server/fronts/server_core_front.hpp>
#include <cerrno>
#include <ctime>
#include <cstring>
#include <cstdio>
#include <system_error>
#include <thread>
#include <vector>
#include <unistd.h>

#include "interserver/settings.hpp"
#include "utils/endian.hpp"
#include "programbase.hpp"
#include "frontbase.hpp"


namespace interserver {


    class ServerProgram : public ProgramBase {
    private:
        std::shared_ptr<spdlog::logger>     logger_;

        std::vector<ClientSocketWrapper*>   clients_;

        int                 master_socket_ = 0;
        struct sockaddr_in  server_addr_;

        bool                stop_flag_ = false;
        bool                in_separate_thread_ = false;
        bool                started_ = false;
        std::thread         thread_;

        Buffer*             server_buffer_;

        void while_loop(int port);
        void on_program_stop();
        void sync_client_byte_order(ClientSocketWrapper* client);

        void on_client_connect(ClientSocketWrapper* client);
        void on_client_disconnect(ClientSocketWrapper* client);

        int32_t last_message_id = 0;
    protected:
        int32_t gen_message_id() {
            --last_message_id;
            if (last_message_id >= 0) {
                last_message_id = -1;
            }
            return last_message_id;
        };
    public:
        std::shared_ptr<spdlog::logger> get_logger() {
            return logger_;
        };

        ServerProgram();
        ServerProgram(const ServerProgram& server) =delete;
        ~ServerProgram();

        void start_tcp(int port, bool in_separate_thread);
        void stop();
    };


    ServerProgram::ServerProgram():
            logger_(gen_logger_st("Server", LOG_SEVERITY_SERVER)),
            server_buffer_(new Buffer(TCP_SERVER_BUFFER_SIZE, false)) {

        register_front(std::shared_ptr<FrontBase>(new ServerCoreFront()));
    }


    ServerProgram::~ServerProgram() {
        if (stop_flag_) {
            if (in_separate_thread_) {
                thread_.join();
            }
            else {
                // TODO: need to use condition variable...
                throw std::runtime_error("Tried to delete a server that is in "
                        "a process of stopping");
            }
        }
        else {
            if (started_) {
                if (in_separate_thread_) {
                    this->stop();
                }
                else {
                    // TODO: need to use condition variable...
                    close(master_socket_);
                    for (auto& client : clients_) {
                        close(client->get_socket_desc());
                        delete client;
                    }
                }
            }
        }
        delete server_buffer_;
    }


    void ServerProgram::on_program_stop() {
        close(master_socket_);

        for(auto& front : fronts_) {
            front->on_program_stop();
        }
        for(auto& client : clients_) {
            close(client->get_socket_desc());
            delete client;
        }
        clients_.clear();

        stop_flag_ = false;
        started_ = false;

        logger_->info(
                "Server stopped (port: {})",
                ntohs(server_addr_.sin_port));
    }


    void ServerProgram::start_tcp(int port, bool in_separate_thread) {
        if (started_) {
            if (stop_flag_) {
                throw std::runtime_error("Server has not yet stopped. "
                        "Tried listen to a port: " + std::to_string(port) +".");
            }
            throw std::runtime_error("Server already started. "
                    "Tried listen to a port: " + std::to_string(port) + ".");
        }
        started_ = true;
        stop_flag_  = false;

        master_socket_ = socket(AF_INET , SOCK_STREAM , 0);

        if (master_socket_ == -1) {
            throw std::runtime_error("Failed to create server socket desc");
        }


        int opt_value = 1;

        if(setsockopt(master_socket_, SOL_SOCKET, (SO_REUSEPORT | SO_REUSEADDR),
                &opt_value, sizeof(int)) == -1 ) {

            throw std::runtime_error(
                    "`setsockopt` failed (SO_REUSEPORT | SO_REUSEADDR)");
        }

        server_addr_.sin_family = AF_INET;
        server_addr_.sin_addr.s_addr = INADDR_ANY;
        server_addr_.sin_port = htons(port);
        if(bind(master_socket_,
                (struct sockaddr *)&server_addr_,
                sizeof(server_addr_)) == -1) {

            throw std::runtime_error("Failed to bind server socket");
        }

        if (listen(master_socket_ , 1024) == -1) {
            throw std::runtime_error("Server `listen` function failure");
        }

        logger_->info("Server starts (port: {})", port);

        in_separate_thread_ = in_separate_thread;
        if (!in_separate_thread_) {
            while_loop(port);
            return;
        }

        thread_ = std::thread(&ServerProgram::while_loop, this, port);
    }


    void ServerProgram::while_loop(int port) {
        int max_sd;
        int socket_desc;
        struct sockaddr_in socket_addr(server_addr_);
        int addr_len = sizeof(socket_addr);
        int read_len;
        int select_result;
        fd_set read_fdset;  //set of socket descriptors
        struct timeval select_timeout = {0, 0};
        ClientSocketWrapper* client;
        while(!stop_flag_) {
            FD_ZERO(&read_fdset);
            FD_SET(master_socket_, &read_fdset);
            max_sd = master_socket_;
            for (auto& client : clients_) {
                FD_SET(client->get_socket_desc(), &read_fdset);

                if(client->get_socket_desc() > max_sd) {
                    max_sd = client->get_socket_desc();
                }
            }

            if (in_separate_thread_) {

                select_timeout.tv_usec = 1; //0.1 * 1000000;

                select_result = select(
                    max_sd + 1, &read_fdset, nullptr, nullptr, &select_timeout);
            }
            else {
                select_result = select(
                        max_sd + 1, &read_fdset, nullptr, nullptr, nullptr);
            }

            if ((select_result < 0) && (errno != EINTR)) {
                logger_->error("Server select error");
                continue;
            }

            //If something happened on the master socket,
            // then its an incoming connection
            if (FD_ISSET(master_socket_, &read_fdset)) {
                socket_desc = accept(master_socket_,
                        (struct sockaddr *)&socket_addr,
                        (socklen_t*)&addr_len);
                if (socket_desc == -1) {
                    logger_->error("Server accept error");
                }
                else {
                    clients_.push_back(new ClientSocketWrapper(
                        socket_desc, socket_addr,
                        std::shared_ptr<Buffer>(
                            new Buffer(TCP_SERVER_CLIENT_BUFFER_SIZE, false)),
                        server_buffer_));

                    on_client_connect(clients_.back());
                }
            }


            // check IO operations on other sockets
            for (unsigned int i=0; i < clients_.size(); i++) {
                client = clients_[i];

                socket_desc = client->get_socket_desc();

                if (!FD_ISSET(socket_desc, &read_fdset)) {
                    continue;
                }

                //client->prepare_read_buffer();
                read_len = recv(socket_desc,
                        client->read_buffer_->get_write_pointer(),
                        client->read_buffer_->get_available_write_size(), 0);

                if ((read_len == 0) || (read_len < 0)) {
                    on_client_disconnect(client);
                    i--;        // go back;
                    continue;   // no need to process this client any further
                }
                // got some message from the client
                client->read_buffer_->add_written_len(read_len);
                try {
                    // syncing byte order if needed
                    if (!client->get_byte_order_synced()) {
                        sync_client_byte_order(client);
                        continue;
                    }
                    process_message(client);
                    if (client->get_force_disconnected()) {
                        on_client_disconnect(client);
                        i--;    // go back;
                        continue;
                    }
                }
                catch (const std::exception& e) {
                    logger_->error("Failed to process input from client "
                        "#{:06d} (ip: {} , port {}): {}",
                                client->get_id(),
                                inet_ntoa(client->get_socket_addr().sin_addr),
                                ntohs(client->get_socket_addr().sin_port),
                                e.what());
                    client->write_error_message("Error processing "
                            "client message");
                    on_client_disconnect(client);
                    i--;        // go back;
                    continue;
                }

            }

        }

        on_program_stop();
    }

    void ServerProgram::stop() {
        if (!started_) {
            return;
        }
        if (!in_separate_thread_) {
            throw std::runtime_error("Server stop: Not in a separate thread");
        }
        stop_flag_ = true;
        thread_.join();
    }


    void ServerProgram::on_client_connect(ClientSocketWrapper* client) {
        logger_->info("Connected #{:06d} ({}:{})",
                client->get_id(),
                inet_ntoa(client->get_socket_addr().sin_addr),
                ntohs(client->get_socket_addr().sin_port));
        logger_->info("Total number of connections: {}", int(clients_.size()));

        for(auto& front : this->fronts_) {
            front->on_client_connect(client);
        }
    }


    void ServerProgram::on_client_disconnect(
            ClientSocketWrapper* client) {

        close(client->get_socket_desc());
        logger_->info("Disconnected #{:06d} ({}:{})",
                client->get_id(),
                inet_ntoa(client->get_socket_addr().sin_addr),
                ntohs(client->get_socket_addr().sin_port));

        for(auto& front : this->fronts_)
        {
            front->on_client_disconnect(client);
        }

        auto pos = find(clients_.begin(), clients_.end(), client)
                - clients_.begin();
        // we only need to be sure that all clients pass the deleted one
        // do not go before the deleted one (for the server loop)
        std::swap(clients_[pos], clients_.back());
        clients_.pop_back();
        delete client;

        logger_->info("Total number of connections: {}", int(clients_.size()));
    }


    void ServerProgram::sync_client_byte_order(ClientSocketWrapper* client) {
        logger_->trace("`sync_client_byte_order`: #{:06d}", client->get_id());

        uint8_t command;
        client->read_buffer_->seek(0);
        client->read_buffer_->read_value(&command);

        bool use_little_endian, changed_byte_order;
        switch(command) {
        // little
        case 2:
            changed_byte_order = !utils::IS_LITTLE_ENDIAN;
            use_little_endian = true;
            client->set_changed_byte_order(changed_byte_order);
            break;
        // big
        case 3:
            changed_byte_order = utils::IS_LITTLE_ENDIAN;
            use_little_endian = false;
            client->set_changed_byte_order(changed_byte_order);
            break;
        // use server byte order
        default:
            changed_byte_order = false;
            use_little_endian = utils::IS_LITTLE_ENDIAN;
            client->set_changed_byte_order(changed_byte_order);
            if (command != 1) { // not "case 1"
                logger_->error("Unknown sync command: {}", command);
            }
        }

        client->set_changed_byte_order(changed_byte_order);
        client->set_byte_order_synced(true);
        client->write_buffer_->write_value(&use_little_endian);

        client->flush_write_buffer(false);
    }

}
