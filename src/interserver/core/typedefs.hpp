#pragma once


namespace interserver {


    class Buffer;
    class TypeMap;

    typedef void (*buffer_only_func)(Buffer* buffer);
    typedef void (*typemap_only_func)(TypeMap* typemap);


    template <typename T>
    struct identity {
        typedef T type;
    };

}

