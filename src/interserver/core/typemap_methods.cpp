#include <unordered_map>
#include <string>
#include <vector>

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "typemap.hpp"


namespace interserver {

    StructType* TypeMap::add_struct(
            const std::string& struct_name, int32_t struct_id) {
        if (has_type_name(struct_name)) {
            throw std::runtime_error("Type with name \"" + struct_name +
                    "\" already registered in this typemap");
        }
        if (has_type_id(struct_id)) {
            throw std::runtime_error("Type with id \"" +
                    std::to_string(struct_id) +
                    "\" already registered in this typemap");
        }
        auto new_struct_type = std::shared_ptr<StructType>(
                new StructType(struct_name, struct_id));
        this->struct_types_.push_back(new_struct_type);
        return new_struct_type.get();
    }


    FuncType* TypeMap::add_func(
            const std::string& func_name, int32_t func_id) {
        if (has_func_name(func_name)) {
            throw std::runtime_error("Function with name \"" + func_name +
                    "\" already registered in this typemap");
        }
        if (has_func_id(func_id)) {
            throw std::runtime_error("Function with id \"" +
                    std::to_string(func_id) +
                    "\" already registered in this typemap");
        }
        auto new_func_type = std::shared_ptr<FuncType>(
                new FuncType(func_name, func_id));
        this->func_types_.push_back(new_func_type);
        return new_func_type.get();
    }


    std::string TypeMap::to_json() {
        rapidjson::StringBuffer json_string_buffer;
        rapidjson::Writer<rapidjson::StringBuffer>
            json_writer(json_string_buffer);

        json_writer.StartObject();
        json_writer.Key("baseTypes");
        json_writer.StartArray();

        for (const auto& base_type : base_types_) {
            json_writer.StartObject();

            json_writer.Key("id");
            json_writer.Int(base_type->id_);

            json_writer.Key("name");
            json_writer.String(base_type->name_.c_str());

            json_writer.Key("size");
            json_writer.Int(base_type->size_);

            json_writer.EndObject();
        }

        json_writer.EndArray(); // baseTypes
        json_writer.Key("structTypes");
        json_writer.StartArray();

        for (const auto& struct_type : struct_types_) {
            json_writer.StartObject();

            json_writer.Key("id");
            json_writer.Int(struct_type->id_);

            json_writer.Key("name");
            json_writer.String(struct_type->name_.c_str());

            json_writer.Key("fields");
            json_writer.StartArray();

            for (const auto& field : struct_type->fields_) {
                json_writer.StartObject();

                json_writer.Key("name");
                json_writer.String(field->name_.c_str());

                json_writer.Key("typeId");
                json_writer.Int(field->type_id_);

                json_writer.EndObject();
            }


            json_writer.EndArray(); // fields
            json_writer.EndObject();
        }

        json_writer.EndArray(); // structTypes
        json_writer.Key("funcTypes");
        json_writer.StartArray();
        for (const auto& func_type : func_types_) {
            json_writer.StartObject();

            json_writer.Key("name");
            json_writer.String(func_type->name_.c_str());

            json_writer.Key("id");
            json_writer.Int(func_type->id_);

            json_writer.Key("args");
            json_writer.StartArray();

            for (const auto& arg : func_type->args_) {
                json_writer.StartObject();

                json_writer.Key("name");
                json_writer.String(arg->name_.c_str());

                json_writer.Key("type_id");
                json_writer.Int(arg->type_id_);

                json_writer.EndObject();
            }

            json_writer.EndArray();

            json_writer.EndObject();
        }
        json_writer.EndArray(); // funcTypes

        json_writer.EndObject(); // root

        return json_string_buffer.GetString();
    }


    void TypeMap::sync(Buffer* buffer, bool use_type_id) {
        auto typemap = TypeMap(buffer, use_type_id);
        sync(&typemap);
    }


    void TypeMap::sync(const TypeMap* typemap) {
        synced_type_ids_.clear();

        // sync base types
        for (const auto& this_base_type : this->base_types_) {
            for (const auto& that_base_type : typemap->base_types_) {
                if (this_base_type->name_ == that_base_type->name_) {
                    synced_type_ids_[this_base_type->id_] =
                            that_base_type->id_;
                    break;
                }
            }
        }
        // sync struct types
        for (const auto& this_struct_type : this->struct_types_) {
            for (const auto& that_struct_type : typemap->struct_types_) {
                if (this_struct_type->name_ == that_struct_type->name_) {
                    synced_type_ids_[this_struct_type->id_] =
                            that_struct_type->id_;
                    break;
                }
            }
        }

        // sync func types
        for (const auto& this_func_type : this->func_types_) {
            for (const auto& that_func_type : typemap->func_types_) {
                if (this_func_type->name_ == that_func_type->name_) {
                    synced_func_ids_[this_func_type->id_] =
                            that_func_type->id_;
                    break;
                }
            }
        }
    }


    int32_t TypeMap::get_synced_type_id(int32_t not_synced_id) {
        auto it = synced_type_ids_.find(not_synced_id);
        if (it != synced_type_ids_.end()) {
            return it->second;
        }
        return not_synced_id;
    }


    int32_t TypeMap::get_synced_func_id(int32_t not_synced_id) {
        auto it = synced_func_ids_.find(not_synced_id);
        if (it != synced_func_ids_.end()) {
            return it->second;
        }
        return not_synced_id;
    }


    StructType* TypeMap::get_struct_by_name(const std::string& name) {
        for (const auto& strct : struct_types_) {
            if (strct->name_ == name) {
                return (StructType*)strct.get();
            }
        }
        return nullptr;
    }


    FuncType* TypeMap::get_func_by_name(const std::string& name) {
        for (const auto& fnct : func_types_) {
            if (fnct->name_ == name) {
                return (FuncType*)fnct.get();
            }
        }
        return nullptr;
    }


    void TypeMap::sync_type_id(int32_t not_synced_id, int32_t synced_id) {
        synced_type_ids_[not_synced_id] = synced_id;
    }


    void TypeMap::sync_func_id(int32_t not_synced_id, int32_t synced_id) {
        synced_func_ids_[not_synced_id] = synced_id;
    }


    bool TypeMap::has_type_name(std::string name) {
        for (const auto& strct : struct_types_) {
            if (strct->name_ == name) {
                return true;
            }
        }
        for (const auto& bs : base_types_) {
            if (bs->name_ == name) {
                return true;
            }
        }
        return false;
    }


    bool TypeMap::has_type_id(int32_t id) {
        for (const auto& strct : struct_types_) {
            if (strct->id_ == id) {
                return true;
            }
        }
        for (const auto& bs : base_types_) {
            if (bs->id_ == id) {
                return true;
            }
        }
        return false;
    }


    bool TypeMap::has_func_name(std::string name) {
        for (const auto& fnc : func_types_) {
            if (fnc->name_ == name) {
                return true;
            }
        }
        return false;
    }


    bool TypeMap::has_func_id(int32_t id) {
        for (const auto& fnc : func_types_) {
            if (fnc->id_ == id) {
                return true;
            }
        }
        return false;
    }

}
