#pragma once

#include <memory>
#include <vector>
#include "spdlog/spdlog.h"
#include "frontbase.hpp"
#include "typemap.hpp"
#include "interserver/core/msgtypes/msgtypes_common.hpp"


namespace interserver {


    class ProgramBase {
    // for doing sync ids
    friend class ServerCoreFront;
    protected:
        std::vector<std::shared_ptr<FrontBase>>     fronts_;
        TypeMap                                     typemap_;

        virtual int32_t gen_message_id() =0;
        void process_message(ClientSocketWrapper* client);

        void collect_front_names(std::vector<std::string>* v);
        void collect_front_names_and_ids(
                std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v);
        void sync_front_other_ids(std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v);
    public:
        virtual ~ProgramBase();

        virtual std::shared_ptr<spdlog::logger> get_logger() {
            static std::shared_ptr<spdlog::logger> logger =
                gen_logger_st("ProgramBase(default)", LOG_SEVERITY_PROGRAMBASE);
            return logger;
        };

        TypeMap* get_typemap() {
            return &typemap_;
        };

        int32_t get_synced_type_id(int32_t id) {
            return typemap_.get_synced_type_id(id);
        };
        int32_t get_synced_func_id(int32_t id) {
            return typemap_.get_synced_func_id(id);
        };

        FrontBase* get_front_by_id(int32_t id, bool id_synced);
        FrontBase* get_front_by_name(std::string name);

        void register_front(std::shared_ptr<FrontBase> front);
        void __unregister_front(std::shared_ptr<FrontBase> front);
        void __unregister_all_fronts();

        virtual void process_error(ClientSocketWrapper* client);
        void read_function_call(ClientSocketWrapper* client, bool use_type_id);

        virtual void stop() =0;
    };

}
