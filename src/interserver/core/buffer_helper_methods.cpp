#include <arpa/inet.h>
#include <cstdint>

#include "buffer.hpp"
#include "buffer_helper.hpp"


namespace interserver {

    bool BufferHelper::have_full_message() {
        if (buffer_->used_ < 4) {
            return false;
        }
        return (buffer_->helper_.get_message_size() <= buffer_->get_used());
    }


    void BufferHelper::flush_data_to_socket(int socket_desc,
            bool set_message_size,
            int forward_count) {
        if (forward_count == -1) {
            forward_count = buffer_->used_;
        }
        if (set_message_size) {
            int pos = buffer_->pos_;
            buffer_->pos_ = 0;
            buffer_->write_value(&buffer_->used_);
            buffer_->pos_ = pos;
        }
        if (send(socket_desc, buffer_->buffer_, forward_count, 0) < 0) {
            throw std::runtime_error("Failed to send data to the server");
        }

        buffer_->forward(forward_count);
    }


    int32_t BufferHelper::get_message_size() {
        int pos = buffer_->pos_;
        // skipping message size
        buffer_->seek(0);
        int32_t size;
        buffer_->read_value(&size);
        buffer_->pos_ = pos;
        return size;
    }


    void BufferHelper::set_message_id(int32_t id) {
        int pos = buffer_->pos_;
        // skipping message size
        buffer_->seek(sizeof(int32_t));
        buffer_->write_value(&id);
        buffer_->pos_ = pos;
    }


    int32_t BufferHelper::get_message_id() {
        int pos = buffer_->pos_;
        // skipping message size
        buffer_->seek(sizeof(int32_t));
        int32_t id;
        buffer_->read_value(&id);
        buffer_->pos_ = pos;
        return id;
    }


    void BufferHelper::set_error_flag(bool flag) {
        int pos = buffer_->pos_;
        // skipping message size, message id
        buffer_->seek(sizeof(int32_t) + sizeof(int32_t));
        buffer_->write_value(&flag);
        buffer_->pos_ = pos;
    }


    bool BufferHelper::get_error_flag() {
        int pos = buffer_->pos_;
        // skipping message size, message id
        buffer_->seek(sizeof(int32_t) + sizeof(int32_t));
        bool flag;
        buffer_->read_value(&flag);
        buffer_->pos_ = pos;
        return flag;
    }


    void BufferHelper::write_error_message(const std::string& msg) {
        set_error_flag(true);
        buffer_->seek(sizeof(int32_t) + sizeof(int32_t) + 1);   // bool_size=1
        buffer_->clear_after();
        buffer_->write_value(&msg);
    }


    void BufferHelper::seek_to_content() {
        // skipping message size, message id, error_flag, front_id
        buffer_->seek(sizeof(int32_t) + sizeof(int32_t) + 1);   // bool_size=1
    }
}
