#pragma once

#include "interserver/settings.hpp"
#include "interserver/core/frontbase.hpp"
#include "spdlog/spdlog.h"


namespace interserver {

    class ServerCoreFront : public ServerFrontBaseCustomLogger {
    private:
        std::vector<std::shared_ptr<msgtypes::IntegerAndString>>
                front_names_and_ids_;
    public:
        ServerCoreFront();

        void read_function_call(ClientSocketWrapper* client);
        void typemap_register(TypeMap* typemap);

        //== const_id=0
        void f_sync_ids(ClientSocketWrapper* client);
    };

}

