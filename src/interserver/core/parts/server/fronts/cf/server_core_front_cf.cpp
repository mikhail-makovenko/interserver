#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/clientsocketwrapper.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "../server_core_front.hpp"


namespace interserver {
namespace CF {
namespace S {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_IDS_ID = 0;

} } } }


namespace interserver { 

    //--------------------------------------------------------------------------

    void ServerCoreFront::read_function_call(
            ::interserver::ClientSocketWrapper* client) {

        int32_t func_id;
        client->read_buffer_->read_value(&func_id);
        func_id = get_program()->get_synced_func_id(func_id);

        if (func_id == ::interserver::CF::S::ServerCoreFront::F_SYNC_IDS_ID) { 
            f_sync_ids(client);
        }
        else {
            client->write_error_message("Unknown function ID: " +
                    std::to_string(func_id), get_logger());
        }
    }

    //--------------------------------------------------------------------------

    void ServerCoreFront::typemap_register(TypeMap* typemap) {
        if (typemap == nullptr) {
            return;
        }

        
        typemap->add_func(
            "::interserver::CF::S::ServerCoreFront::f_sync_ids",
            ::interserver::CF::S::ServerCoreFront::F_SYNC_IDS_ID);
    
    }

}