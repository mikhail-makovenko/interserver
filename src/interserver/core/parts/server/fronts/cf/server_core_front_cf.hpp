#pragma once

#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/clientsocketwrapper.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "../server_core_front.hpp"


namespace interserver {
namespace CF {
namespace S {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_IDS_ID = 0;

} } } }


namespace interserver { 

    //--------------------------------------------------------------------------

    void ServerCoreFront::read_function_call(
            ::interserver::ClientSocketWrapper* client);
                
    //--------------------------------------------------------------------------

    void ServerCoreFront::typemap_register(TypeMap* typemap);

}