#include <algorithm>
#include <vector>
#include "interserver/settings.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/buffer_methods.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/msgtypes/msgtypes_common.hpp"
#include "spdlog/spdlog.h"


namespace interserver {

    class ServerCoreFront : public ServerFrontBaseCustomLogger {
    private:
        std::vector<std::shared_ptr<msgtypes::IntegerAndString>>
                front_names_and_ids_;
    public:
        ServerCoreFront();

        void read_function_call(ClientSocketWrapper* client);
        void typemap_register(TypeMap* typemap);

        //!!constant_id -1
        void f_sync_ids(ClientSocketWrapper* client);
    };



    ServerCoreFront::ServerCoreFront():
        ServerFrontBaseCustomLogger(
            "::interserver::CoreFront",
            0,  // id
            LOG_SEVERITY_SERVER_CORE_FRONT) {}


    void ServerCoreFront::f_sync_ids(ClientSocketWrapper* client) {
        if (front_names_and_ids_.size() == 0) {
            get_program()->collect_front_names_and_ids(
                    &front_names_and_ids_);
            get_logger()->trace("Number of front names and IDs: {}",
                    front_names_and_ids_.size());
        }

        std::vector<std::string> sv;
        std::vector<int32_t> nv;
        auto typemap = get_program()->get_typemap();
        client->read_buffer_->read_container_strings(&sv, typemap, true);
        get_logger()->trace("Sync struct names count: {}", sv.size());

        StructType* strct;
        for (const auto& name : sv) {
            strct = typemap->get_struct_by_name(name);
            if (strct == nullptr) {
                client->write_error_message("Sync unknown struct: " + name,
                        get_logger(), true);
                return;
            }
            nv.push_back(strct->id_);
        }
        client->write_buffer_->write_container_numbers(&nv, typemap, true);

        sv.clear();
        nv.clear();
        client->read_buffer_->read_container_strings(&sv, typemap, true);
        get_logger()->trace("Sync func names count: {}", sv.size());

        FuncType* fnct;
        for (const auto& name : sv) {
            fnct = typemap->get_func_by_name(name);
            if (fnct == nullptr) {
                client->write_error_message("Sync unknown func: " + name,
                        get_logger(), true);
                return;
            }
            nv.push_back(fnct->id_);
        }
        client->write_buffer_->write_container_numbers(&nv, typemap, true);

        sv.clear();
        client->read_buffer_->read_container_strings(&sv, typemap, true);
        get_logger()->trace("Sync front names count: {}", sv.size());
        std::vector<std::shared_ptr<msgtypes::IntegerAndString>> isv;
        for (const auto& name_id : front_names_and_ids_) {
            if (std::find(sv.begin(), sv.end(), name_id->string_) != sv.end()) {
                isv.push_back(name_id);
                client->add_known_front(name_id->integer_);
            }
        }
        client->write_buffer_->write_container_wrapped_pointers(
                &isv, typemap, true);

        client->flush_write_buffer(true);
    }

}
