#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"


namespace interserver {
namespace WF {
namespace S {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_IDS_ID = 0;

    //--------------------------------------------------------------------------

    void f_sync_ids(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer) {
        int32_t temp = program->get_synced_type_id(
            ::interserver::MAPTYPE_FUNCTION_CALL);
        buffer->write_value(&temp);
        temp = 0;
        if (front) {
            temp = front->get_other_id();
        }
        buffer->write_value(&temp);
        temp = program->get_synced_func_id(F_SYNC_IDS_ID);
        buffer->write_value(&temp);

    }

    //--------------------------------------------------------------------------



    void typemap_register(TypeMap* typemap) {
        
        typemap->add_func(
            "::interserver::CF::S::ServerCoreFront::f_sync_ids",
            F_SYNC_IDS_ID);
    
    }

} } } }