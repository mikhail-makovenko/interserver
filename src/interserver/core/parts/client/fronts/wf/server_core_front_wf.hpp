#pragma once

#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"


namespace interserver {
namespace WF {
namespace S {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_IDS_ID = 0;

    //--------------------------------------------------------------------------

    void f_sync_ids(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer);

    //--------------------------------------------------------------------------



    void typemap_register(TypeMap* typemap);

} } } }