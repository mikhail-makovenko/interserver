#include <cstdint>
#include <string>
#include "buffer.hpp"
#include "clientsocketwrapper.hpp"
#include "spdlog/spdlog.h"
#include "interserver/settings.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/msgtypes/msgtypes_common.hpp"


namespace interserver {


    void FrontBase::on_register(ProgramBase* program) {
        program_ = program;
        for (auto& front : fronts_) {
            front->on_register(program);
        }
        get_logger()->debug("Registered (" + name_ + ")");
    }


    void FrontBase::on_unregister(ProgramBase* program) {
        program_ = nullptr;
        for (auto& front : fronts_) {
            front->on_unregister(program);
        }
        get_logger()->debug("Unregistered (" + name_ + ")");
    }


    void FrontBase::collect_front_names(std::vector<std::string>* v) {
        v->push_back(name_);
        for (const auto& front : fronts_) {
            front->collect_front_names(v);
        }
    }


    void FrontBase::collect_front_names_and_ids(
            std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v) {
        v->push_back(std::shared_ptr<msgtypes::IntegerAndString>(
                new msgtypes::IntegerAndString(id_, name_)));
        for (const auto& front : fronts_) {
            front->collect_front_names_and_ids(v);
        }
    }


    void FrontBase::sync_front_other_ids(
            std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v) {
        // looking for our name
        for (const auto& int_and_str : *v) {
            if (int_and_str->string_ == name_) {
                set_other_id(int_and_str->integer_);
                break;
            }
        }

        for (const auto& front : fronts_) {
            front->sync_front_other_ids(v);
        }
    }

}
