#pragma once

#include "buffer.hpp"
#include "typemap.hpp"


namespace interserver {


    template <typename T>
    void Buffer::write_container_wrapped_pointers(
            T* container, TypeMap* typemap, bool use_type_id) {

        if (use_type_id) {
            int32_t type_array_id;
            if (typemap) {
                type_array_id = typemap->get_synced_type_id(MAPTYPE_ARRAY);
            }
            else {
                type_array_id = MAPTYPE_ARRAY;
            }
            write_value(&type_array_id);
            int32_t id =
                    T::value_type::element_type::static_get_type_id(typemap);
            write_value(&id);
        }
        int32_t container_size = container->size();
        write_value(&container_size);

        for (auto const& message : *container) {
            message->encode(this, typemap, false);
        }
    }


    template <typename T>
    void Buffer::read_container_wrapped_pointers(
            T* container, TypeMap* typemap, bool use_type_id) {

        if (use_type_id) {
            int32_t temp;
            read_value(&temp);
            int32_t maptype = MAPTYPE_ARRAY;
            if (typemap) maptype = typemap->get_synced_type_id(maptype);
            if (temp != maptype) {
                throw std::runtime_error("Tried to read an array from buffer, "
                        "but data type is wrong");
            }
            read_value(&temp);
            if (temp != T::value_type::element_type::static_get_type_id(
                    typemap)) {
                throw std::runtime_error("Tried to read an array of "
                        "wrapped pointers from buffer, but data type is wrong");
            }
        }
        int32_t container_size;
        read_value(&container_size);
        container->reserve(container->size() + container_size);

        for (int i=0; i < container_size; i++) {
            container->emplace_back(new typename T::value_type::element_type(
                    this, typemap, false));
        }
    };


    // pure encodable

    template <typename T>
    void Buffer::write_container_encodables(
            T* container, TypeMap* typemap, bool use_type_id) {
        if (use_type_id) {
            int32_t type_array_id;
            if (typemap) {
                type_array_id = typemap->get_synced_type_id(MAPTYPE_ARRAY);
            }
            else {
                type_array_id = MAPTYPE_ARRAY;
            }
            write_value(&type_array_id);
            auto id = T::value_type::static_get_type_id(typemap);
            write_value(&id);
        }
        int32_t container_size = container->size();
        write_value(&container_size);

        for (auto const& message : *container) {
            message.encode(this, typemap, false);
        }
    }


    template <typename T>
    void Buffer::read_container_encodables(
            T* container, TypeMap* typemap, bool use_type_id) {

        if (use_type_id) {
            int32_t temp;
            read_value(&temp);
            int32_t maptype = MAPTYPE_ARRAY;
            if (typemap) maptype = typemap->get_synced_type_id(maptype);
            if (temp != maptype) {
                throw std::runtime_error("Tried to read an array from buffer, "
                        "but data type is wrong");
            }
            read_value(&temp);
            if (temp != T::value_type::static_get_type_id(
                    typemap)) {
                throw std::runtime_error("Tried to read an array of "
                        "encodables from buffer, but data type is wrong");
            }
        }
        int32_t container_size;
        read_value(&container_size);
        container->reserve(container->size() + container_size);

        for (int i=0; i < container_size; i++) {
            container->emplace_back(this, typemap, false);
        }
    }


    template <typename T>
    void Buffer::write_container_numbers(
            T* container, TypeMap* typemap, bool use_type_id) {

        if (use_type_id) {
            int32_t type_array_id;
            int32_t type_id;
            if (typemap) {
                type_array_id = typemap->get_synced_type_id(MAPTYPE_ARRAY);
                type_id = typemap->get_synced_type_id(
                        TYPEMAP_ID<typename T::value_type>::ID);
            }
            else {
                type_array_id = MAPTYPE_ARRAY;
                type_id = TYPEMAP_ID<typename T::value_type>::ID;
            }
            write_value(&type_array_id);
            write_value(&type_id);
        }
        int32_t container_size = container->size();
        write_value(&container_size);

        for (auto const& number : *container) {
            write_value(&number);
        }
    }


    template <typename T>
    void Buffer::read_container_numbers(
            T* container, TypeMap* typemap, bool use_type_id) {

        if (use_type_id) {
            int32_t temp;
            read_value(&temp);
            int32_t maptype = MAPTYPE_ARRAY;
            if (typemap) maptype = typemap->get_synced_type_id(maptype);
            if (temp != maptype) {
                throw std::runtime_error("Tried to read an array from buffer, "
                        "but data type is wrong");
            }
            read_value(&temp);
            maptype = TYPEMAP_ID<typename T::value_type>::ID;
            if (typemap) maptype = typemap->get_synced_type_id(maptype);
            if (temp != maptype) {
                throw std::runtime_error("Tried to read an array of numbers "
                        " from buffer, but data type is wrong");
            }
        }

        int32_t container_size;
        read_value(&container_size);
        container->reserve(container->size() + container_size);
        typename T::value_type temp;
        for (int i=0; i < container_size; i++) {
            read_value(&temp);
            container->push_back(temp);
        }
    }

}
