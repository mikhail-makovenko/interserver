#pragma once


namespace interserver { namespace utils {

    union Union2Bytes {
        uint8_t b[2];
        uint16_t v;
    };

    union Union4Bytes {
        uint8_t b[4];
        uint32_t v;
    };

    union Union8Bytes {
        uint8_t b[4];
        uint32_t v;
    };


    class ByteOrderReverser {
    public:
        void reverse_2_bytes(void* ptr);
        void reverse_4_bytes(void* ptr);
        void reverse_8_bytes(void* ptr);
    private:
        Union2Bytes union_2_bytes_;
        Union4Bytes union_4_bytes_;
        Union8Bytes union_8_bytes_;

        Union2Bytes* union_2_bytes_ptr_;
        Union4Bytes* union_4_bytes_ptr_;
        Union8Bytes* union_8_bytes_ptr_;
    };
} }
