#pragma once

namespace interserver { namespace utils {

    bool is_little_endian();

    const bool IS_LITTLE_ENDIAN = is_little_endian();

} }
