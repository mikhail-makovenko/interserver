#include <cstdint>

namespace interserver { namespace utils {

    union Union2Bytes {
        uint8_t b[2];
        uint16_t v;
    };

    union Union4Bytes {
        uint8_t b[4];
        uint32_t v;
    };

    union Union8Bytes {
        uint8_t b[4];
        uint32_t v;
    };


    class ByteOrderReverser {
    private:
        Union2Bytes union_2_bytes_;
        Union4Bytes union_4_bytes_;
        Union8Bytes union_8_bytes_;

        Union2Bytes* union_2_bytes_ptr_;
        Union4Bytes* union_4_bytes_ptr_;
        Union8Bytes* union_8_bytes_ptr_;
    public:
        void reverse_2_bytes(void* ptr);
        void reverse_4_bytes(void* ptr);
        void reverse_8_bytes(void* ptr);
    };


    void ByteOrderReverser::reverse_2_bytes(void* ptr) {
        this->union_2_bytes_ptr_ = (Union2Bytes*)ptr;
        this->union_2_bytes_ = *this->union_2_bytes_ptr_;
        this->union_2_bytes_ptr_->b[0] = this->union_2_bytes_.b[1];
        this->union_2_bytes_ptr_->b[1] = this->union_2_bytes_.b[0];
    }


    void ByteOrderReverser::reverse_4_bytes(void* ptr) {
        this->union_4_bytes_ptr_ = (Union4Bytes*)ptr;
        this->union_4_bytes_ = *this->union_4_bytes_ptr_;
        this->union_4_bytes_ptr_->b[0] = this->union_4_bytes_.b[3];
        this->union_4_bytes_ptr_->b[1] = this->union_4_bytes_.b[2];
        this->union_4_bytes_ptr_->b[2] = this->union_4_bytes_.b[1];
        this->union_4_bytes_ptr_->b[3] = this->union_4_bytes_.b[0];
    }


    void ByteOrderReverser::reverse_8_bytes(void* ptr) {
        this->union_8_bytes_ptr_ = (Union8Bytes*)ptr;
        this->union_8_bytes_ = *this->union_8_bytes_ptr_;
        this->union_8_bytes_ptr_->b[0] = this->union_8_bytes_.b[7];
        this->union_8_bytes_ptr_->b[1] = this->union_8_bytes_.b[6];
        this->union_8_bytes_ptr_->b[2] = this->union_8_bytes_.b[5];
        this->union_8_bytes_ptr_->b[3] = this->union_8_bytes_.b[4];
        this->union_8_bytes_ptr_->b[4] = this->union_8_bytes_.b[3];
        this->union_8_bytes_ptr_->b[5] = this->union_8_bytes_.b[2];
        this->union_8_bytes_ptr_->b[6] = this->union_8_bytes_.b[1];
        this->union_8_bytes_ptr_->b[7] = this->union_8_bytes_.b[0];
    }

} }
