#pragma once

#include <cstdint>


namespace interserver { namespace utils {

    class IntSequence {
    private:
        int32_t value_;
    public:
        IntSequence(int32_t start_value=0);
        int32_t next();
        void set_last(int32_t last_value);
    };

} }
