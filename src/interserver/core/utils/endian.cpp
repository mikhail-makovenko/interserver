#include <cstdint>


namespace interserver { namespace utils {

    bool is_little_endian() {
        union {
            uint16_t i;
            uint8_t c[2];
        } bint = {0x0102};

        return bint.c[0] == 2;
    }

    const bool IS_LITTLE_ENDIAN = is_little_endian();

} }
