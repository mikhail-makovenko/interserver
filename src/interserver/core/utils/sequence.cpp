#include <cstdint>
#include <stdio.h>

namespace interserver { namespace utils {

    class IntSequence {
    private:
        int32_t value_;
    public:
        IntSequence(int32_t start_value=0);
        int32_t next();
        void set_last(int32_t last_value);
    };


    IntSequence::IntSequence(int32_t start_value): value_(start_value) {}


    int32_t IntSequence::next() {
        return ++this->value_;
    }


    void IntSequence::set_last(int32_t last_value) {
        this->value_ = last_value;
    }


} }
