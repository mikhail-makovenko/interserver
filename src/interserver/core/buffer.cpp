#include <cstdint>
#include <cstring>
//#include <exception>
#include <system_error>
#include <vector>

#include "buffer_helper.hpp"
#include "typedefs.hpp"



namespace interserver {
    /*
    class BufferException: public std::exception
    {
    private:
        std::string msg;
    public:
        BufferException(const std::string& msg) : msg(msg) {}
        virtual const char* what() const throw()
        {
            return this->msg.c_str();
        }
    };
    */


    class TypeMap;


    /// `Buffer` class instances are used to store and receive Internet data.
    class Buffer {
    friend class BufferHelper;
    private:
        uint8_t*        buffer_;   //< Array of bytes
        int             pos_;
        int             size_;      //< Total capacity of the buffer
        int             used_;      //< Used size
        bool            change_byte_order_;

        int validate_newpos(int offset, int whence);
        void copy_reverse(uint8_t* dest, uint8_t* source, int length);

        void set_pos(int pos);
    public:
        BufferHelper    helper_;    //< Contains help functions

        Buffer(int size, bool change_byte_order=false);
        Buffer(const Buffer& buffer) =delete;
        ~Buffer();
        void reset();
        void clear_after(int32_t pos=-1);
        void set_change_byte_order(bool change_byte_order) {
            change_byte_order_ = change_byte_order;
            reset();
        }
        void write_direct(const void* source, int length=1);
        void read_direct(const void* dest, int length=1);
        void peek_direct(const void* dest, int length=1);

        void write_in_order(const void* source, int length=1);
        void read_in_order(const void* dest, int length=1);
        void peek_in_order(const void* dest, int length=1);

        void seek(int offset, int whence=0);
        void rewind();


        //----------------------------------------------------------------------

        // literals

        template <typename T>
        void write_value(const typename identity<T>::type& value);

        template <typename T>
        typename identity<T>::type read_value();

        template <typename T>
        typename identity<T>::type peek_value();

        // pointers

        template <typename T>
        void write_value(const T* value);

        template <typename T>
        void read_value(T* value);

        template <typename T>
        void peek_value(T* value);

        //----------------------------------------------------------------------


        /// This should be called after using this buffer to read from a socket
        void add_written_len(int size);

        /** Shifts data in the buffer to the front by `offset`.
         *
         * Shifts data in the buffer to the front by `offset` number of bytes.
         * Number of `offset` bytes from the end get zeroed.
         */
        void forward(int offset=-1);

        int get_size() { return size_; };
        int get_used() { return used_; };
        int get_pos() { return pos_; };
        int get_available_write_size() { return size_ - used_; };
        bool get_changed_byte_order() {
            return this->change_byte_order_;
        };
        uint8_t* get_write_pointer();
        uint8_t* get_buffer();

        /// T is a container of any wrapped pointers
        template <typename T>
        void write_container_wrapped_pointers(
                T* container, TypeMap* typemap, bool use_type_id);

        /// T is a container of any wrapped pointers
        template <typename T>
        void read_container_wrapped_pointers(
                T* container, TypeMap* typemap, bool use_type_id);

        // pure encodable

        template <typename T>
        void write_container_encodables(
                T* container, TypeMap* typemap, bool use_type_id);

        template <typename T>
        void read_container_encodables(
                T* container, TypeMap* typemap, bool use_type_id);

        // simple containers

        template <typename T>
        void write_container_numbers(
                T* container, TypeMap* typemap, bool use_type_id);

        template <typename T>
        void read_container_numbers(
                T* container, TypeMap* typemap, bool use_type_id);

        // strings

        void read_container_strings(
                std::vector<std::string>* container,
                TypeMap* typemap,
                bool use_type_id);

        void write_container_strings(
                std::vector<std::string>* container,
                TypeMap* typemap,
                bool use_type_id);

    };


    Buffer::Buffer(int size, bool change_byte_order):
            size_(size),
            change_byte_order_(change_byte_order),
            helper_(BufferHelper(this)) {

        buffer_ = new uint8_t[size];
        pos_ = 0;
        used_ = 0;
        memset(buffer_, 0, size_);
    }


    Buffer::~Buffer() {
        delete [] buffer_;
    }


    void Buffer::reset() {
        if (used_ == 0) {
            return;
        }
        pos_ = 0;
        memset(buffer_, 0, used_);
        used_ = 0;
    }


    void Buffer::clear_after(int32_t pos) {
        if (pos == -1) {
            pos = pos_;
        }
        if (pos <= used_) {
            return;
        }
        pos = validate_newpos(pos, 0);
        memset(buffer_, pos, used_);
        used_ = pos;
    }


    int Buffer::validate_newpos(int offset, int whence) {
        switch(whence) {
        case 0: // from the beginning
            break;
        case 1: // from current pos
            offset += pos_;
            break;
        case 2: // from the end
            offset = size_ - offset;
            break;
        default:
            throw std::runtime_error("Wrong `whence` argument: " + whence);
        }

        if (offset < 0) {
            throw std::underflow_error("Internet message Buffer underflow");
        }
        if (offset > size_) {
            throw std::overflow_error("Internet message Buffer overflow");
        }

        return offset;
    }


    void Buffer::set_pos(int pos) {
        pos_ = pos;
        if (pos_ > used_) {
            used_ = pos;
        }
    }


    void Buffer::add_written_len(int size) {
        used_ = validate_newpos(used_ + size, 0);
    }


    void Buffer::write_direct(const void* source, int length) {
        int newpos = validate_newpos(length, 1);
        memcpy(&buffer_[pos_], source, length);
        set_pos(newpos);
    }


    void Buffer::read_direct(const void* dest, int length) {
        pos_ = validate_newpos(length, 1);
        memcpy((void*)dest, &buffer_[pos_-length], length);
    }


    void Buffer::peek_direct(const void* dest, int length) {
        validate_newpos(length, 1);
        memcpy((void*)dest, &buffer_[pos_], length);
    }


    void Buffer::copy_reverse(uint8_t* dest, uint8_t* source, int length) {
        for (register int i=0; i<length; i++) {
            *(dest+i) = *(source+length-i-1);
        }
    }


    void Buffer::write_in_order(const void* source, int length) {
        if (change_byte_order_) {
            set_pos(validate_newpos(length, 1));
            copy_reverse(&buffer_[pos_-length], (uint8_t*)source, length);
            return;
        }
        write_direct(source, length);
    }

    void Buffer::read_in_order(const void* dest, int length) {
        if (change_byte_order_) {
            pos_ = validate_newpos(length, 1);
            copy_reverse((uint8_t*)dest, &buffer_[pos_-length], length);
            return;
        }
        read_direct(dest, length);
    }

    void Buffer::peek_in_order(const void* dest, int length) {
        if (change_byte_order_) {
            validate_newpos(length, 1);
            copy_reverse((uint8_t*)dest, &buffer_[pos_], length);
            return;
        }
        peek_direct(dest, length);
    }


    void Buffer::seek(int offset, int whence) {
        pos_ = validate_newpos(offset, whence);
    }


    void Buffer::rewind() {
        pos_ = 0;
    }


    void Buffer::forward(int offset) {
        if (offset == 0) return;
        if (offset == -1) {
            offset = used_;
        }

        if (offset < 0) {
            throw std::underflow_error("Internet message Buffer cannot "
                    "forward to negative");
        }
        if (offset > size_) {
            throw std::overflow_error("Internet message Buffer cannot "
                    "forward passed size");
        }

        if (offset >= used_) {
            memset(buffer_, 0, used_);
        }
        else {
            // shift the data
            for (int i=0; offset+i < used_; i++) {
                buffer_[i] = buffer_[offset+i];
            }

            memset(buffer_ + offset, 0, used_-offset);
        }

        pos_ -= offset;
        if (pos_ < 0) {
            pos_ = 0;
        }

        used_ -= offset;
        if (used_ < 0) {
            used_ = 0;
        }
    }


    uint8_t* Buffer::get_write_pointer() {
        return buffer_ + used_;

    }


    uint8_t* Buffer::get_buffer() {
        return buffer_;
    }


    //--------------------------------------------------------------------------

    // pointers

    template <typename T>
    void Buffer::write_value(const T* value) {
        write_in_order(value, sizeof(T));
    }


    template <typename T>
    void Buffer::read_value(T* value) {
        read_in_order(value, sizeof(T));
    }


    template <typename T>
    void Buffer::peek_value(T* value) {
        peek_in_order(value, sizeof(T));
    }


    template <>
    void Buffer::write_value(const bool* value) {
        uint8_t v = (*value) ? 1 : 0;
        write_value(&v);
    }


    template <>
    void Buffer::read_value(bool* value) {
        uint8_t v;
        read_value(&v);
        *value = (v!=0);
    }


    template <>
    void Buffer::peek_value(bool* value) {
        uint8_t v;
        peek_value(&v);
        *value = (v!=0);
    }


    template <>
    void Buffer::write_value(const char* value) { // works with string literal
        uint32_t strsize = strlen(value);
        int newpos_ = validate_newpos(sizeof(uint32_t) + strsize, 1);

        write_direct(&strsize, sizeof(uint32_t));

        memcpy(&buffer_[pos_], value, strsize);

        set_pos(newpos_);
    }

    template <>
    void Buffer::write_value(const std::string* value) {
        write_value(value->c_str());
    }


    template <>
    void Buffer::read_value(std::string* value) {
        uint32_t strsize;
        peek_direct(&strsize, sizeof(uint32_t));

        int newpos_ = validate_newpos(strsize + sizeof(uint32_t), 1);
        pos_ += sizeof(uint32_t);

        value->resize(strsize);
        memcpy((void*)value->c_str(), &buffer_[pos_], strsize);

        pos_ = newpos_;
    }


    // literals

    template <typename T>
    void Buffer::write_value(const typename identity<T>::type& value) {
        write_value(&value);
    }


    template <typename T>
    typename identity<T>::type Buffer::read_value() {
        T value;
        read_value(&value);
        return value;
    }


    template <typename T>
    typename identity<T>::type Buffer::peek_value() {
        T value;
        peek_value(&value);
        return value;
    }


    template <>
    void Buffer::write_value<std::string>(
            const typename identity<std::string>::type& value) {
        write_value<std::string>(&value);
    }


    template <>
    typename identity<std::string>::type Buffer::read_value<std::string>() {
        std::string value;
        read_value<std::string>(&value);
        return value;
    }


    template <>
    void Buffer::write_value<bool>(
            const typename identity<bool>::type& value) {
        write_value<bool>(&value);
    }


    template <>
    typename identity<bool>::type Buffer::read_value<bool>() {
        bool value;
        read_value<bool>(&value);
        return value;
    }


    template <>
    typename identity<bool>::type Buffer::peek_value<bool>() {
        bool value;
        peek_value<bool>(&value);
        return value;
    }


    //--------------------------------------------------------------------------

}
