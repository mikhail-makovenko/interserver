#pragma once

#include "buffer.hpp"
#include "typemap_constants.hpp"
#include "utils/sequence.hpp"


namespace interserver {

    class TypeMap;

    /** A class for holding data that is exchanged via network connection.
     *
     *  Derived from MessageBase classes must have static TYPE_ID field of
     *  int32_t type.
     */
    class MessageBase {
    public:
        virtual ~MessageBase() {};

        static int32_t gen_type_id();
        virtual const int32_t get_type_id(TypeMap* typemap) const =0;
        virtual const std::string& get_type_name() const =0;

        virtual void encode(Buffer* buffer, TypeMap* typemap,
                bool use_type_id) const =0;
        void encode_start(Buffer* buffer, TypeMap* typemap,
                bool use_type_id) const;
        void encode_end(Buffer* buffer, TypeMap* typemap,
                bool use_type_id) const {};


        virtual void decode(Buffer* buffer, TypeMap* typemap,
                bool use_type_id) =0;
        void decode_start(Buffer* buffer, TypeMap* typemap,
                bool use_type_id);
        void decode_end(Buffer* buffer, TypeMap* typemap,
                bool use_type_id) {};

    private:
        static utils::IntSequence int_sequence_;
    };

}



