#pragma once

#include <unordered_map>
#include <vector>

#include "utils/sequence.hpp"
#include "typemap_constants.hpp"
#include "msgtypes/msgtypes_typemap.hpp"


namespace interserver {


    /** `BaseType` represent a simple type like an integer or a float.
     *
     * Fields:
     *  - id
     *  - name
     *  - size in bytes,  -1 means size is in the next bytes
     */
    class BaseType : public msgtypes::__BaseType__ {
    public:
        BaseType(int32_t id, const std::string& name, int32_t size);
    };


    /* `TypeField` represents a field of a `StructType` object.
     *
     * Fields:
     *  - name
     *  - type_id (of a `StructType`)
     */
    class TypeField : public msgtypes::__TypeField__ {
    public:
        TypeField(const std::string& name, int32_t type_id);
    };


    /** `StructType` represents a type with properties.
     *
     * Fields:
     *  - name
     *  - id
     *  - fields
     */
    class StructType : public msgtypes::__StructType__ {
    public:
        StructType(const std::string& name, int32_t id);

        void add_field(const std::string& name, int32_t type_id_);
    };


    /// `FuncArg` contains function argument name and it's id.
    class FuncArg : public msgtypes::__FuncArg__ {
    public:
        using msgtypes::__FuncArg__::__FuncArg__;
    };


    /// `FuncType` contains function name and id and arguments.
    class FuncType : public msgtypes::__FuncType__ {
    public:
        using msgtypes::__FuncType__::__FuncType__;

        void add_arg(const std::string& name, int32_t type_id);
    };


    /** `TypeMap` class is used to keep track of all known types.
     *
     * Keeps track of all known `BaseType` and `StructType` types.
     * Can provide collected information in a form that can be passed to
     * a client program via a socket.
     *
     * Fields:
     *  - base_types
     *  - struct_types
     */
    class TypeMap : public msgtypes::__TypeMap__ {
    private:
        std::unordered_map<int32_t, int32_t> synced_type_ids_;
        std::unordered_map<int32_t, int32_t> synced_func_ids_;
    public:
        TypeMap();
        TypeMap(Buffer* buffer, bool use_type_id):
                msgtypes::__TypeMap__(buffer, nullptr, use_type_id) {};

        static int32_t gen_type_id();
        StructType* add_struct(
                const std::string& struct_name, int32_t struct_id);
        FuncType* add_func(
                const std::string& func_name, int32_t func_id);

        std::string to_json();

        void sync(Buffer* buffer, bool use_type_id);
        void sync(const TypeMap* typemap);
        int32_t get_synced_type_id(int32_t not_synced_id);
        int32_t get_synced_func_id(int32_t not_synced_id);

        void sync_type_id(int32_t not_synced_id, int32_t synced_id);
        void sync_func_id(int32_t not_synced_id, int32_t synced_id);

        StructType* get_struct_by_name(const std::string& name);
        FuncType* get_func_by_name(const std::string& name);

        bool has_type_name(std::string name);
        bool has_type_id(int32_t id);

        bool has_func_name(std::string name);
        bool has_func_id(int32_t id);
    };
}
