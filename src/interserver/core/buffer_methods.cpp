#include "buffer.hpp"
#include "typemap.hpp"


namespace interserver {

    void Buffer::write_container_strings(
            std::vector<std::string>* container,
            TypeMap* typemap,
            bool use_type_id) {

        if (use_type_id) {
            // array id
            int32_t type_array_id;
            if (typemap) {
                type_array_id = typemap->get_synced_type_id(MAPTYPE_ARRAY);
            }
            else {
                type_array_id = MAPTYPE_ARRAY;
            }
            write_in_order(&type_array_id, sizeof(type_array_id));

            // string id
            int32_t type_string_id;
            if (typemap) {
                type_string_id = typemap->get_synced_type_id(MAPTYPE_STRING);
            }
            else {
                type_string_id = MAPTYPE_STRING;
            }
            write_in_order(&type_string_id, sizeof(type_string_id));
        }
        int32_t container_size = container->size();
        write_value(&container_size);

        for (const std::string& str : *container) {
            write_value(&str);
        }
    }

    void Buffer::read_container_strings(
            std::vector<std::string>* container,
            TypeMap* typemap,
            bool use_type_id) {

        if (use_type_id) {
            int32_t temp;
            read_value(&temp);
            int32_t maptype = MAPTYPE_ARRAY;
            if (typemap) maptype = typemap->get_synced_type_id(maptype);
            if (temp != maptype) {
                throw std::runtime_error("Tried to read an array from buffer, "
                        "but data type is wrong");
            }
            read_value(&temp);
            maptype = MAPTYPE_STRING;
            if (typemap) maptype = typemap->get_synced_type_id(maptype);
            if (temp != maptype) {
                throw std::runtime_error("Tried to read an array of strings "
                        " from buffer, but data type is wrong");
            }
        }
        int32_t container_size;
        read_value(&container_size);
        container->reserve(container->size() + container_size);
        for (int i=0; i < container_size; i++) {
            container->emplace_back(read_value<std::string>());
        }
    }



}
