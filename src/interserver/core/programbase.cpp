#include <memory>
#include <vector>
#include "spdlog/spdlog.h"
#include "frontbase.hpp"
#include "interserver/settings.hpp"
#include "typemap.hpp"
#include "interserver/core/msgtypes/msgtypes_common.hpp"


namespace interserver {

    class ServerCoreFront;

    class ProgramBase {
    // for doing sync ids
    friend class ServerCoreFront;
    protected:
        std::vector<std::shared_ptr<FrontBase>>     fronts_;
        TypeMap                                     typemap_;

        virtual int32_t gen_message_id() =0;
        void process_message(ClientSocketWrapper* client);

        void collect_front_names(std::vector<std::string>* v);
        void collect_front_names_and_ids(
                std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v);
        void sync_front_other_ids(
                std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v);
    public:
        virtual ~ProgramBase();

        virtual std::shared_ptr<spdlog::logger> get_logger() {
            static std::shared_ptr<spdlog::logger> logger =
                gen_logger_st("ProgramBase(default)", LOG_SEVERITY_PROGRAMBASE);
            return logger;
        };

        TypeMap* get_typemap() {
            return &typemap_;
        };

        int32_t get_synced_type_id(int32_t id) {
            return typemap_.get_synced_type_id(id);
        };
        int32_t get_synced_func_id(int32_t id) {
            return typemap_.get_synced_func_id(id);
        };

        FrontBase* get_front_by_id(int32_t id, bool use_other);
        FrontBase* get_front_by_name(std::string name);

        void register_front(std::shared_ptr<FrontBase> front);
        void __unregister_front(std::shared_ptr<FrontBase> front);
        void __unregister_all_fronts();

        virtual void process_error(ClientSocketWrapper* client);
        void read_function_call(ClientSocketWrapper* client, bool use_type_id);

        virtual void stop() =0;
    };


    ProgramBase::~ProgramBase() {
        __unregister_all_fronts();
    }


    FrontBase* ProgramBase::get_front_by_id(int32_t id, bool use_other) {
        for (const auto& front : fronts_) {
            if (use_other) {
                if (front->get_other_id() == id) {
                    return front.get();
                }
            }
            else {
                if (front->get_id() == id) {
                    return front.get();
                }
            }
        }
        return nullptr;
    }


    FrontBase* ProgramBase::get_front_by_name(std::string name) {
        for (const auto& front : fronts_) {
            if (front->get_name() == name) {
                return front.get();
            }
        }
        return nullptr;
    }


    void ProgramBase::register_front(
                std::shared_ptr<FrontBase> front) {
        fronts_.push_back(front);
        front->on_register(this);
    }


    void ProgramBase::__unregister_front(
            std::shared_ptr<FrontBase> front) {
        auto front_ptr = front.get();
        auto pos = find_if(fronts_.begin(), fronts_.end(),
                [front_ptr](const std::shared_ptr<FrontBase>& sp_front)
                        -> bool {
                    return sp_front.get() == front_ptr;
                }) - fronts_.begin();
        std::swap(fronts_[pos], fronts_.back());
        fronts_.pop_back();
        front->on_unregister(this);
    }


    void ProgramBase::__unregister_all_fronts() {
        for (auto front : fronts_) {
            front->on_unregister(this);
        }
        fronts_.clear();
    }


    void ProgramBase::process_message(ClientSocketWrapper* client) {
        if (!client->read_buffer_->helper_.have_full_message()) {
            return;
        }

        client->prepare_write_buffer();
        client->write_buffer_->helper_.set_message_id(gen_message_id());
        client->write_buffer_->helper_.seek_to_content();
        client->read_buffer_->helper_.seek_to_content();

        if (client->read_buffer_->helper_.get_error_flag()) {
            process_error(client);
            client->read_buffer_remove_current_message();
            return;
        }

        int32_t message_type;
        client->read_buffer_->read_value(&message_type);

        if (message_type == get_synced_type_id(MAPTYPE_FUNCTION_CALL)) {
            read_function_call(client, false);
            client->read_buffer_remove_current_message();
            return;
        }

        if (message_type == get_synced_type_id(MAPTYPE_CALLBACK)) {
            // TODO callback
            client->read_buffer_remove_current_message();
            return;
        }

        client->write_error_message( "Unknown message type: " +
                std::to_string(message_type), get_logger(), true);
        client->read_buffer_remove_current_message();
    }


    void ProgramBase::read_function_call(ClientSocketWrapper* client,
            bool use_type_id) {
        int32_t temp;
        if (use_type_id) {
            client->read_buffer_->read_value(&temp);
            if (temp != get_synced_type_id(MAPTYPE_FUNCTION_CALL)) {
                client->write_error_message("Failed to call function "
                        "(wrong type)", get_logger(), true);
                return;
            }
        }
        client->read_buffer_->read_value(&temp);
        FrontBase* front = get_front_by_id(temp, true);

        bool err = (front == nullptr);
        if (!err) {
            if (front->get_id() != 0) { // 0 is a special front
                err = !client->front_is_known(front->get_id());
            }
        }
        if (err) {
            client->write_error_message("Failed to call function "
                    "(no such front or unknown front: " +
                    std::to_string(temp) + ")", get_logger(), true);
            return;
        }

        front->read_function_call(client);
    }


    void ProgramBase::process_error(ClientSocketWrapper* client) {
        get_logger()->error("(process_error) " +
                client->read_buffer_->read_value<std::string>());
    }


    void ProgramBase::collect_front_names(std::vector<std::string>* v) {
        for (const auto& front : fronts_) {
            front->collect_front_names(v);
        }
    }


    void ProgramBase::collect_front_names_and_ids(
            std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v) {
        for (const auto& front : fronts_) {
            front->collect_front_names_and_ids(v);
        }
    }


    void ProgramBase::sync_front_other_ids(
            std::vector<std::shared_ptr<msgtypes::IntegerAndString>>* v) {
        for (const auto& front : fronts_) {
            front->sync_front_other_ids(v);
        }
    }
}
