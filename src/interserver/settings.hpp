#pragma once

#ifndef INTERSERVER_TEST
#   include "settings_prod.hpp"
#else
#   include "tests/settings_test.hpp"
#endif


namespace interserver {

    /// Generates single threaded logger object
    inline std::shared_ptr<spdlog::logger> gen_logger_st(
            std::string name, spdlog::level::level_enum level) {

        auto new_logger = spdlog::stdout_logger_st(name, LOGGER_USE_COLOR);
        new_logger->set_level(level);
        new_logger->set_pattern(LOGGER_PATTERN);
        return new_logger;
    }

}
