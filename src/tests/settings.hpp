#pragma once

#include "spdlog/spdlog.h"


using spdlog::level::level_enum;


namespace interserver { namespace test {

    const int TEST_SERVER_PORT = 10102;

} }
