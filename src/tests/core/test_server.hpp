#include "gtest/gtest.h"
#include "utils/server_and_clients.hpp"

/*
#include <future>
#define TEST_SERVER_AND_CLIENTS_TIMEOUT_BEGIN() \
    std::promise<bool> promisedFinished; \
    auto futureResult = promisedFinished.get_future(); \
    std::thread([scs_ptr](std::promise<bool>& finished) {

#define TEST_SERVER_AND_CLIENTS_TIMEOUT_FAIL_END(X)  \
        finished.set_value(true); \
    }, std::ref(promisedFinished)).detach(); \
   scs_ptr->stop(); \
   ASSERT_TRUE(futureResult.wait_for(std::chrono::milliseconds(X)) != std::future_status::timeout) \
       << "Fail: timed out error";

#define TEST_SERVER_AND_CLIENTS_TIMEOUT_SUCCESS_END(X) \
        finished.set_value(true); \
    }, std::ref(promisedFinished)).detach(); \
    ASSERT_FALSE(futureResult.wait_for(std::chrono::milliseconds(X)) != std::future_status::timeout) \
        << "Fail: timed out error";

using std::chrono::seconds;
using std::cv_status;
*/

namespace interserver { namespace test {


    TEST(Server, client_connection) {
        ServerAndClients scs;
        auto scs_ptr = &scs;
        //TEST_SERVER_AND_CLIENTS_TIMEOUT_BEGIN()

        scs_ptr->set_client_count(1);
        scs_ptr->start_tcp();
        scs_ptr->stop();

        //TEST_SERVER_AND_CLIENTS_TIMEOUT_FAIL_END(1000)
    }


} }
