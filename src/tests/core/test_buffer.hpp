#pragma once

#include <cstring>
#include <iostream>
#include <memory>
#include <system_error>
#include <vector>

#include "gtest/gtest.h"
#include "interserver/core.hpp"
#include "msgtypes/msgtypes_test_buffer.hpp"


namespace interserver { namespace test {

    class TestBuffer : public ::testing::Test {
    protected:
        virtual void SetUp() {
            buffer_.reset(new interserver::Buffer(BUFFER_SIZE_));
        }

        const int INT_SIZE_ = sizeof(uint32_t);
        bool CHANGE_BYTE_ORDER_ARR_[2] = {false, true};

        int pos_;
        const int BUFFER_SIZE_ = 256;
        std::unique_ptr<interserver::Buffer> buffer_;
    };

    std::ostream& operator<<(std::ostream &stream,
            const msgtypes::TestBuffer_Message& tm) {

      return stream << tm.to_string();
    }


    TEST_F(TestBuffer, read_write_strings) {
        for (bool change_byte_order : CHANGE_BYTE_ORDER_ARR_) {
            buffer_->set_change_byte_order(change_byte_order);
            ASSERT_EQ(buffer_->get_pos(), 0) <<
                    "Change byte order: " << change_byte_order;

            // write strings in both buffers
            std::string str_value_w = "String: 'Русский Текст'.";
            buffer_->write_value(&str_value_w);

            pos_ = INT_SIZE_ + str_value_w.size();  // 1 int for size
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            buffer_->seek(0, 0);
            ASSERT_EQ(buffer_->get_pos(), 0) <<
                    "Change byte order: " << change_byte_order;

            std::string str_value_r = buffer_->read_value<std::string>();
            //ASSERT_EQ(str_value_r, str_value_w);

            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
        }
    }


    TEST_F(TestBuffer, read_write_numbers_and_bools) {
        for (bool change_byte_order : CHANGE_BYTE_ORDER_ARR_) {
            buffer_->set_change_byte_order(change_byte_order);
            pos_ = 0;
            ASSERT_EQ(buffer_->get_pos(), pos_)
                << "Change byte order: " << change_byte_order;

            // writing

            bool    b1  = true;
            bool    b2  = false;
            int8_t  i8  = 80;
            int16_t i16 = 16000;
            int32_t i32 = 32000000;
            int64_t i64 = 64000000000;

            buffer_->write_value(&b1);
            pos_ += 1;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            buffer_->write_value(&b2);
            pos_ += 1;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            buffer_->write_value(&i8);
            pos_ += sizeof(i8);
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            buffer_->write_value(&i16);
            pos_ += sizeof(i16);
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            buffer_->write_value(&i32);
            pos_ += sizeof(i32);
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            buffer_->write_value(&i64);
            pos_ += sizeof(i64);
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            int total_size = buffer_->get_pos();

            // peeking

            pos_ = 0;
            buffer_->seek(0, 0);
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            bool    b1_p;
            bool    b2_p;
            int8_t  i8_p;
            int16_t i16_p;
            int32_t i32_p;
            int64_t i64_p;

            buffer_->peek_value(&b1_p);
            ASSERT_EQ(b1_p, b1) << "Change byte order: " << change_byte_order;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
            pos_ += 1;
            buffer_->seek(pos_, 0);

            buffer_->peek_value(&b2_p);
            ASSERT_EQ(b2_p, b2) << "Change byte order: " << change_byte_order;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
            pos_ += 1;
            buffer_->seek(pos_, 0);

            buffer_->peek_value(&i8_p);
            ASSERT_EQ(i8_p, i8) << "Change byte order: " << change_byte_order;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
            pos_ += sizeof(i8);
            buffer_->seek(pos_, 0);

            buffer_->peek_value(&i16_p);
            ASSERT_EQ(i16_p, i16) << "Change byte order: " << change_byte_order;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
            pos_ += sizeof(i16);
            buffer_->seek(pos_, 0);

            buffer_->peek_value(&i32_p);
            ASSERT_EQ(i32_p, i32) << "Change byte order: " << change_byte_order;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
            pos_ += sizeof(i32);
            buffer_->seek(pos_, 0);

            buffer_->peek_value(&i64_p);
            ASSERT_EQ(i64_p, i64) << "Change byte order: " << change_byte_order;
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;
            pos_ += sizeof(i64);
            buffer_->seek(pos_, 0);

            ASSERT_EQ(buffer_->get_pos(), total_size) <<
                    "Change byte order: " << change_byte_order;

            // reeding

            pos_ = 0;
            buffer_->seek(0, 0);
            ASSERT_EQ(buffer_->get_pos(), pos_) <<
                    "Change byte order: " << change_byte_order;

            bool    b1_r;
            bool    b2_r;
            int8_t  i8_r;
            int16_t i16_r;
            int32_t i32_r;
            int64_t i64_r;

            buffer_->read_value(&b1_r);
            ASSERT_EQ(b1_r, b1) << "Change byte order: " << change_byte_order;

            buffer_->read_value(&b2_r);
            ASSERT_EQ(b2_r, b2) << "Change byte order: " << change_byte_order;

            buffer_->read_value(&i8_r);
            ASSERT_EQ(i8_r, i8) << "Change byte order: " << change_byte_order;

            buffer_->read_value(&i16_r);
            ASSERT_EQ(i16_r, i16) << "Change byte order: " << change_byte_order;

            buffer_->read_value(&i32_r);
            ASSERT_EQ(i32_r, i32) << "Change byte order: " << change_byte_order;

            buffer_->read_value(&i64_r);
            ASSERT_EQ(i64_r, i64) << "Change byte order: " << change_byte_order;

        }
    }


    TEST_F(TestBuffer, get_available_write_size) {
        const std::string str_const = "Some string";
        const int STR_CONST_SIZE = str_const.size() + INT_SIZE_;

        int64_t i64;
        // to read written string
        int str_pos_in_buffer;

        int pos = 0;
        int used = 0;  // number of bytes already written by the buffer
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // add_written_len, used size changes, position not
        pos += 0;
        used += 2;
        buffer_->add_written_len(2);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // writing moves pos and adds used size if needed
        pos += 8;
        used = pos;
        buffer_->write_value(&i64);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        pos += STR_CONST_SIZE;
        used = pos;
        str_pos_in_buffer = buffer_->get_pos();  // will read the string later
        buffer_->write_value(&str_const);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // peek does nothing
        buffer_->peek_value(&i64);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // reed only moves position
        pos += 8;
        buffer_->read_value(&i64);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        buffer_->seek(str_pos_in_buffer, 0);
        pos = buffer_->get_pos() + STR_CONST_SIZE;
        buffer_->read_value<std::string>();
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // seek only moves position
        pos = used + 50;
        buffer_->seek(pos, 0);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // write after used size will mark space in between as used
        // (already have sought passed some space after used size)
        pos += 8;
        used = pos;
        buffer_->write_value(&i64);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);

        // writing inside used size will not changed used size
        buffer_->seek(0, 0);
        pos = 8 + STR_CONST_SIZE + 8;
        ASSERT_GT(used, pos);
        buffer_->write_value(&i64);
        buffer_->write_value(&str_const);
        buffer_->write_value(&i64);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(buffer_->get_available_write_size(), BUFFER_SIZE_ - used);
    }


    TEST_F(TestBuffer, overflow_and_underflow) {
        int pos = 0;

        try {
            buffer_->seek(-1, 0);
            FAIL() << "Seek to negative position succeeded";
        }
        catch(const std::exception& e) {
            ASSERT_EQ(buffer_->get_pos(), pos) <<
                    "Position changed after seeking to negative";
        }

        try {
            buffer_->seek(buffer_->get_size() + 1, 0);
            FAIL() << "Seek to position larger then buffer size succeeded";
        }
        catch(const std::exception& e) {
            ASSERT_EQ(buffer_->get_pos(), pos) <<
                    "Position changed after seeking passed buffer size";
        }

        try {
            int64_t value = 10;
            pos = buffer_->get_size() - 7;
            buffer_->seek(pos, 0);
            buffer_->write_value(&value);
            FAIL() << "Wrote int64_t passed buffer capacity";
        }
        catch(const std::exception& e) {
            ASSERT_EQ(buffer_->get_pos(), pos) <<
                "Position changed after trying to write number with overflow";
        }

        try {
            std::string str = "My string";
            pos = buffer_->get_size() - INT_SIZE_ - str.size() + 1;
            buffer_->seek(pos, 0);
            buffer_->write_value(&str);
            FAIL() << "Wrote string passed buffer capacity";
        }
        catch(const std::exception& e) {
            ASSERT_EQ(buffer_->get_pos(), pos) <<
                "Position changed after trying to write string with overflow";
        }

    }


    TEST_F(TestBuffer, forward) {
        int pos = 0;
        int8_t  n1 = 1;
        int16_t n2 = 2;
        int32_t n3 = 3;
        int32_t n4 = 4;
        int32_t n4_r;
        int64_t n5 = 5;
        int64_t n5_r;
        ASSERT_EQ(buffer_->get_pos(), pos);

        pos += sizeof(n1) + sizeof(n2) + sizeof(n3) + sizeof(n4) + sizeof(n5);
        buffer_->write_in_order(&n1, sizeof(n1));
        buffer_->write_in_order(&n2, sizeof(n2));
        buffer_->write_in_order(&n3, sizeof(n3));
        buffer_->write_in_order(&n4, sizeof(n4));
        buffer_->write_in_order(&n5, sizeof(n5));
        ASSERT_EQ(
                buffer_->get_pos(),
                buffer_->get_size()-buffer_->get_available_write_size());

        pos = sizeof(n1) + sizeof(n2) + sizeof(n3) + sizeof(n4);
        buffer_->seek(pos);
        buffer_->forward(sizeof(n1) + sizeof(n2) + sizeof(n3));

        ASSERT_EQ(buffer_->get_pos(), sizeof(n4));
        ASSERT_EQ(buffer_->get_size() - buffer_->get_available_write_size(),
                  sizeof(n4) + sizeof(n5));  // 3 number forwarded

        buffer_->seek(0);
        buffer_->read_in_order(&n4_r, sizeof(n4_r));
        ASSERT_EQ(n4_r, n4);

        buffer_->read_in_order(&n5_r, sizeof(n5_r));
        ASSERT_EQ(n5_r, n5);

        // zeros after the position
        buffer_->read_in_order(&n5_r, sizeof(n5_r));
        ASSERT_EQ(n5_r, 0);

    }


    TEST_F(TestBuffer, container_of_wrapped_pointers) {
        typedef msgtypes::TestBuffer_Message TM;
        typedef std::vector<std::unique_ptr<TM>> tm_vector;
        tm_vector vector_w;
        vector_w.push_back(std::unique_ptr<TM>(new TM(100, 1000)));
        vector_w.push_back(std::unique_ptr<TM>(new TM(200, 2000)));
        vector_w.push_back(std::unique_ptr<TM>(new TM(300, 3000)));
        buffer_->write_container_wrapped_pointers(&vector_w, nullptr, true);

        // making sure the data is correct
        int32_t read_int;
        buffer_->seek(0);
        // array type
        buffer_->read_value(&read_int);
        ASSERT_EQ(read_int, interserver::MAPTYPE_ARRAY)
            << "Wrong array type";
        // value type
        buffer_->read_value(&read_int);
        ASSERT_EQ(read_int, TM::static_get_type_id(nullptr))
            << "Wrong message type";

        // size taken by type declarations
        int types_declaration_len = buffer_->get_pos();

        // size
        buffer_->read_value(&read_int);
        ASSERT_EQ(read_int, 3) << "Wrong array size";

        // reading
        buffer_->seek(0);
        tm_vector vector_r_1;
        buffer_->read_container_wrapped_pointers(&vector_r_1, nullptr, true);
        ASSERT_EQ(vector_r_1.size(), vector_w.size());
        for (unsigned int i=0; i < vector_w.size(); i++) {
            ASSERT_EQ(*vector_r_1[i].get(), *vector_w[i].get());
        }

        // now without writing type data
        int excpect_pos = buffer_->get_pos() - types_declaration_len;
        buffer_->reset();
        buffer_->write_container_wrapped_pointers(&vector_w, nullptr, false);
        ASSERT_EQ(excpect_pos, buffer_->get_pos());

        // reading
        buffer_->seek(0);
        tm_vector vector_r_2;
        buffer_->read_container_wrapped_pointers(&vector_r_2, nullptr, false);
        ASSERT_EQ(vector_r_2.size(), vector_w.size());
        for (unsigned int i=0; i < vector_w.size(); i++) {
            ASSERT_EQ(*vector_r_2[i].get(), *vector_w[i].get());
        }
    }


    TEST_F(TestBuffer, container_of_encodables) {
        typedef msgtypes::TestBuffer_Message TM;
        typedef std::vector<TM> tm_vector;
        tm_vector vector_w;
        vector_w.emplace_back(100, 1000);
        vector_w.emplace_back(200, 2000);
        vector_w.emplace_back(300, 3000);
        buffer_->write_container_encodables(&vector_w, nullptr, true);

        // making sure the data is correct
        int32_t read_int;
        buffer_->seek(0);
        // array type
        buffer_->read_value(&read_int);
        ASSERT_EQ(read_int, interserver::MAPTYPE_ARRAY)
            << "Wrong array type";
        // value type
        buffer_->read_value(&read_int);
        ASSERT_EQ(read_int, TM::static_get_type_id(nullptr))
            << "Wrong message type";

        // size taken by type declarations
        int types_declaration_len = buffer_->get_pos();

        // size
        buffer_->read_value(&read_int);
        ASSERT_EQ(read_int, 3) << "Wrong array size";

        // reading
        buffer_->seek(0);
        tm_vector vector_r_1;
        buffer_->read_container_encodables(&vector_r_1, nullptr, true);
        ASSERT_EQ(vector_r_1.size(), vector_w.size());
        for (unsigned int i=0; i < vector_w.size(); i++) {
            ASSERT_EQ(vector_r_1[i], vector_w[i]);
        }

        // now without writing type data
        int excpect_pos = buffer_->get_pos() - types_declaration_len;
        buffer_->reset();
        buffer_->write_container_encodables(&vector_w, nullptr, false);
        ASSERT_EQ(excpect_pos, buffer_->get_pos());

        // reading
        buffer_->seek(0);
        tm_vector vector_r_2;
        buffer_->read_container_encodables(&vector_r_2, nullptr, false);
        ASSERT_EQ(vector_r_2.size(), vector_w.size());
        for (unsigned int i=0; i < vector_w.size(); i++) {
            ASSERT_EQ(vector_r_2[i], vector_w[i]);
        }
    }


    TEST_F(TestBuffer, container_of_numbers) {
        std::vector<uint16_t> v_uint16_t, v_uint16_t_read;
        std::vector<int32_t> v_int32_t, v_int32_t_read;

        // 2*10 + 4*10 = 60 bytes (without type declarations)
        for (int i=0; i < 10; i++) {
            v_uint16_t.push_back(1000*i);
            v_int32_t.push_back(1000*i);
        }

        // saving vectors
        int32_t pos;

        buffer_->write_container_numbers(&v_uint16_t, nullptr, true);
        pos = buffer_->get_pos();
        buffer_->write_container_numbers(&v_int32_t, nullptr, false);
        // no data accept for the one in container was written + array size
        ASSERT_EQ(
                buffer_->get_pos(),
                pos + (sizeof(int32_t) + 10*sizeof(int32_t)));

        pos = buffer_->get_pos();
        int32_t temp;
        buffer_->seek(0);

        // assert types
        buffer_->read_value(&temp);
        ASSERT_EQ(temp, MAPTYPE_ARRAY);
        buffer_->read_value(&temp);
        ASSERT_EQ(temp, TYPEMAP_ID<uint16_t>::ID);

        buffer_->seek(0);
        buffer_->read_container_numbers(&v_uint16_t_read, nullptr, true);
        buffer_->read_container_numbers(&v_int32_t_read, nullptr, false);
        ASSERT_EQ(buffer_->get_pos(), pos);
        ASSERT_EQ(v_uint16_t.size(), v_uint16_t_read.size());
        ASSERT_EQ(v_int32_t.size(), v_int32_t_read.size());

        for (unsigned int i=0; i < v_uint16_t.size(); i++) {
            ASSERT_EQ(v_uint16_t[i], v_uint16_t_read[i]);
            ASSERT_EQ(v_int32_t[i], v_int32_t_read[i]);
        }
    }


    TEST_F(TestBuffer, container_of_strings) {
        std::vector<std::string> vs = {
                "one", "two", "three",  // 10 characters
        };
        std::vector<std::string> vs_read_1, vs_read_2;

        int32_t pos, temp;
        buffer_->write_container_strings(&vs, nullptr, true);
        pos = buffer_->get_pos();
        buffer_->seek(0);

        // assert types
        buffer_->read_value(&temp);
        ASSERT_EQ(temp, MAPTYPE_ARRAY);
        buffer_->read_value(&temp);
        ASSERT_EQ(temp, MAPTYPE_STRING);

        buffer_->seek(0);
        buffer_->read_container_strings(&vs_read_1, nullptr, true);
        ASSERT_EQ(buffer_->get_pos(), pos);

        buffer_->reset();
        buffer_->write_container_strings(&vs, nullptr, false);
        // array type and string type IDs = 2*sizeof(...)
        ASSERT_EQ(buffer_->get_pos(), pos - 2*sizeof(int32_t));
        pos = buffer_->get_pos();
        buffer_->seek(0);
        buffer_->read_container_strings(&vs_read_2, nullptr, false);
        ASSERT_EQ(buffer_->get_pos(), pos);

        ASSERT_EQ(vs_read_1.size(), vs.size());
        ASSERT_EQ(vs_read_2.size(), vs.size());
        for (unsigned int i=0; i < vs.size(); ++i) {
            ASSERT_EQ(vs_read_1[i], vs[i]);
            ASSERT_EQ(vs_read_2[i], vs[i]);
        }
    }

} }
