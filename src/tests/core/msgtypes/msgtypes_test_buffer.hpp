#pragma once

#include "interserver/core/messagebase.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"


namespace interserver { namespace test { namespace msgtypes { 


    //--------------------------------------------------------------------------

    class TestBuffer_Message : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        TestBuffer_Message(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        TestBuffer_Message(uint16_t attr_1, uint32_t attr_2): 
            attr_1_(attr_1),
            attr_2_(attr_2) {};

        uint16_t attr_1_;
        uint32_t attr_2_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        bool operator == (const TestBuffer_Message& other) const {
           return ((this->attr_1_ == other.attr_1_) &&
                   (this->attr_2_ == other.attr_2_));
        }

        std::string to_string() const {
            return "attr_1_: " + std::to_string(this->attr_1_) + ", " +
                    "attr_2_: " + std::to_string(this->attr_2_);
        }

    };


    //--------------------------------------------------------------------------


} } }