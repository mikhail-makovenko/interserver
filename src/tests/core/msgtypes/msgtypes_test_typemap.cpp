#include "interserver/core/messagebase.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"
#include <cstring>


namespace interserver { namespace test { namespace msgtypes { 


    //--------------------------------------------------------------------------

    // TestTypeMap_2 requires TestTypeMap_1 to have empty constructor
    class TestTypeMap_1 : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        TestTypeMap_1():
            id_(0),
            name_("") {};
        TestTypeMap_1(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };

        int32_t id_;
        std::string name_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    // constants ------------------------------------------

    const int TestTypeMap_1::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string TestTypeMap_1::TYPE_NAME =
            "INTERSERVER::TEST::TestTypeMap_1";

    // getters --------------------------------------------

    const int32_t TestTypeMap_1::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return TestTypeMap_1::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_1::TYPE_ID);
    }

    const int32_t TestTypeMap_1::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return TestTypeMap_1::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_1::TYPE_ID);
    }

    const std::string& TestTypeMap_1::get_type_name() const {
        return TestTypeMap_1::TYPE_NAME;
    }

    const std::string& TestTypeMap_1::static_get_type_name() {
        return TestTypeMap_1::TYPE_NAME;
    }

    // other methods --------------------------------------

    void TestTypeMap_1::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&id_);
        buffer->write_value(&name_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_1::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&id_);
        buffer->read_value(&name_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_1::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                TestTypeMap_1::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                TestTypeMap_1::static_get_type_name(),
                TestTypeMap_1::static_get_type_id(typemap));

        new_type->add_field("id",
                MAPTYPE_INT_32);

        new_type->add_field("name",
                MAPTYPE_STRING);

    }


    //--------------------------------------------------------------------------


    class TestTypeMap_2 : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        TestTypeMap_2(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };

        int32_t id_;
        std::string name_;
        TestTypeMap_1 typemap_1_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    // constants ------------------------------------------

    const int TestTypeMap_2::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string TestTypeMap_2::TYPE_NAME =
            "INTERSERVER::TEST::TestTypeMap_2";

    // getters --------------------------------------------

    const int32_t TestTypeMap_2::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return TestTypeMap_2::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_2::TYPE_ID);
    }

    const int32_t TestTypeMap_2::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return TestTypeMap_2::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_2::TYPE_ID);
    }

    const std::string& TestTypeMap_2::get_type_name() const {
        return TestTypeMap_2::TYPE_NAME;
    }

    const std::string& TestTypeMap_2::static_get_type_name() {
        return TestTypeMap_2::TYPE_NAME;
    }

    // other methods --------------------------------------

    void TestTypeMap_2::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&id_);
        buffer->write_value(&name_);
        typemap_1_.encode(buffer, typemap, false);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_2::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&id_);
        buffer->read_value(&name_);
        typemap_1_.decode(buffer, typemap, false);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_2::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                TestTypeMap_2::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                TestTypeMap_2::static_get_type_name(),
                TestTypeMap_2::static_get_type_id(typemap));

        new_type->add_field("id",
                MAPTYPE_INT_32);

        new_type->add_field("name",
                MAPTYPE_STRING);

        new_type->add_field("typemap_1",
                TestTypeMap_1::static_get_type_id(typemap));

    }


    //--------------------------------------------------------------------------


    class TestTypeMap_Named_1 : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        TestTypeMap_Named_1(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };

        int32_t id_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    // constants ------------------------------------------

    const int TestTypeMap_Named_1::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string TestTypeMap_Named_1::TYPE_NAME =
            "INTERSERVER::TEST::TestTypeMap_1";

    // getters --------------------------------------------

    const int32_t TestTypeMap_Named_1::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return TestTypeMap_Named_1::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_Named_1::TYPE_ID);
    }

    const int32_t TestTypeMap_Named_1::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return TestTypeMap_Named_1::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_Named_1::TYPE_ID);
    }

    const std::string& TestTypeMap_Named_1::get_type_name() const {
        return TestTypeMap_Named_1::TYPE_NAME;
    }

    const std::string& TestTypeMap_Named_1::static_get_type_name() {
        return TestTypeMap_Named_1::TYPE_NAME;
    }

    // other methods --------------------------------------

    void TestTypeMap_Named_1::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&id_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_Named_1::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&id_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_Named_1::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                TestTypeMap_Named_1::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                TestTypeMap_Named_1::static_get_type_name(),
                TestTypeMap_Named_1::static_get_type_id(typemap));

        new_type->add_field("id",
                MAPTYPE_INT_32);

    }


    //--------------------------------------------------------------------------


    class TestTypeMap_Named_2 : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        TestTypeMap_Named_2(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };

        int32_t id_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    // constants ------------------------------------------

    const int TestTypeMap_Named_2::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string TestTypeMap_Named_2::TYPE_NAME =
            "INTERSERVER::TEST::TestTypeMap_2";

    // getters --------------------------------------------

    const int32_t TestTypeMap_Named_2::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return TestTypeMap_Named_2::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_Named_2::TYPE_ID);
    }

    const int32_t TestTypeMap_Named_2::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return TestTypeMap_Named_2::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestTypeMap_Named_2::TYPE_ID);
    }

    const std::string& TestTypeMap_Named_2::get_type_name() const {
        return TestTypeMap_Named_2::TYPE_NAME;
    }

    const std::string& TestTypeMap_Named_2::static_get_type_name() {
        return TestTypeMap_Named_2::TYPE_NAME;
    }

    // other methods --------------------------------------

    void TestTypeMap_Named_2::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&id_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_Named_2::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&id_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void TestTypeMap_Named_2::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                TestTypeMap_Named_2::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                TestTypeMap_Named_2::static_get_type_name(),
                TestTypeMap_Named_2::static_get_type_id(typemap));

        new_type->add_field("id",
                MAPTYPE_INT_32);

    }


    //--------------------------------------------------------------------------


} } }