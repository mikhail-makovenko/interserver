#include "interserver/core/messagebase.hpp"
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"


namespace interserver { namespace test { namespace msgtypes { 


    //--------------------------------------------------------------------------

    class TestBuffer_Message : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

    public:
        TestBuffer_Message(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };
        TestBuffer_Message(uint16_t attr_1, uint32_t attr_2): 
            attr_1_(attr_1),
            attr_2_(attr_2) {};

        uint16_t attr_1_;
        uint32_t attr_2_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);

        // literals from types file

        bool operator == (const TestBuffer_Message& other) const {
           return ((this->attr_1_ == other.attr_1_) &&
                   (this->attr_2_ == other.attr_2_));
        }

        std::string to_string() const {
            return "attr_1_: " + std::to_string(this->attr_1_) + ", " +
                    "attr_2_: " + std::to_string(this->attr_2_);
        }

    };


    // constants ------------------------------------------

    const int TestBuffer_Message::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string TestBuffer_Message::TYPE_NAME =
            "INTERSERVER::TEST::TestBuffer_Message";

    // getters --------------------------------------------

    const int32_t TestBuffer_Message::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return TestBuffer_Message::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestBuffer_Message::TYPE_ID);
    }

    const int32_t TestBuffer_Message::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return TestBuffer_Message::TYPE_ID;
        }
        return typemap->get_synced_type_id(TestBuffer_Message::TYPE_ID);
    }

    const std::string& TestBuffer_Message::get_type_name() const {
        return TestBuffer_Message::TYPE_NAME;
    }

    const std::string& TestBuffer_Message::static_get_type_name() {
        return TestBuffer_Message::TYPE_NAME;
    }

    // other methods --------------------------------------

    void TestBuffer_Message::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        buffer->write_value(&attr_1_);
        buffer->write_value(&attr_2_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void TestBuffer_Message::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read_value(&attr_1_);
        buffer->read_value(&attr_2_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void TestBuffer_Message::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                TestBuffer_Message::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                TestBuffer_Message::static_get_type_name(),
                TestBuffer_Message::static_get_type_id(typemap));

        new_type->add_field("attr_1",
                MAPTYPE_UINT_16);

        new_type->add_field("attr_2",
                MAPTYPE_UINT_32);

    }


    //--------------------------------------------------------------------------


} } }