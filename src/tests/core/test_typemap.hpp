#pragma once

#include <boost/algorithm/string/replace.hpp>

#include "gtest/gtest.h"
#include "interserver/core.hpp"
#include "msgtypes/msgtypes_test_typemap.hpp"


namespace interserver { namespace test {

    TEST(TestTypeMap, encoding) {
        interserver::Buffer buffer(1024);
        interserver::TypeMap typemap_1;
        interserver::TypeMap typemap_2;

        // to make JSON smaller
        typemap_1.base_types_.resize(2);
        typemap_2.base_types_.resize(2);

        msgtypes::TestTypeMap_1::typemap_register(&typemap_1);
        msgtypes::TestTypeMap_2::typemap_register(&typemap_1);

        int32_t test_typemap_id_1 =
                msgtypes::TestTypeMap_1::static_get_type_id(&typemap_1);

        int32_t test_typemap_id_2 =
                msgtypes::TestTypeMap_2::static_get_type_id(&typemap_2);

        std::string expect_json =
                "{"
                "    'baseTypes': [{"
                "        'id': 0,"
                "        'name': 'EMTPY',"
                "        'size': 0"
                "    }, {"
                "        'id': 1,"
                "        'name': 'NULL',"
                "        'size': 0"
                "    }],"
                "    'structTypes': [{"
                "        'id': " + std::to_string(test_typemap_id_1) + "," +
                "        'name': 'INTERSERVER::TEST::TestTypeMap_1',"
                "        'fields': [{"
                "            'name': 'id',"
                "            'typeId': 4"
                "        }, {"
                "            'name': 'name',"
                "            'typeId': 13"
                "        }]"
                "    }, {"
                "        'id': " + std::to_string(test_typemap_id_2) + "," +
                "        'name': 'INTERSERVER::TEST::TestTypeMap_2',"
                "        'fields': [{"
                "            'name': 'id',"
                "            'typeId': 4"
                "        }, {"
                "            'name': 'name',"
                "            'typeId': 13"
                "        }, {"
                "            'name': 'typemap_1',"
                "            'typeId': " + std::to_string(test_typemap_id_1) +
                "        }]"
                "    }],"
                "    'funcTypes': []"
                "}";

        boost::replace_all(expect_json, " ", "");
        boost::replace_all(expect_json, "'", "\"");
        ASSERT_EQ(typemap_1.to_json(), expect_json);
        ASSERT_NE(typemap_1.to_json(), typemap_2.to_json());

        typemap_1.encode(&buffer, &typemap_1, true);
        int expect_buffer_pos = buffer.get_pos();
        buffer.seek(0);

        typemap_2.decode(&buffer, &typemap_1, true);
        ASSERT_EQ(buffer.get_pos(), expect_buffer_pos);
        ASSERT_EQ(typemap_1.to_json(), typemap_2.to_json());
    }


    TEST(TestTypeMap, sync) {
        interserver::TypeMap typemap_1;
        interserver::TypeMap typemap_2;

        typemap_2.base_types_[0]->id_ = typemap_2.base_types_[1]->id_ + 10000;
        const int base_type_id_1 = typemap_1.base_types_[0]->id_;
        const int base_type_id_2 = typemap_2.base_types_[0]->id_;

        const int test_tp_1_id =
                msgtypes::TestTypeMap_1::static_get_type_id(&typemap_1);
        const int test_tp_2_id =
                msgtypes::TestTypeMap_2::static_get_type_id(&typemap_2);

        const int test_tp_1_named_id =
                msgtypes::TestTypeMap_Named_1::static_get_type_id(&typemap_1);
        const int test_tp_2_named_id =
                msgtypes::TestTypeMap_Named_2::static_get_type_id(&typemap_2);

        ASSERT_NE(base_type_id_1, base_type_id_2);
        ASSERT_NE(test_tp_1_id, test_tp_1_named_id);
        ASSERT_NE(test_tp_2_id, test_tp_2_named_id);

        msgtypes::TestTypeMap_1::typemap_register(&typemap_1);
        msgtypes::TestTypeMap_2::typemap_register(&typemap_1);

        msgtypes::TestTypeMap_Named_1::typemap_register(&typemap_2);
        msgtypes::TestTypeMap_Named_2::typemap_register(&typemap_2);

        // before syncing
        ASSERT_NE(
            typemap_1.get_synced_type_id(test_tp_1_id),
            typemap_2.get_synced_type_id(test_tp_1_named_id)
        );
        ASSERT_NE(
            typemap_1.get_synced_type_id(test_tp_2_id),
            typemap_2.get_synced_type_id(test_tp_2_named_id)
        );
        ASSERT_NE(
            typemap_1.get_synced_type_id(base_type_id_1),
            typemap_2.get_synced_type_id(base_type_id_2)
        );

        // after sync
        typemap_2.sync(&typemap_1);
        std::string fisrt_sync_json = typemap_2.to_json();

        ASSERT_EQ(
            typemap_1.get_synced_type_id(test_tp_1_id),
            typemap_2.get_synced_type_id(test_tp_1_named_id)
        );
        ASSERT_EQ(
            typemap_1.get_synced_type_id(test_tp_2_id),
            typemap_2.get_synced_type_id(test_tp_2_named_id)
        );
        ASSERT_EQ(
            typemap_1.get_synced_type_id(base_type_id_1),
            typemap_2.get_synced_type_id(base_type_id_2)
        );

        // second sync does nothing
        typemap_2.sync(&typemap_1);
        ASSERT_EQ(typemap_2.to_json(), fisrt_sync_json);
    }


    TEST(TestTypeMap, to_json) {
        interserver::TypeMap typemap;

        // structs

        int32_t type_id_1 = 1000;
        auto new_type_1 = typemap.add_struct("TEST:struct_1", type_id_1);
        new_type_1->add_field("i8", interserver::MAPTYPE_INT_8);
        new_type_1->add_field("i16", interserver::MAPTYPE_INT_16);
        new_type_1->add_field("i32", interserver::MAPTYPE_INT_32);
        new_type_1->add_field("i64", interserver::MAPTYPE_INT_64);

        int32_t type_id_2 = type_id_1 + 1;
        auto new_type_2 = typemap.add_struct("TEST:struct_2", type_id_2);
        new_type_2->add_field("u8", interserver::MAPTYPE_UINT_8);
        new_type_2->add_field("u16", interserver::MAPTYPE_UINT_16);
        new_type_2->add_field("u32", interserver::MAPTYPE_UINT_32);
        new_type_2->add_field("u64", interserver::MAPTYPE_UINT_64);

        int32_t type_id_3 = type_id_2 + 1;
        auto new_type_3 = typemap.add_struct("TEST:struct_3", type_id_3);
        new_type_3->add_field("f32", interserver::MAPTYPE_FLOAT);
        new_type_3->add_field("d64", interserver::MAPTYPE_DOUBLE);

        int32_t type_id_4 = type_id_3 + 1;
        auto new_type_4 = typemap.add_struct("TEST:struct_4", type_id_4);
        new_type_4->add_field("bool", interserver::MAPTYPE_BOOL);
        new_type_4->add_field("str", interserver::MAPTYPE_STRING);

        int32_t type_id_5 = type_id_4 + 1;
        auto new_type_5 = typemap.add_struct("TEST:struct_5", type_id_5);
        new_type_5->add_field("array", interserver::MAPTYPE_ARRAY);
        new_type_5->add_field("map", interserver::MAPTYPE_MAP);
        new_type_5->add_field("binary", interserver::MAPTYPE_BINARY);

        int32_t type_id_6 = type_id_5 + 1;
        auto new_type_6 = typemap.add_struct("TEST:struct_6", type_id_6);
        new_type_6->add_field("struct_1_field", new_type_1->id_);
        new_type_6->add_field("struct_2_field", new_type_2->id_);
        new_type_6->add_field("struct_3_field", new_type_3->id_);
        new_type_6->add_field("struct_4_field", new_type_4->id_);
        new_type_6->add_field("struct_5_field", new_type_5->id_);

        // functions

        int32_t func_id_1 = 100;
        auto new_func_1 = typemap.add_func("new_func_1", func_id_1);
        new_func_1->add_arg("new_type_1", type_id_1);
        new_func_1->add_arg("new_type_6", type_id_6);

        int32_t func_id_2 = func_id_1 + 1;
        typemap.add_func("new_func_2", func_id_2);

        typemap.base_types_.resize(2); // to make JSON smaller
        std::string result_json = typemap.to_json().c_str();

        std::string expect_json =
                // single quotes will be replaced later,
                // whitespace will be also removed
                "{"
                "    'baseTypes': [{"
                "        'id': 0,"
                "        'name': 'EMTPY',"
                "        'size': 0"
                "    }, {"
                "        'id': 1,"
                "        'name': 'NULL',"
                "        'size': 0"
                "    }],"
                "    'structTypes': [{"
                "        'id': 1000,"
                "        'name': 'TEST:struct_1',"
                "        'fields': [{"
                "            'name': 'i8',"
                "            'typeId': 2"
                "        }, {"
                "            'name': 'i16',"
                "            'typeId': 3"
                "        }, {"
                "            'name': 'i32',"
                "            'typeId': 4"
                "        }, {"
                "            'name': 'i64',"
                "            'typeId': 5"
                "        }]"
                "    }, {"
                "        'id': 1001,"
                "        'name': 'TEST:struct_2',"
                "        'fields': [{"
                "            'name': 'u8',"
                "            'typeId': 6"
                "        }, {"
                "            'name': 'u16',"
                "            'typeId': 7"
                "        }, {"
                "            'name': 'u32',"
                "            'typeId': 8"
                "        }, {"
                "            'name': 'u64',"
                "            'typeId': 9"
                "        }]"
                "    }, {"
                "        'id': 1002,"
                "        'name': 'TEST:struct_3',"
                "        'fields': [{"
                "            'name': 'f32',"
                "            'typeId': 10"
                "        }, {"
                "            'name': 'd64',"
                "            'typeId': 11"
                "        }]"
                "    }, {"
                "        'id': 1003,"
                "        'name': 'TEST:struct_4',"
                "        'fields': [{"
                "            'name': 'bool',"
                "            'typeId': 12"
                "        }, {"
                "            'name': 'str',"
                "            'typeId': 13"
                "        }]"
                "    }, {"
                "        'id': 1004,"
                "        'name': 'TEST:struct_5',"
                "        'fields': [{"
                "            'name': 'array',"
                "            'typeId': 14"
                "        }, {"
                "            'name': 'map',"
                "            'typeId': 15"
                "        }, {"
                "            'name': 'binary',"
                "            'typeId': 16"
                "        }]"
                "    }, {"
                "        'id': 1005,"
                "        'name': 'TEST:struct_6',"
                "        'fields': [{"
                "            'name': 'struct_1_field',"
                "            'typeId': 1000"
                "        }, {"
                "            'name': 'struct_2_field',"
                "            'typeId': 1001"
                "        }, {"
                "            'name': 'struct_3_field',"
                "            'typeId': 1002"
                "        }, {"
                "            'name': 'struct_4_field',"
                "            'typeId': 1003"
                "        }, {"
                "            'name': 'struct_5_field',"
                "            'typeId': 1004"
                "        }]"
                "    }],"
                "    'funcTypes': [{"
                "        'name': 'new_func_1',"
                "        'id': 100,"
                "        'args': [{"
                "            'name': 'new_type_1',"
                "            'type_id': 1000"
                "        }, {"
                "            'name': 'new_type_6',"
                "            'type_id': 1005"
                "        }]"
                "    }, {"
                "        'name': 'new_func_2',"
                "        'id': 101,"
                "        'args': []"
                "    }]"
                "}";

        boost::replace_all(expect_json, " ", "");
        boost::replace_all(expect_json, "'", "\"");

        EXPECT_EQ(typemap.to_json(), expect_json);
    }

} }
