#pragma once

#include <tests/core/utils/front.hpp>

#include "front.hpp"
#include "tests/settings.hpp"


namespace interserver { namespace test {


class ServerAndClients {
    private:
        interserver::ServerProgram*                                server_;
        std::vector<std::unique_ptr<interserver::ClientProgram>>   clients_;
    public:
        ServerAndClients(): server_(nullptr) {};
        ServerAndClients(const ServerAndClients& csc) =delete;
        ~ServerAndClients();

        void set_client_count(size_t clients_count);
        interserver::ClientProgram* get_client(size_t index);
        interserver::ServerProgram* get_server();
        void start_tcp(int port=TEST_SERVER_PORT);
        void stop();
        void clear();
    };

} }
