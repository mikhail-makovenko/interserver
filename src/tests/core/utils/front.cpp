#include "interserver/core.hpp"


namespace interserver { namespace test {

    class ServerFront;

    typedef void(*server_front_func)(
            ServerFront* front, ClientSocketWrapper* client);
    typedef void(*server_front_stop_func)(ServerFront* front);


    class ServerFront : public FrontBase {
    private:
        server_front_func       process_message_ = nullptr;
        server_front_func       on_client_connect_ = nullptr;
        server_front_func       on_client_disconnect_ = nullptr;
        server_front_stop_func  on_server_stop_ = nullptr;
    public:
        using ::interserver::FrontBase::FrontBase;

        void set_process_message_func(server_front_func func);
        void set_on_client_connect_func(server_front_func func);
        void set_on_client_disconnect_func(server_front_func func);
        void set_on_server_stop_func(server_front_stop_func func);

        void read_function_call(ClientSocketWrapper* client);
        void on_client_connect(ClientSocketWrapper* client);
        void on_client_disconnect(ClientSocketWrapper* client);
        void on_server_stop();

        void reset();
    };



    void ServerFront::set_process_message_func(server_front_func func) {
        process_message_ = func;
    }

    void ServerFront::set_on_client_connect_func(server_front_func func) {
        on_client_connect_ = func;
    }

    void ServerFront::set_on_client_disconnect_func(server_front_func func) {
        on_client_disconnect_ = func;
    }

    void ServerFront::set_on_server_stop_func(server_front_stop_func func) {
        on_server_stop_ = func;
    }



    void ServerFront::read_function_call(ClientSocketWrapper* client) {
        if (process_message_) {
            process_message_(this, client);
        }
    }

    void ServerFront::on_client_connect(ClientSocketWrapper* client) {
        if (on_client_connect_) {
            on_client_connect_(this, client);
        }
    }

    void ServerFront::on_client_disconnect(ClientSocketWrapper* client) {
        if (on_client_disconnect_) {
            on_client_disconnect_(this, client);
        }
    }

    void ServerFront::on_server_stop() {
        if (on_server_stop_) {
            on_server_stop_(this);
        }
    }



    void ServerFront::reset() {
        process_message_ = nullptr;
        on_client_connect_ = nullptr;
        on_client_disconnect_ = nullptr;
        on_server_stop_ = nullptr;
    }


} }
