#include "tests/settings.hpp"
#include <vector>
#include "interserver/core.hpp"


namespace interserver { namespace test {


    class ServerAndClients {
    private:
        interserver::ServerProgram*                                server_;
        std::vector<std::unique_ptr<interserver::ClientProgram>>   clients_;
    public:
        ServerAndClients(): server_(nullptr) {};
        ServerAndClients(const ServerAndClients& csc) =delete;
        ~ServerAndClients();

        void set_client_count(size_t clients_count);
        interserver::ClientProgram* get_client(size_t index);
        interserver::ServerProgram* get_server();
        void start_tcp(int port=TEST_SERVER_PORT);
        void stop();
        void clear();
    };


    ServerAndClients::~ServerAndClients() {
        clear();
    }


    void ServerAndClients::set_client_count(size_t clients_count) {
        clear();
        clients_.reserve(clients_count);
        for (size_t i=0; i < clients_count; ++i) {
            clients_.emplace_back(new interserver::ClientProgram());
        }
    }


    interserver::ClientProgram* ServerAndClients::get_client(
            size_t index) {
        return clients_.at(index).get();
    };


    interserver::ServerProgram* ServerAndClients::get_server() {
        return server_;
    }


    void ServerAndClients::start_tcp(int port) {
        if (server_ == nullptr) {
            server_ = new interserver::ServerProgram();
        }
        server_->start_tcp(port, true);
        for (auto& client : clients_) {
            client->start_tcp("127.0.0.1", port);
        }
    }


    void ServerAndClients::stop() {
        for (auto& client : clients_) {
            client->stop();
        }
        server_->stop();
    }


    void ServerAndClients::clear() {
        if (server_) {
            delete server_;
            server_ = nullptr;
        }
        clients_.clear();
    }


} }
