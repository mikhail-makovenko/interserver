/* This settings file is included instead of production version of settings
 * when running tests.
 */
#pragma once

#include "spdlog/spdlog.h"


using spdlog::level::level_enum;


namespace interserver {

    // server config
    const int TCP_SERVER_CLIENT_BUFFER_SIZE = 32*1024;
    const int TCP_SERVER_BUFFER_SIZE = 2*1024*1024;
    // client config
    const int TCP_CLIENT_BUFFER_SIZE = 2*1024*1024;

    // logger levels
    const level_enum LOG_SEVERITY_SERVER = level_enum::trace;
    const level_enum LOG_SEVERITY_CLIENT = level_enum::trace;
    const level_enum LOG_SEVERITY_PROGRAMBASE = level_enum::trace;
    const level_enum LOG_SEVERITY_FRONTBASE = level_enum::trace;
    const level_enum LOG_SEVERITY_SERVER_CORE_FRONT = level_enum::trace;

    // logger settings
    const bool LOGGER_USE_COLOR = false;
    const std::string LOGGER_PATTERN = "[%d-%Y-%m %H-%M-%S.%e][%L] \"%n\": %v";

}
