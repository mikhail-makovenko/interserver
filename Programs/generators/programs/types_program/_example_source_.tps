#include <something>
#include <nothing>

!!CLASSNAME_PREFIX          INTERSERVER::
!!SEPARATE_METHODS_FILE     1

class FirstType
private
    string      str_val

    int32_t     number
public
    bool        bool_val
