#include "interserver/core/messagebase.hpp"
#include <cstring>
#include "interserver/core/buffer.hpp"
#include "interserver/core/typemap.hpp"
#include "_example_dest_.hpp"
#include <something>
#include <nothing>


namespace interserver {
    class Buffer;
    class TypeMap;
}


namespace ns { 


    //--------------------------------------------------------------------------

    // getters --------------------------------------------

    const int32_t FirstType::get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return FirstType::TYPE_ID;
        }
        return typemap->get_synced_id(FirstType::TYPE_ID);
    }

    const int32_t FirstType::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return FirstType::TYPE_ID;
        }
        return typemap->get_synced_id(FirstType::TYPE_ID);
    }

    const std::string& FirstType::get_type_name() {
        return FirstType::TYPE_NAME;
    }

    const std::string& FirstType::static_get_type_name() {
        return FirstType::TYPE_NAME;
    }

    // other methods --------------------------------------

    void FirstType::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        encode_start(buffer, typemap, use_type_id);

        buffer->write(&this->bool_val_);
        buffer->write_value(&this->str_val_);
        buffer->write_4bytes(&this->number_);
        
        encode_end(buffer, typemap, use_type_id);
    }


    void FirstType::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        buffer->read(&this->bool_val_);
        this->str_val_ = buffer->read_value<std::string>();
        buffer->read_4bytes(&this->number_);
        
        decode_end(buffer, typemap, use_type_id);
    }


    void FirstType::typemap_register(::interserver::TypeMap* typemap) {
        auto new_type = typemap->create_struct(
                FirstType::static_get_type_name(),
                FirstType::static_get_type_id(typemap));

        new_type->add_field("bool_val",
                MAPTYPE_BOOL);

        new_type->add_field("str_val",
                MAPTYPE_STRING);

        new_type->add_field("number",
                MAPTYPE_INT_32);

    }


    //--------------------------------------------------------------------------


}