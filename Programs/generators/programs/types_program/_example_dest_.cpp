#include "interserver/core/messagebase.hpp"
#include <cstring>
#include <something>
#include <nothing>


namespace interserver {
    class Buffer;
    class TypeMap;
}


namespace ns { 


    //--------------------------------------------------------------------------

    class FirstType : public ::interserver::MessageBase {
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;

        std::string str_val_;
        int32_t number_;

    public:
        FirstType(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) {
            decode(buffer, typemap, use_type_id);
        };

        bool bool_val_;

        const int32_t get_type_id(
                ::interserver::TypeMap* typemap);
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name();
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);    };


    // constants ------------------------------------------

    const int FirstType::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string FirstType::TYPE_NAME =
            "INTERSERVER::FirstType";


    //--------------------------------------------------------------------------


}