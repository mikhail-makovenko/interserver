{% if header %}{#
#}#pragma once
{% endif %}
{#
#}{% for include in collection.iter_include_preprocessors(methods_file=methods_file) %}{#
#}{{ include }}
{% endfor %}
{% if collection.separate_methods_file %}
namespace interserver {
    class Buffer;
    class TypeMap;
}

{% endif %}
{% for ns in collection.namespaces %}namespace {{ ns }} { {% endfor %}
{% set forward_declared_class_names = collection.get_forward_declared_class_names() %}{#
#}{% if not methods_file and forward_declared_class_names %}
    // forward declaration
{% for class_name in forward_declared_class_names %}{#
#}    class {{ class_name }};
{% endfor %}{% endif %}

    //--------------------------------------------------------------------------
{% for cls in collection.iter_classes() %}{% if not methods_file %}
{{ cls.gen_name_declaration(4, options.class_base) }}
    private:
        static const int TYPE_ID;
        static const std::string TYPE_NAME;{#
#}{% if cls.fields['private'] %}
{% for field in cls.fields['private'] %}
    {{ field.gen_name_declaration(indent=4, pad_top=not loop.first and not last_bottom_padded, pad_bottom=not loop.last) }}{#
#}{% endfor %}{% endif %}{#
#}{% if cls.fields['protected'] %}

    protected:{#
#}{% for field in cls.fields['protected'] %}
    {{ field.gen_name_declaration(indent=4, pad_top=not loop.first and not last_bottom_padded, pad_bottom=not loop.last) }}{#
#}{% endfor %}{% endif %}

    public:
{{ cls.gen_cpp_constructors(8) }}
{% set last_bottom_padded=False %}
{% for field in cls.iter_fields('public') %}{#
    #}{{ field.gen_name_declaration(indent=8, pad_top=not loop.first and not last_bottom_padded, pad_bottom=not loop.last) }}
{% set last_bottom_padded=field.has_top_comment %}{% endfor %}
        const int32_t get_type_id(
                ::interserver::TypeMap* typemap) const;
        static const int32_t static_get_type_id(
                ::interserver::TypeMap* typemap);

        const std::string& get_type_name() const;
        static const std::string& static_get_type_name();

        void encode(
                ::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id) const;
        void decode(::interserver::Buffer* buffer,
                ::interserver::TypeMap* typemap,
                bool use_type_id);

        static void typemap_register(::interserver::TypeMap* typemap);{#
#}{% if cls.literals %}

        // literals from types file{#
#}{% for literal in cls.literals %}

{{ literal.to_string(8) }}

{% endfor %}{% endif %}{#
#}    };
{% endif %}{% if not header or methods_file %}{% if not methods_file %}

    // constants ------------------------------------------

    const int {{ cls.name }}::TYPE_ID =
            ::interserver::MessageBase::gen_type_id();

    const std::string {{ cls.name }}::TYPE_NAME =
            "{{ cls.type_name }}";
{% endif %}{% if not collection.separate_methods_file or methods_file %}
    // getters --------------------------------------------

    const int32_t {{ cls.name }}::get_type_id(
            ::interserver::TypeMap* typemap) const {
        if (typemap == nullptr) {
            return {{ cls.name }}::TYPE_ID;
        }
        return typemap->get_synced_type_id({{ cls.name }}::TYPE_ID);
    }

    const int32_t {{ cls.name }}::static_get_type_id(
            ::interserver::TypeMap* typemap) {
        if (typemap == nullptr) {
            return {{ cls.name }}::TYPE_ID;
        }
        return typemap->get_synced_type_id({{ cls.name }}::TYPE_ID);
    }

    const std::string& {{ cls.name }}::get_type_name() const {
        return {{ cls.name }}::TYPE_NAME;
    }

    const std::string& {{ cls.name }}::static_get_type_name() {
        return {{ cls.name }}::TYPE_NAME;
    }

    // other methods --------------------------------------

    void {{ cls.name }}::encode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) const {

        encode_start(buffer, typemap, use_type_id);

        {% for field in cls.iter_fields() %}{#
        #}{{ field.get_setter_code(8) }};
        {% endfor %}
        encode_end(buffer, typemap, use_type_id);
    }


    void {{ cls.name }}::decode(
            ::interserver::Buffer* buffer,
            ::interserver::TypeMap* typemap,
            bool use_type_id) {

        decode_start(buffer, typemap, use_type_id);

        {% for field in cls.iter_fields() %}{#
        #}{{ field.get_getter_code(8) }};
        {% endfor %}
        decode_end(buffer, typemap, use_type_id);
    }


    void {{ cls.name }}::typemap_register(::interserver::TypeMap* typemap) {
        if (typemap->has_type_name(
                {{ cls.name }}::static_get_type_name())) {
            return;
        }

        auto new_type = typemap->add_struct(
                {{ cls.name }}::static_get_type_name(),
                {{ cls.name }}::static_get_type_id(typemap));
{% for field in cls.iter_fields() %}
        new_type->add_field("{{ field.name }}",
                {{ field.class_id }});
{% endfor %}
    }
{% endif %}{% endif %}

    //--------------------------------------------------------------------------

{% endfor %}
{% for ns in collection.namespaces %}} {% endfor %}
