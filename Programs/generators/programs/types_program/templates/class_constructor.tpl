{% if cls.has_option('CONSTRUCTOR') and 'EMPTY' in cls.options['CONSTRUCTOR'] %}{#
#}{{ cls.name }}(){% if cls.get_constructor_fields('EMPTY') %}:{% for field in cls.get_constructor_fields('EMPTY') %}
    {{ field.property_name }}({{ field.default_value }}){% if not loop.last %},{% endif %}{#
#}{% endfor %}{% endif %} {};
{% endif %}{#
#}{{ cls.name }}(
        ::interserver::Buffer* buffer,
        ::interserver::TypeMap* typemap,
        bool use_type_id) {
    decode(buffer, typemap, use_type_id);
{% raw %}}{% endraw %};{#
#}{% if cls.non_empty_constructors_defined %}{% for c in cls.iter_not_empty_constructors() %}{% if cls.get_constructor_fields(c) %}
{{ cls.name }}({% for field in cls.get_constructor_fields(c) %}{#
#}{{ field.argument_type }} {{ field.name }}{% if not loop.last %}, {% endif %}{#
#}{% endfor %}): {% for field in cls.get_constructor_fields(c) %}
    {{ field.property_name }}({{ field.name }}){% if not loop.last %},{% endif %}{#
#}{% endfor %} {};
{% endif %}{% endfor %}{% endif %}
