from .chunk_base import ChunkBase
from .. import exceptions

class LiteralChunk(ChunkBase):
    def __init__(self, fname, line, lines, index, collection):
        found = False
        for index_, line_ in enumerate(lines[index+1:]):
            if line_.strip().endswith('!}'):
                found = True
                self.skip_source_lines = index_+1
                break

        if not found:
            raise exceptions.TranslatonError(fname, line, index,
                "Failed to find and of literal")

        literal_lines = lines[index+1:index+self.skip_source_lines]
        used_indent = None
        for line in literal_lines:
            if not line:
                continue
            line_indet = len(line) - len(line.lstrip())
            if used_indent is None or line_indet < used_indent:
                used_indent = line_indet

        literal_lines = literal_lines

        self.literal_lines = list(l[used_indent:] for l in literal_lines)

    def to_string(self, indent):
        indent = ' ' * indent
        return '\n'.join('%s%s' % (indent if l.strip() else '', l)
            for l in self.literal_lines)
