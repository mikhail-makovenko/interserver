import re
from .chunk_base import ChunkBase
from .field_chunk import FieldChunk
from .literal_chunk import LiteralChunk

try:
    from ...settings import jinja2_env
except ValueError:
    from settings import jinja2_env

class ClassChunk(ChunkBase):

    IS_COLLECTION = True



    def __init__(self, fname, line, lines, index, collection):
        self.options = {
            'CONSTRUCTOR': [],
        }
        line = self.extruct_no_code_data(fname, line, lines, index, collection)

        self.name = line.strip().lstrip('class').strip()
        self.name = self.name.strip()
        self.fields = {
            'private': [],
            'protected': [],
            'public': [],
        }
        self.current_visibility = 'public'
        self.literals = []
        self.collection = collection
        self.classname_prefix = \
            collection.top_collection.template_options['CLASSNAME_PREFIX']


    @property
    def namespaced_name(self):
        return '::%s::%s' % (
            '::'.join(self.top_collection.namespaces),
            self.name,
        )


    @property
    def non_empty_constructors_defined(self):
        return (self.has_option('CONSTRUCTOR') and
                    (not len(self.options['CONSTRUCTOR']) == 1 or
                        self.options['CONSTRUCTOR'][0] != 'EMPTY'))

    def iter_not_empty_constructors(self):
        if not self.non_empty_constructors_defined:
            return []
        for constructor in self.options['CONSTRUCTOR']:
            if constructor != 'EMPTY':
                yield constructor


    def get_constructor_fields(self, constructor_type):
        result = []
        if constructor_type in ('EMPTY', 'BASIC'):
            for key in self.fields:
                for field in self.fields[key]:
                    if field.is_basic_type and field.singular:
                        result.append(field)
        return result


    def gen_cpp_constructors(self, indent=4):
        indent = ' '*indent;
        template = jinja2_env.get_template('class_constructor.tpl')
        output = template.render(
            cls=self,
            collection=self.collection).strip()
        if indent:
            output = '\n'.join(
                ['%s%s' % (indent, line) for line in output.split('\n')])
        return output


    def add(self, chunk):
        if isinstance(chunk, FieldChunk):
            self.fields[self.current_visibility].append(chunk)
            return

        if isinstance(chunk, LiteralChunk):
            self.literals.append(chunk)
            return

        raise Exception('ClassChunk cannot contain chunk of class: %s' %
            chunk.__class__.__name__)


    @property
    def type_name(self):
        if self.has_option('NAME'):
            name = self.options['NAME'][0]
        else:
            name = self.name
        return '%s%s' % (self.classname_prefix, name)


    @property
    def constant(self):
        return self.cls_constant(self.name)


    def iter_fields(self, visibility=None):
        if visibility is None:
            for key in self.fields:
                for field in self.fields[key]:
                    yield field
        else:
            for field in self.fields[visibility]:
                yield field


    def gen_name_declaration(self, indent, basename):
        result = 'class %s : public %s {' % (self.name, basename)
        result = self.add_comments(result, indent).rstrip()
        return result
