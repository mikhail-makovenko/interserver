import os
import itertools
from .. import typemap
from .class_chunk import ClassChunk
from .preprocessor_include_chunk import PreprocessorIncludeChunk
from .preprocessor_include_chunk import MockPreprocessorIncludeChunk


class ChunkCollection(object):

    IS_COLLECTION = True

    def __init__(self):
        self.dest_file = None

        self.top_collection = self
        self.template_options = {
            'SEPARATE_METHODS_FILE': '0',
            'CLASSNAME_PREFIX': '',
            'NAMESPACE': 'ns',
        }

        self.preprocessors = {
            'include': [],
            '__include': [],
        }
        self.classes = []
        self.collection = None


    def add(self, chunk):
        if isinstance(chunk, ClassChunk):
            self.classes.append(chunk)
            return

        if isinstance(chunk, PreprocessorIncludeChunk):
            self.preprocessors['include'].append(chunk)
            return

        raise Exception('ChunkCollection cannot contain chunk of class: %s' %
            chunk.__class__.__name__)


    def add_template_option_from_line(self, options_str):
        key, value = options_str[2:].split(' ', 1)  # [2:] skip '!!'
        self.template_options[key.strip()] = value.strip()


    def add_preprocessor(self, key, value, allow_double=True):
        if not allow_double:
            for v in self.preprocessors[key]:
                if v.inclusion_chunk == value.inclusion_chunk:
                    return

        self.preprocessors[key].append(value)


    def iter_include_preprocessors(self, methods_file):
        for preprocessor in self.preprocessors['__include']:
            yield preprocessor


        if methods_file:
            yield MockPreprocessorIncludeChunk(
                    '#include "interserver/core/buffer.hpp"')

            yield MockPreprocessorIncludeChunk(
                    '#include "interserver/core/typemap.hpp"')

            yield MockPreprocessorIncludeChunk(
                    '#include "%s.hpp"' % os.path.basename(self.dest_file))

        for preprocessor in self.preprocessors['include']:
            yield preprocessor


    def iter_classes(self):
        for cls in self.classes:
            yield cls


    def iter_forward_declared_class_names(self):
        known_class_names = set()
        for cls in self.iter_classes():
            for field in cls.iter_fields():
                if not field.is_basic_type:
                    known_class_names.add(field.type)

        for cls_name in known_class_names:
            found = False
            for declared_cls in self.iter_classes():
                if declared_cls.name == cls_name:
                    found = True
                    break

            if not found:
                yield cls_name


    def get_forward_declared_class_names(self):
        return sorted(list(self.iter_forward_declared_class_names()))


    @property
    def separate_methods_file(self):
        return self.template_options['SEPARATE_METHODS_FILE'] == '1'

    @property
    def namespaces(self):
        return self.template_options['NAMESPACE'].split('::')



    def prepare(self, dest_file):
        self.dest_file = dest_file

        self.add_preprocessor(
            '__include',
            MockPreprocessorIncludeChunk(
                '#include "interserver/core/messagebase.hpp"'),
            False)

        '''
        self.add_preprocessor(
            '__include',
            MockPreprocessorIncludeChunk(
                '#include "interserver/core/typemap_constants.hpp"'),
            False)
        '''

        if not self.separate_methods_file:
            self.add_preprocessor(
                '__include',
                MockPreprocessorIncludeChunk(
                    '#include "interserver/core/buffer.hpp"'),
                False)

            self.add_preprocessor(
                '__include',
                MockPreprocessorIncludeChunk(
                    '#include "interserver/core/typemap.hpp"'),
                False)

        add_string = False
        for cls in self.iter_classes():
            for field in cls.iter_fields():
                if field.type == 'std::string':
                    add_string = True
                    break

        if add_string:
            self.add_preprocessor(
                '__include',
                MockPreprocessorIncludeChunk('#include <cstring>'),
                False)
