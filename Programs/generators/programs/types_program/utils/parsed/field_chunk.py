from .preprocessor_include_chunk import MockPreprocessorIncludeChunk
from .chunk_base import ChunkBase
from .. import typemap


class FieldChunk(ChunkBase):

    CONTAINER_VECTOR = 1

    def __init__(self, fname, line, lines, index, collection):
        line = self.extruct_no_code_data(fname, line, lines, index, collection)

        self.is_array_of_raw = False
        self.is_array_of_wrappers = False
        self.type, self.name = line.strip().rsplit(' ', 1)
        self.type = self.type.strip()
        if self.type == 'string':
            self.type = 'std::string'

        self.modifiers = []
        if ' ' in self.type:
            parts = [p.strip() for p in self.type.split(' ') if p.strip()]
            self.modifiers, self.type = parts[:-1], parts[-1]

        self.name = self.name.strip()
        self.collection = collection

        self.type_containers = []
        if self.type.endswith('[]'):
            while self.type.endswith('[]'):
                self.type = self.type[:-2]
                self.type_containers.append(self.CONTAINER_VECTOR)

            self.top_collection.add_preprocessor(
                '__include',
                MockPreprocessorIncludeChunk('#include <vector>'),
                False)

            if self.is_basic_type:
                self.is_array_of_raw = True
            else:
                self.is_array_of_wrappers = True

                self.top_collection.add_preprocessor(
                    '__include',
                    MockPreprocessorIncludeChunk('#include <memory>'),
                    False)


    @property
    def is_array(self):
        return self.is_array_of_raw or self.is_array_of_wrappers


    @property
    def property_name(self):
        return '%s_' % self.name


    @property
    def property_type(self):
        property_type = '%s %s' % \
            (' '.join(self.modifiers), self.__property_type())
        return property_type.strip()


    def __property_type(self):
        current_type_str='%s'
        type_containers = self.type_containers
        while len(type_containers):
            type_containers = type_containers[:-1]
            current_type_str = (current_type_str %'std::vector<%s>')

            if len(type_containers):
                continue

            if self.is_basic_type:
                pass
            else:
                current_type_str = (current_type_str % 'std::shared_ptr<%s>')

        return current_type_str % self.type


    @property
    def argument_type(self):
        if self.is_basic_type and self.type != 'std::string':
            return self.type
        return 'const %s&' % self.type


    @property
    def class_id(self):
        if self.type in typemap.BASE_ASSIGNABLE_TYPES:
            return typemap.BASE_ASSIGNABLE_TYPES[self.type]['const']

        # reference to another class
        return '%s::static_get_type_id(typemap)' % self.type


    @property
    def is_basic_type(self):
        return (self.type in typemap.BASE_ASSIGNABLE_TYPES) or \
            self.type == 'std::string'


    @property
    def default_value(self):
        if not self.is_basic_type:
            raise Exception('Not a basic type')
        if self.type == 'bool':
            return 'false'
        if self.type == 'std::string':
            return '""'
        return 0;


    @property
    def singular(self):
        return not self.type_containers

    def get_getter_code(self, indent=0):
        indent = ' ' * indent
        if self.is_array:
            result = '%s.clear();\n' % self.property_name
            if self.is_array_of_raw:
                if self.type != 'std::string':
                    return (result + \
                            '%sbuffer->read_container_numbers'
                            '(&%s, typemap, false)' %
                                (indent, self.property_name))
                else:
                    return (result + \
                            '%sbuffer->read_container_strings'
                            '(&%s, typemap, false)' %
                                (indent, self.property_name))
            if self.is_array_of_wrappers:
                return (result +
                        '%sbuffer->read_container_wrapped_pointers('
                        '&%s, typemap, false)' %
                            (indent, self.property_name))

        if self.type in typemap.BASE_ASSIGNABLE_TYPES:
            return 'buffer->%s(&%s)' % (
                typemap.BASE_ASSIGNABLE_TYPES[self.type]['getter'],
                self.property_name,
            )

        return '%s.decode(buffer, typemap, false)' % self.property_name


    def get_setter_code(self, indent=0):
        indent = ' ' * indent
        if self.is_array:
            result = ''

            if self.is_array_of_raw:
                if self.type != 'std::string':
                    return (result +
                        'buffer->write_container_numbers'
                        '(&%s, typemap, false)' % \
                                (self.property_name))
                else:
                    return (result +
                        'buffer->write_container_strings'
                        '(&%s, typemap, false)' % \
                                (self.property_name))
            if self.is_array_of_wrappers:
                return (result +
                    'buffer->write_container_wrapped_pointers('
                    '&%s, typemap, false)' % \
                            (self.property_name))

        if self.is_basic_type:
            return 'buffer->%s(&%s)' % (
                typemap.BASE_ASSIGNABLE_TYPES[self.type]['setter'],
                self.property_name,
            )
        return "%s.encode(buffer, typemap, false)" % self.property_name


    def gen_name_declaration(self, indent=0, pad_top=0, pad_bottom=0):
        result = '%s %s;' % (self.property_type, self.property_name)

        result = self.add_comments(result, indent, pad_top, pad_bottom)

        return result
