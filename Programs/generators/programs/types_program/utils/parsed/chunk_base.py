import re


class ChunkBase(object):

    IS_COLLECTION = False
    skip_source_lines = 0

    @staticmethod
    def to_underscore_style(str):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', str)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


    @property
    def top_collection(self):
        top_collection = self.collection
        while top_collection.collection:
            top_collection = top_collection.collection
        return top_collection


    def extruct_no_code_data(self, fname, line, lines, index, collection):
        self.inline_comment = ''
        if '//' in line:
            line, self.inline_comment = line.split('//', 1)
            self.inline_comment = '//%s' % self.inline_comment.rstrip()

        self.top_comment_lines = []
        index -= 1
        while index >= 0 and lines[index].strip().startswith('//'):
            comment_line = lines[index].strip()
            self.top_comment_lines.append(comment_line)
            index -= 1
        self.top_comment_lines = list(reversed(self.top_comment_lines))

        if not hasattr(self, 'options'):
            self.options = {}
        if '!!' in line:
            line, options = line.split('!!', 1)
            self.options = {
                o['name']: o['args']
                    for o in [self.__process_option(o) for o in
                                options.split(';') if o.strip()]
            }
        return line


    def has_option(self, option_name):
        return option_name in self.options and len(self.options[option_name])


    @property
    def has_top_comment(self):
        return len(self.top_comment_lines) != 0


    def add_comments(self, string, indent=0, pad_top=0, pad_bottom=0):
        indent = ' '*indent
        statement = '%s%s' % (indent, string)
        if self.inline_comment:
            statement = '%s  %s' % (statement, self.inline_comment)

        result = ''
        for line in self.top_comment_lines:
            result = '%s%s%s\n' % (result, indent, line)

        if self.top_comment_lines:
            statement = '%s%s%s%s' % \
                ('\n' * pad_top, result, statement, '\n')

        return statement


    def __process_option(self, option):
        option = option.strip()
        if not option:
            return

        name, args = re.search(r'([^\(]+)([\s\S]*)', option).groups()
        name = name.strip()
        args = args.strip('()')
        args = [a.strip() for a in args.split(',') if a.strip()]
        return {
            'name': name,
            'args': args,
        }
