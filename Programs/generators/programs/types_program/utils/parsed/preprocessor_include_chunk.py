from .chunk_base import ChunkBase



class PreprocessorIncludeChunk(ChunkBase):

    def __init__(self, fname, line, lines, index, collection):
        parts = line.split('//', 1)
        self.inclusion_chunk = parts[0].strip()
        self.inline_comment = parts[1] if len(parts) == 2 else ""


    def __str__(self):
        return ("%s  %s" % (self.inclusion_chunk, self.inline_comment)).strip()



class MockPreprocessorIncludeChunk(PreprocessorIncludeChunk):

    def __init__(self, inclusion_chunk, inline_comment=""):
        self.inclusion_chunk = inclusion_chunk
        self.inline_comment = inline_comment
