BASE_ASSIGNABLE_TYPES = {
    # signed int
    'int8_t': {
        'const': 'MAPTYPE_INT_8',
        'size': 1,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'int16_t': {
        'const': 'MAPTYPE_INT_16',
        'size': 2,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'int32_t': {
        'const': 'MAPTYPE_INT_32',
        'size': 4,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'int64_t': {
        'const': 'MAPTYPE_INT_64',
        'size': 8,
        'getter': 'read_value',
        'setter': 'write_value',
    },

    # unsigned int
    'uint8_t': {
        'const': 'MAPTYPE_UINT_8',
        'size': 1,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'uint16_t': {
        'const': 'MAPTYPE_UINT_16',
        'size': 2,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'uint32_t': {
        'const': 'MAPTYPE_UINT_32',
        'size': 4,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'uint64_t': {
        'const': 'MAPTYPE_UINT_64',
        'size': 8,
        'getter': 'read_value',
        'setter': 'write_value',
    },

    # floats
    'float': {
        'const': 'MAPTYPE_FLOAT',
        'size': 4,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'double': {
        'const': 'MAPTYPE_DOUBLE',
        'size': 8,
        'getter': 'read_value',
        'setter': 'write_value',
    },

    # other
    'std::string': {
        'const': 'MAPTYPE_STRING',
        'size': -1,
        'getter': 'read_value',
        'setter': 'write_value',
    },
    'bool': {
        'const': 'MAPTYPE_BOOL',
        'size': 1,
        'getter': 'read_value',
        'setter': 'write_value',
    },

}

BASE_UNASSIGNABLE_TYPE_IDS = {
    'array': 'MAPTYPE_ARRAY',
    'map': 'MAPTYPE_MAP',
    'binary': 'MAPTYPE_BINARY',
}
