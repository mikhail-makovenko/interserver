from . import parsed
from . import exceptions
from .parsed.preprocessor_include_chunk import MockPreprocessorIncludeChunk

try:
    from ..settings import jinja2_env
except ValueError:
    from types.settings import jinja2_env


def __get_chunk_class_for_line(fname, line, lines, index, collection):
    if line.lstrip().startswith('//'):
        return

    if line.lstrip().startswith('{!'):
        return parsed.LiteralChunk

    if line.startswith('#include'):
        return parsed.PreprocessorIncludeChunk

    if line.startswith('class'):
        return parsed.ClassChunk

    if isinstance(collection, parsed.ClassChunk):
        return parsed.FieldChunk

    raise exceptions.TranslatonError(fname, line, index)


def __translate_line(fname, line, lines, index, collection):
    ''' If the translated chunk is a collection itself, then return the
        collection.
    '''
    if line.startswith('!!'):
        collection.top_collection.add_template_option_from_line(line)
        return

    ChunkConstructor = \
        __get_chunk_class_for_line(fname, line, lines, index, collection)
    if not ChunkConstructor:
        return

    chunk = ChunkConstructor(fname, line, lines, index, collection)

    try:
        collection.add(chunk)
    except Exception as e:
        raise exceptions.TranslatonError(fname, line, index, str(e))

    return chunk


def translate_file(source, dest, options={}):
    chunk_collection = parsed.ChunkCollection()
    chunk_stack = [
        [chunk_collection, 0]
    ]

    with open(source, 'r') as source_fp:
        lines = source_fp.read().split('\n')

    indent = False
    index = -1  # +1 will be later
    while index < len(lines)-1:
        index += 1
        line = lines[index]
        indent_length = len(line) - len(line.lstrip())
        line = line.strip()
        if not line:
            continue

        if line in ('public', 'private', 'protected'):
            chunk_stack[-1][0].current_visibility = line
            continue


        if indent:
            indent = False
            if indent_length <= chunk_stack[-1][1]:  # nothing inside
                chunk_stack = chunk_stack[:-1]
            else:
                chunk_stack[-1][1] = indent_length

        for i in range(len(chunk_stack)):
            if chunk_stack[i][1] > indent_length:
                chunk_stack = chunk_stack[:i]
                break

        chunk = \
            __translate_line(source, line, lines, index, chunk_stack[-1][0])

        if chunk:
            index += chunk.skip_source_lines
            if chunk.IS_COLLECTION:
                indent = True
                chunk_stack.append([chunk, indent_length])

    chunk_collection.prepare(dest)

    template = jinja2_env.get_template('types.tpl')

    with open('%s.cpp' % dest, 'w') as dest_fp:
        output = template.render(
            methods_file=False,
            header=False,
            collection=chunk_collection,
            options=options).strip()
        dest_fp.write(output)

    with open('%s.hpp' % dest, 'w') as dest_fp:
        output = template.render(
            methods_file=False,
            header=True,
            collection=chunk_collection,
            options=options).strip()
        dest_fp.write(output)

    if chunk_collection.separate_methods_file:
        chunk_collection.add_preprocessor(
            '__include',
            MockPreprocessorIncludeChunk(
                '#include "interserver/core/buffer_methods.hpp"'),
            False)

        with open('%s_methods.cpp' % dest, 'w') as dest_fp:
            output = template.render(
                methods_file=True,
                header=False,
                collection=chunk_collection,
                options=options).strip()
            dest_fp.write(output)
