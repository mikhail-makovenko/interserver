class TranslatonError(Exception):
    def __init__(self, fname, line, index, msg='Syntax error'):
        msg = ('Exception:\n'
            'message: %s\n'
            'file: %s\n'
            'line_no: %d\n'
            'line: %s' % (msg, fname, index, line))
        Exception.__init__(self, msg)
