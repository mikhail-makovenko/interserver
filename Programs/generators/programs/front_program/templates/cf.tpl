{% if header_file %}#pragma once

{% endif %}{#
#}#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/clientsocketwrapper.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "../{{source_file_name}}"
{% for include in includes %}{#
#}{{include}}
{% endfor %}

{% for namespace in namespaces %}namespace {{ namespace }} {
{% endfor %}{#
#}namespace CF {
namespace {{front_added_namespace}} {
namespace {{class_name}} {

    // constants
{% for func_data in funcs_data %}{#
#}    const int32_t {{func_data.const_name}} = {{func_data.const_value}};
{% endfor %}
{% for namespace in namespaces %}} {% endfor %}} } }


{% for namespace in namespaces %}namespace {{ namespace }} { {% endfor %}

    //--------------------------------------------------------------------------

    void {{class_name}}::read_function_call(
            ::interserver::ClientSocketWrapper* client){% if header_file %};
                {% else %} {

        int32_t func_id;
        client->read_buffer_->read_value(&func_id);
        func_id = get_program()->get_synced_func_id(func_id);

        {% for func_data in funcs_data %}if (func_id == {% for namespace in namespaces %}::{#
                #}{{ namespace }}{% endfor %}::CF::{{front_added_namespace}}::{{class_name}}::{#
                #}{{func_data.const_name}}) { {#
            #}{% for arg in func_data.args %}
            {{arg.as_variable_declaration}};
            {{ arg.get_getter_code(16) }};
            {% endfor %}
            {{func_data.name}}(client{% if func_data.args %}, {% endif %}{% for arg in func_data.args %}{#
            #}{{arg.name}}{% if not loop.last %}, {% endif %}{#
            #}{% endfor %});
        }{% endfor %}
        else {
            client->write_error_message("Unknown function ID: " +
                    std::to_string(func_id), get_logger());
        }
    }
{% endif %}
    //--------------------------------------------------------------------------

    void {{class_name}}::typemap_register(TypeMap* typemap){% if header_file %};{% else %} {
        if (typemap == nullptr) {
            return;
        }

        {% if args_present %}::interserver::FuncType* new_func;
        {% endif %}{#
        #}{% for func_data in funcs_data %}
        {% if func_data.args|length %}new_func = {% endif %}typemap->add_func(
            "{% for namespace in namespaces %}::{{ namespace }}{% endfor %}::{#
                #}CF::{{front_added_namespace}}::{{class_name}}::{{func_data.name}}",
            {% for namespace in namespaces %}::{{ namespace }}{% endfor %}{#
            #}::CF::{{front_added_namespace}}::{{class_name}}::{{func_data.const_name}});
    {% for arg in func_data.args %}
        new_func->add_arg("{{arg.name}}", {% if arg.is_basic_type %}{#
            #}::interserver::TYPEMAP_ID<{{arg.property_type}}>::ID{#
            #}{% else %}{{arg.full_type}}::static_get_type_id(nullptr){% endif %});{#
    #}{% endfor %}{#
    #}{% endfor %}
    }{% endif %}

{% for namespace in namespaces %}} {% endfor %}
