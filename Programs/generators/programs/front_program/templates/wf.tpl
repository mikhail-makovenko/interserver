{% if header_file %}#pragma once

{% endif %}{#
#}#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
{% for include in includes %}{#
#}{{include}}
{% endfor %}

{% for namespace in namespaces %}namespace {{ namespace }} {
{% endfor %}{#
#}namespace WF {
namespace {{front_added_namespace}} {
namespace {{class_name}} {

    // constants
{% for func_data in funcs_data %}{#
#}    const int32_t {{func_data.const_name}} = {{func_data.const_value}};
{% endfor %}
    //--------------------------------------------------------------------------
{% for func_data in funcs_data %}
    void {{func_data.name}}(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer{% if func_data.args %},{% endif %}{#
#}{% for arg in func_data.args %}
            {{arg.as_argument_declaration}}{% if not loop.last %},{% endif %}{#
#}{% endfor %}{#
#}){% if header_file %};
{% else %} {
        int32_t temp = program->get_synced_type_id(
            ::interserver::MAPTYPE_FUNCTION_CALL);
        buffer->write_value(&temp);
        temp = 0;
        if (front) {
            temp = front->get_other_id();
        }
        buffer->write_value(&temp);
        temp = program->get_synced_func_id({{func_data.const_name}});
        buffer->write_value(&temp);

{% for arg in func_data.args %}{#
#}        {{ arg.get_setter_code(8) }};
{% endfor %}{#
#}    }
{% endif %}{# if header file #}
    //--------------------------------------------------------------------------
{% endfor %}


    void typemap_register(TypeMap* typemap){% if header_file %};{% else %} {
        {% if args_present %}::interserver::FuncType* new_func;
        {% endif %}{#
        #}{% for func_data in funcs_data %}
        {% if func_data.args|length %}new_func = {% endif %}typemap->add_func(
            "{% for namespace in namespaces %}::{{ namespace }}{% endfor %}::{#
                #}CF::{{front_added_namespace}}::{{class_name}}::{{func_data.name}}",
            {{func_data.const_name}});
    {% for arg in func_data.args %}
        new_func->add_arg("{{arg.name}}", {% if arg.is_basic_type %}{#
            #}::interserver::TYPEMAP_ID<{{arg.property_type}}>::ID{#
            #}{% else %}{{arg.full_type}}::static_get_type_id(nullptr){% endif %});{#
    #}{% endfor %}{#
    #}{% endfor %}
    }{% endif %}

{% for namespace in namespaces %}} {% endfor %}} } }
