import os
import jinja2

BASE_ROOT = os.path.dirname(os.path.realpath(__file__))

jinja2_loader = jinja2.FileSystemLoader(os.path.join(BASE_ROOT, 'templates'))
jinja2_env = jinja2.Environment(loader=jinja2_loader)


BASE_CLASS_NAME = 'ServerFrontBase'
