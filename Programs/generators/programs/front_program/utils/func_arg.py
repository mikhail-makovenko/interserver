import re
from types_program.utils.parsed.field_chunk import FieldChunk
from types_program.utils.parsed.chunk_collection import ChunkCollection


class FuncArg(FieldChunk):

    CONTAINER_VECTOR = 1

    def __init__(self, type_, name, default, namespaces):
        self.namespaces = '::%s' % '::'.join(namespaces)
        self.name = name
        self.type = type_
        self.default = default
        self.type_containers = []
        self.collection = ChunkCollection()
        self.is_array_of_raw = False
        self.is_array_of_wrappers = False

        self.pass_by_refference = self.type.endswith('&')
        self.type = self.type.strip('&')

        is_array = False
        if self.type.endswith('>'):
            self.type_containers.append(self.CONTAINER_VECTOR)

            is_array = True
            self.pass_by_refference = True
            match = re.search(r'<([\s\S]*?>)>', self.type)
            if match:
                self.type = match.groups()[0].strip()

        if self.type.endswith('>'):
            match = re.search(r'<([\s\S]*?)>', self.type)
            if match:
                self.type = match.groups()[0].strip()

        if is_array:
            if self.is_basic_type:
                self.is_array_of_raw = True
            else:
                self.is_array_of_wrappers = True

        self.modifiers = []


    @property
    def property_type(self):
        property_type = FieldChunk.property_type.fget(self)
        if self.pass_by_refference:
            property_type = '%s&' % property_type

        if not self.is_basic_type and not self.type.startswith('::'):
            type_ = '%s::%s' % (self.namespaces, self.type)
            property_type = property_type.replace(
                '<%s>' % self.type,
                '<%s>' % type_
            )

        return property_type


    @property
    def full_type(self):
        property_type = self.type
        if not self.is_basic_type and not self.type.startswith('::'):
            property_type = '%s::%s' % (self.namespaces, self.type)

        return property_type


    @property
    def as_argument_declaration(self):
        if  self.default is None:
            return '%s %s' % (self.property_type, self.property_name)

        return '%s %s=%s' % \
            (self.property_type, self.property_name, self.default)


    @property
    def as_variable_declaration(self):
        result = self.as_argument_declaration.rstrip('&');
        if '=' in result:
            result = result.split('=', 1)[0].strip()
        return result;


    def __repr__(self):
        if  self.default is not None:
            return '[%s %s=%s]' % \
                (self.property_type, self.property_name, self.default)
        else:
            return '[%s %s]' % \
                (self.property_type, self.property_name)
