from .func_arg import FuncArg


class FuncData(object):

    __last_id = 0;

    def __init__(self, name, args, namespaces, opts):
        FuncData.__last_id += 1
        self.id = FuncData.__last_id;
        self.options = opts
        self.name = name
        self.args = \
            [FuncArg(arg[0], arg[1], arg[2], namespaces) for arg in args]

    @property
    def const_name(self):
        return '%s_ID' % self.name.upper()

    def __repr__(self):
        return '<FuncData: {} {}>'.format(self.name, self.args)


    @property
    def const_value(self):
        if self.options and 'const_id' in self.options:
            return self.options['const_id']
        else:
            return '::interserver::FrontBase::gen_func_id()'
