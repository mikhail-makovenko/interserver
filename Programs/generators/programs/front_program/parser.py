import os
import re
from . import settings
from .settings import jinja2_env
from .utils.func_data import FuncData


FUNC_PREFIX = '(f_|sf_|cf_)'


def generate_cpp_files(source_file_path,
        dest_sender_folder_path, dest_receiver_folder_path, is_server_front):

    if source_file_path.endswith('_wf.cpp') or \
            source_file_path.endswith('_wf.hpp') or \
            source_file_path.endswith('_rw.hpp') or \
            source_file_path.endswith('_rw.hpp'):
        print('File ignored: %s' % source_file_path)
        return

    with open(source_file_path) as fp:
        content_ = fp.read()

    # remove comments
    content_ = re.sub(r'/\*[\s\S]*?\*/', '', content_)

    content = ''
    for line in content_.split('\n'):
        if '//' in line:
            if not re.match(r'#\s*include[\s\S]*', line) and \
                    not line.strip().startswith('//=='):
                line = line.split('//', 1)[0]
        content = '%s\n%s' % (content, line)

    '''
    match = re.search(
        r'(public|protected|private)\s*?%s[^\w\d]' % settings.BASE_CLASS_NAME,
        content)
    if not match:
        return
    '''

    includes = []
    for include in re.finditer(r'#\s*include[^\n]*//[^\n]*!!include', content):
        include = include.group(0)
        if '//' in include:
            include = include.split('//', 1)[0]
        includes.append(include.strip())

    namespaces = []
    for namespace in re.finditer(
            r'\s*[\n{]\s*namespace\s+([\w\d]*)\s*', content):
        namespaces.append(namespace.group(1))

    match = re.search(r'\n\s*void read_function_call\(([\s\S]*?)\)[\s\n]*;',
        content)
    if not match:
        print('No `read_function_call` function: %s' % source_file_path)
        return

    class_name = re.search(r'class\s+([\w\d]*)', content).group(1)

    func_declarations = set()
    funcs = []
    for match in re.finditer(
            r'(//==[^\n]*\s*)?(void[\s]+%s[^\(]*?\([^\)]*?\))' % FUNC_PREFIX,
            content):

        func_declaration = match.group(2)
        func_declaration = re.sub(r',\s+', ',', func_declaration)
        func_declaration = re.sub(r'\s+', ' ', func_declaration)
        func_declaration = func_declaration.\
            replace(' (', '(').\
            replace(' )', ')').strip()

        if func_declaration in func_declarations:
            continue
        func_declarations.add(func_declaration)
        funcs.append([func_declaration, match.group(1)])

    funcs_data = []
    for func_d, options in funcs:
        opts = {}
        #print(options)
        if options:
            options = options.strip().lstrip('/').lstrip('=').lstrip()
            options = options.split(',')
            for opt in options:
                key, value = opt.split('=')
                opts[key.strip()] = value.strip()


        match = re.search(r'void\s+([^\(]+)\(([^\)]*?)\)', func_d)
        func_name, args = match.groups()
        func_name = func_name.strip()
        args = [a.strip().split(' ', 1) for a in args.split(',') if a.strip()]
        args = [
            #skip `ClientSocketWrapper` argument
            [type_, name] for type_, name in args[1:]
        ]

        func_args = []
        for arg in args:
            parts = arg[1].split('=', 1)
            name = parts[0]
            if len(parts) == 2:
                default = parts[1]
            else:
                default = None

            #type, name, default
            func_args.append([arg[0], name, default])

        #check if we already have it
        found = False
        for func_data in funcs_data:
            if func_data.name != name:
                continue

            same_args = True
            for index, arg in enumerate(func_data.args):
                if func_args[index][0] != arg.name:
                    same_args = False
                    break

            if same_args:
                found = True
                break

        if not found:
            funcs_data.append(FuncData(func_name, func_args, namespaces, opts))


    source_file_name = os.path.basename(source_file_path)
    if '.' in source_file_name:
        source_file_name = source_file_name.rsplit('.', 1)[0]

    args_present = False
    for func_data in funcs_data:
        if func_data.args:
            args_present = True
            break

    front_added_namespace = "S" if is_server_front else "C"

    template_kwargs = {
        'is_server_front': is_server_front,
        'front_added_namespace': front_added_namespace,
        'namespaces': namespaces,
        'class_name': class_name,
        'includes': includes,
        'funcs_data': funcs_data,
        'args_present': args_present,
    }

    template = jinja2_env.get_template('wf.tpl')
    output = template.render(
        header_file=False,
        **template_kwargs).strip()
    dest = os.path.join(dest_receiver_folder_path,
        'wf/%s_wf.cpp' % source_file_name)
    with open(dest, 'w') as fp:
        fp.write(output)


    output = template.render(
        header_file=True,
        **template_kwargs).strip()
    dest = os.path.join(dest_receiver_folder_path,
        'wf/%s_wf.hpp' % source_file_name)
    with open(dest, 'w') as fp:
        fp.write(output)


    template = jinja2_env.get_template('cf.tpl')
    output = template.render(
        source_file_name=source_file_name + '.hpp',
        header_file=False,
        **template_kwargs).strip()
    dest = os.path.join(dest_sender_folder_path,
        'cf/%s_cf.cpp' % source_file_name)
    with open(dest, 'w') as fp:
        fp.write(output)

    output = template.render(
        source_file_name=source_file_name + '.hpp',
        header_file=True,
        **template_kwargs).strip()
    dest = os.path.join(dest_sender_folder_path,
        'cf/%s_cf.hpp' % source_file_name)
    with open(dest, 'w') as fp:
        fp.write(output)
