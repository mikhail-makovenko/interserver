#pragma once

#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "interserver/core/clientsocketwrapper.hpp"


namespace interserver {
namespace uhaha {
namespace WF {
namespace C {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_FRONTS_ID = ::interserver::FrontBase::gen_func_id();
    const int32_t F_OTHER_ID = -1;
    const int32_t F_NEW_ID = ::interserver::FrontBase::gen_func_id();

    //--------------------------------------------------------------------------

    void f_sync_fronts(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer,
            std::vector<int32_t>& v1_,
            std::vector<std::shared_ptr<::interserver::uhaha::MyType>>& v2_,
            std::vector<std::shared_ptr<::ns::MyType>>& v3_,
            std::vector<std::shared_ptr<::interserver::uhaha::ns::MyType>>& v4_,
            std::string ono_="200");

    //--------------------------------------------------------------------------

    void f_other(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer,
            bool flag_,
            int32_t i_);

    //--------------------------------------------------------------------------

    void f_new(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer);

    //--------------------------------------------------------------------------



    void typemap_register(TypeMap* typemap);

} } } } }