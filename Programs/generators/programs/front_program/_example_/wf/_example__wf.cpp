#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "interserver/core/clientsocketwrapper.hpp"


namespace interserver {
namespace uhaha {
namespace WF {
namespace C {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_FRONTS_ID = ::interserver::FrontBase::gen_func_id();
    const int32_t F_OTHER_ID = -1;
    const int32_t F_NEW_ID = ::interserver::FrontBase::gen_func_id();

    //--------------------------------------------------------------------------

    void f_sync_fronts(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer,
            std::vector<int32_t>& v1_,
            std::vector<std::shared_ptr<::interserver::uhaha::MyType>>& v2_,
            std::vector<std::shared_ptr<::ns::MyType>>& v3_,
            std::vector<std::shared_ptr<::interserver::uhaha::ns::MyType>>& v4_,
            std::string ono_="200") {
        int32_t temp = program->get_synced_type_id(
            ::interserver::MAPTYPE_FUNCTION_CALL);
        buffer->write_value(&temp);
        temp = 0;
        if (front) {
            temp = front->get_other_id();
        }
        buffer->write_value(&temp);
        temp = program->get_synced_func_id(F_SYNC_FRONTS_ID);
        buffer->write_value(&temp);

        buffer->write_container_numbers(&v1_, typemap, false);
        buffer->write_container_wrapped_pointers(&v2_, typemap, false);
        buffer->write_container_wrapped_pointers(&v3_, typemap, false);
        buffer->write_container_wrapped_pointers(&v4_, typemap, false);
        buffer->write_value(&ono_);
    }

    //--------------------------------------------------------------------------

    void f_other(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer,
            bool flag_,
            int32_t i_) {
        int32_t temp = program->get_synced_type_id(
            ::interserver::MAPTYPE_FUNCTION_CALL);
        buffer->write_value(&temp);
        temp = 0;
        if (front) {
            temp = front->get_other_id();
        }
        buffer->write_value(&temp);
        temp = program->get_synced_func_id(F_OTHER_ID);
        buffer->write_value(&temp);

        buffer->write_value(&flag_);
        buffer->write_value(&i_);
    }

    //--------------------------------------------------------------------------

    void f_new(
            ::interserver::ProgramBase* program,
            ::interserver::FrontBase* front,
            ::interserver::Buffer* buffer) {
        int32_t temp = program->get_synced_type_id(
            ::interserver::MAPTYPE_FUNCTION_CALL);
        buffer->write_value(&temp);
        temp = 0;
        if (front) {
            temp = front->get_other_id();
        }
        buffer->write_value(&temp);
        temp = program->get_synced_func_id(F_NEW_ID);
        buffer->write_value(&temp);

    }

    //--------------------------------------------------------------------------



    void typemap_register(TypeMap* typemap) {
        ::interserver::FuncType* new_func;
        
        new_func = typemap->add_func(
            "::interserver::uhaha::CF::C::ServerCoreFront::f_sync_fronts",
            F_SYNC_FRONTS_ID);
    
        new_func->add_arg("v1", ::interserver::TYPEMAP_ID<std::vector<int32_t>&>::ID);
        new_func->add_arg("v2", ::interserver::uhaha::MyType::static_get_type_id(nullptr));
        new_func->add_arg("v3", ::ns::MyType::static_get_type_id(nullptr));
        new_func->add_arg("v4", ::interserver::uhaha::ns::MyType::static_get_type_id(nullptr));
        new_func->add_arg("ono", ::interserver::TYPEMAP_ID<std::string>::ID);
        new_func = typemap->add_func(
            "::interserver::uhaha::CF::C::ServerCoreFront::f_other",
            F_OTHER_ID);
    
        new_func->add_arg("flag", ::interserver::TYPEMAP_ID<bool>::ID);
        new_func->add_arg("i", ::interserver::TYPEMAP_ID<int32_t>::ID);
        typemap->add_func(
            "::interserver::uhaha::CF::C::ServerCoreFront::f_new",
            F_NEW_ID);
    
    }

} } } } }