#include "interserver/core/server_frontbase.hpp"
#include "interserver/core/clientsocketwrapper.hpp"     //!!include


namespace interserver { namespace uhaha {

    class ServerCoreFront : public ServerFrontBase<ServerCoreFront> {
    private:
        int id_ = 0;
    public:
        ServerCoreFront();

        void f_sync_fronts(ClientSocketWrapper* client,
                std::vector<int32_t>& v1,
                std::vector<std::shared_ptr<MyType>>& v2,
                std::vector<std::shared_ptr<::ns::MyType>>& v3,
                std::vector<std::shared_ptr<ns::MyType>>& v4,
                std::string ono="200");

        //== const_id=-1
        void f_other(ClientSocketWrapper* client, bool flag, int32_t i);
        void f_new(ClientSocketWrapper* client);


        void read_function_call(ClientSocketWrapper* client);

        int get_id() { return id_ };
    };


    //void i_other11(ClientSocketWrapper* client, bool flag, int32_t i);



    ServerCoreFront::ServerCoreFront():
        ServerFrontBase("core", 0) {}

} }
