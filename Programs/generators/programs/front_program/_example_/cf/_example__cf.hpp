#pragma once

#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/clientsocketwrapper.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "../_example_.hpp"
#include "interserver/core/clientsocketwrapper.hpp"


namespace interserver {
namespace uhaha {
namespace CF {
namespace C {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_FRONTS_ID = ::interserver::FrontBase::gen_func_id();
    const int32_t F_OTHER_ID = -1;
    const int32_t F_NEW_ID = ::interserver::FrontBase::gen_func_id();

} } } } }


namespace interserver { namespace uhaha { 

    //--------------------------------------------------------------------------

    void ServerCoreFront::read_function_call(
            ::interserver::ClientSocketWrapper* client);
                
    //--------------------------------------------------------------------------

    void ServerCoreFront::typemap_register(TypeMap* typemap);

} }