#include <vector>
#include <string>
#include "interserver/core/buffer.hpp"
#include "interserver/core/clientsocketwrapper.hpp"
#include "interserver/core/frontbase.hpp"
#include "interserver/core/programbase.hpp"
#include "interserver/core/typemap.hpp"
#include "../_example_.hpp"
#include "interserver/core/clientsocketwrapper.hpp"


namespace interserver {
namespace uhaha {
namespace CF {
namespace C {
namespace ServerCoreFront {

    // constants
    const int32_t F_SYNC_FRONTS_ID = ::interserver::FrontBase::gen_func_id();
    const int32_t F_OTHER_ID = -1;
    const int32_t F_NEW_ID = ::interserver::FrontBase::gen_func_id();

} } } } }


namespace interserver { namespace uhaha { 

    //--------------------------------------------------------------------------

    void ServerCoreFront::read_function_call(
            ::interserver::ClientSocketWrapper* client) {

        int32_t func_id;
        client->read_buffer_->read_value(&func_id);
        func_id = get_program()->get_synced_func_id(func_id);

        if (func_id == ::interserver::uhaha::CF::C::ServerCoreFront::F_SYNC_FRONTS_ID) { 
            std::vector<int32_t>& v1_;
            v1_.clear();
                buffer->read_container_numbers(&v1_, typemap, false);
            
            std::vector<std::shared_ptr<::interserver::uhaha::MyType>>& v2_;
            v2_.clear();
                buffer->read_container_wrapped_pointers(&v2_, typemap, false);
            
            std::vector<std::shared_ptr<::ns::MyType>>& v3_;
            v3_.clear();
                buffer->read_container_wrapped_pointers(&v3_, typemap, false);
            
            std::vector<std::shared_ptr<::interserver::uhaha::ns::MyType>>& v4_;
            v4_.clear();
                buffer->read_container_wrapped_pointers(&v4_, typemap, false);
            
            std::string ono_;
            buffer->read_value(&ono_);
            
            f_sync_fronts(client, v1, v2, v3, v4, ono);
        }if (func_id == ::interserver::uhaha::CF::C::ServerCoreFront::F_OTHER_ID) { 
            bool flag_;
            buffer->read_value(&flag_);
            
            int32_t i_;
            buffer->read_value(&i_);
            
            f_other(client, flag, i);
        }if (func_id == ::interserver::uhaha::CF::C::ServerCoreFront::F_NEW_ID) { 
            f_new(client);
        }
        else {
            client->write_error_message("Unknown function ID: " +
                    std::to_string(func_id), get_logger());
        }
    }

    //--------------------------------------------------------------------------

    void ServerCoreFront::typemap_register(TypeMap* typemap) {
        if (typemap == nullptr) {
            return;
        }

        ::interserver::FuncType* new_func;
        
        new_func = typemap->add_func(
            "::interserver::uhaha::CF::C::ServerCoreFront::f_sync_fronts",
            ::interserver::uhaha::CF::C::ServerCoreFront::F_SYNC_FRONTS_ID);
    
        new_func->add_arg("v1", ::interserver::TYPEMAP_ID<std::vector<int32_t>&>::ID);
        new_func->add_arg("v2", ::interserver::uhaha::MyType::static_get_type_id(nullptr));
        new_func->add_arg("v3", ::ns::MyType::static_get_type_id(nullptr));
        new_func->add_arg("v4", ::interserver::uhaha::ns::MyType::static_get_type_id(nullptr));
        new_func->add_arg("ono", ::interserver::TYPEMAP_ID<std::string>::ID);
        new_func = typemap->add_func(
            "::interserver::uhaha::CF::C::ServerCoreFront::f_other",
            ::interserver::uhaha::CF::C::ServerCoreFront::F_OTHER_ID);
    
        new_func->add_arg("flag", ::interserver::TYPEMAP_ID<bool>::ID);
        new_func->add_arg("i", ::interserver::TYPEMAP_ID<int32_t>::ID);
        typemap->add_func(
            "::interserver::uhaha::CF::C::ServerCoreFront::f_new",
            ::interserver::uhaha::CF::C::ServerCoreFront::F_NEW_ID);
    
    }

} }