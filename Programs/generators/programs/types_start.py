import os
import re
if __name__ != '__main__':
    from .types_program.utils import tps_parser
    from .types_program.utils.tps_parser import translate_file


if __name__ == '__main__':
    p = ''
    from types_program.utils import tps_parser
    tps_parser.translate_file(
        p + 'types_program/_example_source_.tps',
        p + 'types_program/_example_dest_',
        options={
            'class_base': '::interserver::MessageBase',
        })
