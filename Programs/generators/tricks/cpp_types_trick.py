import os
from watchdog.tricks import Trick
from ..programs import types_start


class CppTypesTrick(Trick):

    def hr(self, chars):
        print(chars*80)

    def main_hr(self):
        self.hr('=')

    def sub_hr(self):
        self.hr('-')

    def __init__(self,
            patterns=None,
            ignore_patterns=None,
            ignore_directories=False, case_sensitive=True,

            source_folder=False, dest_folder=False):

        super().__init__(
            patterns, ignore_patterns, ignore_directories, case_sensitive)

        self.source_folder_orig = source_folder
        self.dest_folder_orig = dest_folder

        self.source_folder = os.path.realpath(source_folder)
        self.dest_folder = os.path.realpath(dest_folder)

        self.main_hr()
        print('TypesTrick created:')
        self.sub_hr()
        print('Source folder: %s' % self.source_folder)
        self.sub_hr()
        print('Dest folder: %s' % self.dest_folder)
        self.main_hr()


    def on_any_event(self, event):
        pass

    def on_modified(self, event):
        print('TypesTrick Detected modification in: %s' % event.src_path)
        src_path = os.path.realpath(event.src_path)

        add_to_dest = src_path[len(self.source_folder):]
        add_to_dest = add_to_dest.lstrip('/').lstrip('\\')
        add_to_dest = os.path.splitext(add_to_dest)[0]

        dest_path = os.path.join(self.dest_folder, add_to_dest)
        dest_path_orig = os.path.join(self.dest_folder_orig, add_to_dest)

        types_start.translate_file(src_path, dest_path, options={
            'class_base': '::interserver::MessageBase',
            'namespaces': ['interserver', 'msgtype'],
        })
        self.main_hr()

    def on_deleted(self, event):
        pass

    def on_created(self, event):
        pass

    def on_moved(self, event):
        pass
