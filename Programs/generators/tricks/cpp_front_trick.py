import os
from watchdog.tricks import Trick
from ..programs import front_start


class CppFrontTrick(Trick):

    def hr(self, chars):
        print(chars*80)

    def main_hr(self):
        self.hr('=')

    def sub_hr(self):
        self.hr('-')


    def __init__(self,
            patterns=None,
            ignore_patterns=None,
            ignore_directories=False, case_sensitive=True):

        super().__init__(
            patterns, ignore_patterns, ignore_directories, case_sensitive)

        self.main_hr()
        print('FrontTrick created:')
        self.sub_hr()
        print(self.patterns)
        self.main_hr()


    def on_modified(self, event):
        src_path = os.path.realpath(event.src_path)

        path, filename = os.path.split(src_path)
        if '.' in filename:
            filename = filename.split('.', 1)[0]

        #remove 'front' part
        path, wrapper_folder = os.path.split(path)

        if wrapper_folder != 'fronts':
            #self.sub_hr()
            #print('File ignored'.upper())
            #self.main_hr()
            return

        path, folder_sender = os.path.split(path)


        if folder_sender == 'server':
            is_server_front = True
            folder_receiver = 'client'
        elif folder_sender == 'client':
            is_server_front = False
            folder_receiver = 'server'
        else:
            #self.sub_hr()
            #print('File ignored'.upper())
            #self.main_hr()
            return

        sender_path = os.path.join(path, folder_sender, 'fronts')
        receiver_path = os.path.join(path, folder_receiver, 'fronts')

        print('FrontTrick Detected modification in: %s' % event.src_path)
        self.main_hr()
        front_start.parser.generate_cpp_files(
            src_path, sender_path, receiver_path, is_server_front)
